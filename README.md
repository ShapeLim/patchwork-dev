# Patchwork

### To 공동저자들에게

* 현재 Patchwork2를 다른 데이터셋에서 돌릴 때는 `config/params.yaml`의 `sensor height`와 `sensor_model`를 변경해야 함
* 하지만 귀찮으니 sensor_model은 그대로 두고 먼저 돌려보길...더 dense한 LiDAR에 대해서는 큰 문제 없을 듯
* 현재 Patchwork2는 `GSAP`으로 돼있음. 
    * 헤더파일은 `include/patchwork/bin.hpp`와 `include/patchwork/GSAP.hpp`으로 돼있음
    * To 민호: KITTI에서 테스트하느라 `utils.hpp`가 include 돼있는데 알고리즘 동작에는 필요 없는 부분이어서, 다른 레포지토리로 옮겨서 테스트해야 하면 걍 제거해도 됨
* 현재 Patchwork2의 최종 모드로 돌리려면 파라미터가 아래와 같이 `rosparam set`을 해준 후에 돌려야 함
```
<rosparam param="/algorithm">"GSAP"</rosparam>
<rosparam param="/mode">"intercoupled"</rosparam>
<rosparam param="/gnc_on">"no_gnc"</rosparam>
```

* To 승재: KITTI에서 돌려볼 때는 해당 브랜치의 `launch/offline_kitti_ht.launch` 사용
    * `sequence`와 `init_idx` 설정해야 함
    * rviz 파일은 현재 너의 dev 브랜치와 merge해둔 상태이니 적절히 사용하도록...
    * flatness가 쌓일 적절한 time stamp가 필요하므로 약 20 frame 이전에서 실행시켜야 함
        * E.g. `02`의 `3300`이 타겟이면 `sequence`는 `02`, `init_idx`는 3280


# 발전한 baseline 저장할 branch

* `print_properties_for_analysis()`로 분석할 수 있음
* 현재 initial margin 0.2, 0.5 밖에 조정 안 해봤지만 추가적으로 조정 필요해서 ablation study 필요해보임!
    * 낮아지면 precision이 많이 올라감

Official page of *"Patchwork: Concentric Zone-based Region-wise Ground Segmentation with Ground Likelihood Estimation Using a 3D LiDAR Sensor"*, which is accepted by RA-L with IROS'21 option 

#### [[Video](https://youtu.be/rclqeDi4gow)] [[Preprint Paper](https://urserver.kaist.ac.kr/publicdata/patchwork/RA_L_21_patchwork_final_submission.pdf)] [[Project Wiki](https://github.com/LimHyungTae/patchwork/wiki)]

Patchwork                  |  Concept of our method (CZM & GLE)
:-------------------------:|:-------------------------:
![](img/patchwork_concept_resized.jpg) |  ![](img/patchwork.gif)

It's an overall updated version of **R-GPF of ERASOR** [[Code](https://github.com/LimHyungTae/ERASOR)] [[Paper](https://arxiv.org/abs/2103.04316)]. 

----

# Demo

## KITTI 00 

![](img/demo_kitti00_v2.gif)

## Rough Terrain

![](img/demo_terrain_v3.gif)

----


### Characteristics

* Single hpp file (`include/patchwork/patchwork.hpp`)

* Robust ground consistency

As shown in the demo videos and below figure, our method shows the most promising robust performance compared with other state-of-the-art methods, especially, our method focuses on the little perturbation of precision/recall as shown in [this figure](img/seq_00_pr_zoom.pdf).

Please kindly note that the concept of *traversable area* and *ground* is quite different! Please refer to our paper.


## Contents
0. [Test Env.](#Test-Env.)
0. [Requirements](#requirements)
0. [How to Run Patchwork](#How-to-Run-Patchwork)
0. [Citation](#citation)

### Test Env.

The code is tested successfully at
* Linux 18.04 LTS
* ROS Melodic

## Requirements

### ROS Setting
- 1. Install [ROS](http://torch.ch/docs/getting-started.html) on a machine. 
- 2. Thereafter, [jsk-visualization](https://github.com/jsk-ros-pkg/jsk_visualization) is required to visualize Ground Likelihood Estimation status.

```bash
sudo apt-get install ros-melodic-jsk-recognition
sudo apt-get install ros-melodic-jsk-common-msgs
sudo apt-get install ros-melodic-jsk-rviz-plugins
```

- 3. Compile compile this package. We use [catkin tools](https://catkin-tools.readthedocs.io/en/latest/),
```bash
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
git clone https://github.com/LimHyungTae/patchwork.git
cd .. && catkin build patchwork 
```

## How to Run Patchwork

We provide three examples

* Offline KITTI dataset
* Online (ROS Callback) KITTI dataset
* Own dataset using pcd files

### Offline KITTI dataset

1. Download [SemanticKITTI](http://www.semantic-kitti.org/dataset.html#download) Odometry dataset (We also need labels since we also open the evaluation code! :)

2. Set the `data_path` in `launch/offline_kitti.launch` for your machine.

The `data_path` consists of `velodyne` folder and `labels` folder as follows:

```
data_path (e.g. 00, 01, ..., or 10)
_____velodyne
     |___000000.bin
     |___000001.bin
     |___000002.bin
     |...
_____labels
     |___000000.label
     |___000001.label
     |___000002.label
     |...
_____...
   
```

3. Run launch file 
```
roslaunch patchwork offline_kitti.launch
```

You can directly feel the speed of Patchwork! :wink:

### Online (ROS Callback) KITTI dataset

We also provide rosbag example. If you run our patchwork via rosbag, please refer to this example.

1. Download readymade rosbag 

```
wget https://urserver.kaist.ac.kr/publicdata/patchwork/kitti_00_xyzilid.bag
```

2. After building this package, run the roslaunch as follows:

```
roslaunch patchwork rosbag_kitti.launch
```

3. Then play the rosbag file in another command

```
rosbag play kitti_00_xyzilid.bag
```

### Own dataset using pcd files

To be updated (In fact, we already set the data loader in `include/tools/pcd_loader.hpp`) 



## Citation

If you use our code or method in your work, please consider citing the following:

	@article{lim2021patchwork,
    title={Patchwork: Concentric Zone-based Region-wise Ground Segmentation with Ground Likelihood Estimation Using a 3D LiDAR Sensor},
    author={Lim, Hyungtae and Minho, Oh and Myung, Hyun},
    journal={IEEE Robotics and Automation Letters},
    year={2021}
    }

---------

### Description

All explanations of parameters and other experimental results will be uploaded in [wiki](https://github.com/LimHyungTae/patchwork/wiki)

### Contact

If you have any questions, please let me know:

- Hyungtae Lim {[shapelim@kaist.ac.kr]()}


### TODO List

- [x] Add ROS support
- [x] Add preprint paper
- [x] Add demo videos
- [ ] Add demo examples
- [ ] Update wiki

-----

# patchwork2_data
