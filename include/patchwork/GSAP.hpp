#ifndef GENERALIZED_PATCHWORK_H
#define GENERALIZED_PATCHWORK_H

#define NOT_INITIALIZED -1.0

#include <patchwork/bin.hpp>

bool PARTIAL_DRAW_ON = false;
//static vector<int> given_sector_idxes = {0, 7, 8, 15};
static vector<int> given_sector_idxes = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
//static vector<int> given_sector_idxes = {0, 1, 6, 7, 8, 9, 14, 15};

struct HDL64E_PARAM {
    vector<vector<int> > NUM_LASER_CHANNELS_PER_ZONE = {{24, 12},
                                                        {4, 3, 2, 2},
                                                        {2, 2, 2, 1},
                                                        {1, 1, 1, 1, 1}};
    vector<int>          NUM_RINGS_PER_ZONE          = {2, 4, 4, 5};
    vector<int>          NUM_SECTORS_PER_ZONE        = {16, 32, 56, 32}; // 4의 배수가 되어야 함!
    // Please refer to set_prior_idxes_of_patches

    float LOWER_FOV_BOUNDARY = -24.8;
    float VERTICAL_RESOLUTION = 0.4;
};

struct RGBD_PARAM {
    vector<int>          NUM_RINGS_PER_ZONE          = {5, 2};
    vector<int>          NUM_SECTORS_PER_ZONE        = {36, 32}; // 36 shows the best performance
    vector<float>        MAX_RANGES_PER_ZONE         = {6.0, 10.0};

//    vector<int>          NUM_RINGS_PER_ZONE          = {4, 2};
//    vector<int>          NUM_SECTORS_PER_ZONE        = {54, 32};
//    vector<float>        MAX_RANGES_PER_ZONE         = {4.0, 7.0};

    float LOWER_FOV_BOUNDARY = -65.0;
};

template<typename PointT>
class GSAP {

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    typedef std::vector<Patch<PointT> > Ring;

    unsigned long TIME_STEP_ = 0;

    GSAP<PointT>() {};

    GSAP<PointT>(ros::NodeHandle *nh) : node_handle_(*nh) {
        // Init ROS related
        ROS_INFO("Inititalizing GSAP...");
        node_handle_.param<string>("/sequence", SEQ, "00");

        node_handle_.param("sensor_height", sensor_height_, 1.723);
        node_handle_.param<bool>("/gsap/verbose", verbose_, true);

        node_handle_.param<float>("/gsap/sensor_model/root_plane_z_var", root_plane_z_var_, 0.0002);
        node_handle_.param<float>("/gsap/prior_plane/root_plane_planarity", root_plane_planarity_, 0.75);
        node_handle_.param<float>("/gsap/prior_plane/z_var_noise", z_var_noise_, 0.0005);
        node_handle_.param<float>("/gsap/prior_plane/linearity_thr", linearity_thr_, 0.9);
        node_handle_.param<bool>("/gsap/prior_plane/use_vicinity_plane", use_vicinity_planes_, false);

        node_handle_.param("/gsap/num_iter", num_iter_, 3);
        node_handle_.param("/gsap/num_lpr", num_lpr_, 20);
        node_handle_.param("/gsap/num_min_pts", num_min_pts_, 10);
        node_handle_.param("/gsap/th_seeds", th_seeds_, 0.5);
        node_handle_.param("/gsap/th_dist", th_dist_, 0.3);
        node_handle_.param("/gsap/min_r", min_range_, 2.7); // It indicates bodysize of the car.
        node_handle_.param("/gsap/num_prior_patches", num_prior_patches_, 3);

        node_handle_.param("/gsap/sv_container/num_max_singular_values", num_max_singular_values_, 50);
        node_handle_.param<float>("/gsap/sv_container/sv_init_mean", sv_init_mean_, 0.001); // It should be large enough
        node_handle_.param<float>("/gsap/sv_container/sv_init_std", sv_init_std_, 0.002); // It should be large enough

        node_handle_.param<float>("/gsap/TRIS/upper_bound", tris_params_.upper_bound, 0.4);
        node_handle_.param<float>("/gsap/TRIS/lower_bound", tris_params_.lower_bound, -0.7);
        node_handle_.param<int>("/gsap/TRIS/num_min_pts_for_pseudo_occupancy", tris_params_.num_min_pts_for_pseudo_occupancy, 40);
        node_handle_.param<float>("/gsap/TRIS/rel_z_for_pseudo_occupancy", tris_params_.rel_z_for_pseudo_occupancy, 0.65);
        node_handle_.param<float>("/gsap/TRIS/scale_for_pseudo_occupancy", tris_params_.scale_for_pseudo_occupancy, 2.0);
        node_handle_.param<int>("/gsap/TRIS/ng_to_g_ring_idx", tris_params_.ng_to_g_ring_idx, 8);

        nh->param<bool>("/gsap/revert/revert_on", revert_params_.revert_on, false);
        nh->param<int>("/gsap/revert/num_min_revert_pts", revert_params_.num_min_pts_for_revert, 5);
        nh->param<int>("/gsap/revert/ring_idx_boundary", revert_params_.ring_idx_boundary, 0);
        nh->param<float>("/gsap/revert/th_revert_prob", revert_params_.th_revert_prob, 0.1);
        nh->param<float>("/gsap/revert/th_mean_max_res", revert_params_.th_mean_max_res, 0.4);
        nh->param<float>("/gsap/revert/th_mean_inner_product", revert_params_.th_mean_inner_product, 0.94);
        nh->param<float>("/gsap/revert/th_mean_singular_value", revert_params_.th_mean_singular_value, 0.004);
        nh->param<float>("/gsap/revert/th_mean_surface_variable", revert_params_.th_mean_surface_variable, 0.002);

        nh->param<float>("/gsap/revert/th_std_max_res", revert_params_.th_std_max_res, 0.4);
        nh->param<float>("/gsap/revert/th_std_inner_product", revert_params_.th_std_inner_product, 0.94);
        nh->param<float>("/gsap/revert/th_std_larger_singular_value", revert_params_.th_std_larger_singular_value, 0.002);
        nh->param<float>("/gsap/revert/th_std_smaller_singular_value", revert_params_.th_std_smaller_singular_value, 0.1614);
        nh->param<float>("/gsap/revert/th_std_surface_variable", revert_params_.th_std_surface_variable, 0.0005);

        node_handle_.param<float>("/gsap/closest_ring_upper_margin", closest_ring_upper_margin_, 0.8);
        node_handle_.param<float>("/gsap/closest_ring_lower_margin", closest_ring_lower_margin_, 0.8);

        node_handle_.param<float>("/gsap/gnc/noise_bound", noise_bound_, 0.125);
        node_handle_.param<float>("/gsap/gnc/gnc_factor", gnc_factor_, 1.2);

        node_handle_.getParam("/patchwork/czm/elevation_thresholds", elevation_thr_);
        node_handle_.getParam("/patchwork/czm/flatness_thresholds", flatness_thr_);

        float abs_uprightness_angle, rel_uprightness_angle;
        node_handle_.param<float>("/gsap/uprightness/abs_uprightness_angle", abs_uprightness_angle, 75.0); // The larger, the more strict
        node_handle_.param<float>("/gsap/uprightness/rel_uprightness_angle", rel_uprightness_angle, 45.0); // The larger, the more strict

        node_handle_.param<float>("/gsap/rel_z_thr_independent", gseg_params_.rel_z_thr_independent, 1.0);
        node_handle_.param<float>("/gsap/rel_z_thr_intercoupled", gseg_params_.rel_z_thr_intercoupled, 1.0);
        node_handle_.param<int>("/gsap/zone1_idx", gseg_params_.zone1_idx, 2);
        node_handle_.param<int>("/gsap/zone2_idx", gseg_params_.zone2_idx, 7);

        for (int i = 0; i < 100; ++i) {
            cout << gseg_params_.zone1_idx << endl;
        }

        node_handle_.param<bool>("/gsap/use_independent_estimation", use_independent_estimation_, false);
        node_handle_.param<bool>("/verbose/independent_on", v_i_, false);
        node_handle_.param<bool>("/verbose/tris_on", v_t_, false);
        node_handle_.param<bool>("/verbose/ground_est_on", v_g_, false);
        node_handle_.param<bool>("/verbose/prior_plane_on", v_p_, false);


        abs_uprightness_thr_ = sin(DEG2RAD(abs_uprightness_angle));
        rel_uprightness_thr_ = sin(DEG2RAD(rel_uprightness_angle));

        num_rings_of_interest_ = elevation_thr_.size();

        // To filter noises caused by multipath/reflection of LiDAR signal
        closest_ring_upper_z_thr_ = -sensor_height_ + closest_ring_upper_margin_;
        closest_ring_lower_z_thr_ = -sensor_height_ - closest_ring_lower_margin_;

        ROS_INFO("Sensor Height: %f", sensor_height_);
        ROS_INFO("Num of Iteration: %d", num_iter_);
        ROS_INFO("Num of LPR: %d", num_lpr_);
        ROS_INFO("Num of min. points: %d", num_min_pts_);
        ROS_INFO("Seeds Threshold: %f", th_seeds_);
        ROS_INFO("Distance Threshold: %f", th_dist_);
        ROS_INFO("Min. range:: %f", min_range_);
        ROS_INFO("Abs. uprightness: %f", abs_uprightness_thr_);
        ROS_INFO("Rel. uprightness: %f", rel_uprightness_thr_);
        ROS_INFO("Noise Bound: %f", noise_bound_);
        ROS_INFO("GNC Factor: %f", gnc_factor_);
        ROS_INFO("Closest ring upper boundary: %f", closest_ring_upper_margin_);
        ROS_INFO("Upper boundary of TRIS: %f", tris_params_.upper_bound);
        ROS_INFO("Lower boundary of TRIS: %f", tris_params_.lower_bound);

        node_handle_.param<string>("/gsap/sensor_model", sensor_model_, "HDL-64E");

        node_handle_.param("/patchwork/visualize", visualize_, true);

        node_handle_.param<string>("/mode", mode_, "original");
        node_handle_.param<string>("/gnc_on", gnc_on_, "no_gnc");

        poly_list_.header.frame_id = "/map";
        poly_list_.polygons.reserve(130000);

        revert_pc.reserve(NUM_HEURISTIC_MAX_PTS_IN_CLOUD);
        reject_pc.reserve(NUM_HEURISTIC_MAX_PTS_IN_CLOUD);

        PlaneViz      = node_handle_.advertise<jsk_recognition_msgs::PolygonArray>("/gpf/plane", 100, true);
        revert_pc_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/revert_pc", 100, true);
        below_est_g_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/below_ground", 100, true);
        reject_pc_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/reject_pc", 100, true);
        init_seed_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/initial_seeds", 100, true);
        normal_pub    = node_handle_.advertise<sensor_msgs::PointCloud2>("/normals", 100, true);
        viz_marker_pub = node_handle_.advertise<visualization_msgs::MarkerArray>("/bin_id", 100, true);

        root_plane_ = set_root_plane(root_plane_z_var_, root_plane_planarity_);
        cout << "[Initialization]: Setting params..."  << endl;
        set_parameters(sensor_model_);
        cout << "[Initialization]: Setting NZM..."  << endl;
        set_nonlinear_zone_model(sensor_model_);

        set_degeneracy_index();
        tris_params_.degeneracy_idx = degeneracy_idx_;

        if (verbose_) {
            cout << "\033[1;32mCurrent state of NZM\033[0m" << endl;
            cout << sqr_boundary_ranges_.size() << endl;
            cout << num_sectors_per_ring_.size() << endl;
            cout << NZM_.size() << endl;
            int      idx = 0;
            for (int i : num_sectors_per_ring_) {
                cout << i << " vs " << NZM_[idx++].size() << endl;
            }
            cout << endl;

            for (float sqr_r: sqr_boundary_ranges_) {
                cout << sqrt(sqr_r) << " , ";
            }
            cout << endl;

            for (int j = 0; j < num_rings_of_interest_; ++j) {
                cout << elevation_thr_[j] << " vs " << flatness_thr_[j] << endl;
            }
        }
    }

    void estimate_ground(
            const pcl::PointCloud<PointT> &cloudIn,
            pcl::PointCloud<PointT> &cloudOut,
            pcl::PointCloud<PointT> &cloud_nonground,
            pcl::PointCloud<PointT> &cloud_viz_nonground,
            double &time_taken);

    bool is_available_as_prior_plane(const Patch<PointT> &candidate);

    geometry_msgs::PolygonStamped set_plane_polygon(const MatrixXf &normal_v, const float &d);

private:
    ros::NodeHandle node_handle_;
    string          mode_;
    string          gnc_on_;

    TRISParams      tris_params_;
    RevertParams    revert_params_;
    GSegParams      gseg_params_;

    // For sensor configuration
    string sensor_model_;

    float  lower_fov_boundary_;
    float  laser_vertical_resolution_;

    // Verbose Flags
    bool   v_i_;
    bool   v_t_;
    bool   v_g_;
    bool   v_p_;

    bool   use_independent_estimation_;
    bool   use_vicinity_planes_;
    float  root_plane_z_var_;
    float  root_plane_planarity_;
    float  z_var_noise_;
    float  linearity_thr_;

    // Parameters for non-linear zone model
    // sqr: For reducing computational complexity
    vector<float> sqr_boundary_ranges_;
    // For visualization
    vector<float> boundary_ranges_;

    // For NZM
    int                    num_rings_of_interest_;
    vector<vector<int> >   num_laser_channels_per_zone_;
    vector<int>            num_sectors_per_ring_;
    vector<Ring>           NZM_;
    vector<PlaneInfo>      prior_planes_;
    vector<PlaneInfo>      vicinity_planes_; //

    // For sv container
    vector<deque<float> >  sv_container_; // The third singular values
    int num_max_singular_values_;
    float sv_init_mean_;
    float sv_init_std_;

    int num_iter_;
    int num_lpr_;
    int num_min_pts_;
    int num_total_rings_;

    int degeneracy_idx_ = INT_MAX;
    // sv container


    double sensor_height_;
    double th_seeds_;
    double th_dist_;
    double min_range_;
    double abs_uprightness_thr_;
    double rel_uprightness_thr_;

    float noise_bound_;
    float gnc_factor_;

    float closest_ring_upper_margin_;
    float closest_ring_lower_margin_;

    float closest_ring_upper_z_thr_;
    float closest_ring_lower_z_thr_;

    bool  verbose_;
    float d_;
    float th_dist_d_;

    // For visualization
    bool visualize_;

    vector<double> elevation_thr_;
    vector<double> flatness_thr_;

    int num_prior_patches_; // heuristic. The front value is the parameter

    PlaneInfo root_plane_;

    jsk_recognition_msgs::PolygonArray poly_list_;

    ros::Publisher                        PlaneViz, revert_pc_pub, reject_pc_pub, init_seed_pub, normal_pub, below_est_g_pub, viz_marker_pub;
    pcl::PointCloud<PointT>               revert_pc, reject_pc, init_seed_pc, below_est_g;
    pcl::PointCloud<pcl::PointXYZINormal> normals_; // For visualization

    /***
     * Initialization function
     */
    void set_parameters(const string &sensor_model);

    template<typename SensorT>
    void set_num_sectors_per_ring(const SensorT& params);

    void set_nonlinear_zone_model(const string &sensor_model);

    void set_sqr_boundary_ranges(const string sensor_model, const float init_incident_angle);

    PlaneInfo set_root_plane(float z_var, float planarity);

    void initialize_patches();

    void initialize_vicinity_planes();

    void initialize_sv_containers(const string sensor_model);

    void set_prior_idxes_of_patches(Patch<PointT> &patch, int target_ring_idx, int target_sector_idx);

    void set_degeneracy_index();

    /**************************************/

    void calc_mean_and_std(const int ring_idx, float& mean, float& std);

    void update_vicinity_planes();

    void update_sv_container();

    int get_previous_sector_idx(const int ring_idx, const int sector_idx);

    void get_closest_plane_info(const int ring_idx, const int sector_idx,
                                              vector<PlaneInfo>& candidates, int num_cand);
    /**************************************/

    void estimate_ground_independently(pcl::PointCloud<PointT> &est_ground, pcl::PointCloud<PointT> &est_nonground);

    void estimate_ground_intercoupled(pcl::PointCloud<PointT> &est_ground, pcl::PointCloud<PointT> &est_nonground,
                                      pcl::PointCloud<PointT> &est_viz_nonground);

    void set_prior_planes_near_the_patch(Patch<PointT> &patch, vector<PlaneInfo> &prior_bins, bool verbose=false);

    void flush_all_patches();

    float xy2theta(const float &x, const float &y);

    float xy2sqr_r(const float &x, const float &y);

    bool is_in_boundary(const PointT p);

    int get_ring_idx(const PointT p);

    int get_sector_idx(const PointT p, const int ring_idx);

    float get_z_prior(const int &ring_idx, const int &sector_idx);

    /***
     * For visulization of Ground Likelihood Estimation
     */
    geometry_msgs::PolygonStamped set_polygons(int ring_idx, int sector_idx, int num_sectors, int num_split);
};

template<typename PointT>
inline
void GSAP<PointT>::set_parameters(const string &sensor_model) {
    // https://hypertech.co.il/wp-content/uploads/2015/12/HDL-64E-Data-Sheet.pdf
    if (sensor_model == "HDL-64E") {
        HDL64E_PARAM params;
        // The below parameters are only used in `set_nonlinear_zone_model`
        lower_fov_boundary_        = params.LOWER_FOV_BOUNDARY;
        laser_vertical_resolution_ = params.VERTICAL_RESOLUTION;

        // Setting main parameters for NZM
        num_laser_channels_per_zone_ = params.NUM_LASER_CHANNELS_PER_ZONE;

        // Num. of sectors per ring is set
        set_num_sectors_per_ring(params);

    } else if (sensor_model == "RGBD") {
        RGBD_PARAM params;
        lower_fov_boundary_ = params.LOWER_FOV_BOUNDARY;
        set_num_sectors_per_ring(params);

    } else {
        throw invalid_argument("[NZM] Other sensors are not implemented");
    }
}

template<typename PointT>
template<typename SensorT>
inline
void GSAP<PointT>::set_num_sectors_per_ring(const SensorT& params) {
    num_sectors_per_ring_.clear();
    int             count = 0;
    for (const auto &num_rings: params.NUM_RINGS_PER_ZONE) {
        cout << num_rings << " , ";
        for (int j = 0; j < num_rings; ++j) {
            num_sectors_per_ring_.push_back(params.NUM_SECTORS_PER_ZONE[count]);
        }
        count++;
    }
    num_total_rings_ = num_sectors_per_ring_.size();

}


template<typename PointT>
inline
void GSAP<PointT>::set_sqr_boundary_ranges(const string sensor_model, const float init_incident_angle) {
    /***
     * 제곱 값을 따로 저장하는 이유:
     * 연산적으로 sqrt (루트)가 컴퓨터 연산량을 많이 요구하기 때문에, ring_idx 얻을 때 square 값에서 찾는게 더 빠름
     * 그냥 boundary는 polygon viz를 위해 저장
     */
    // sqr (square): For speed-up
    // original : For viz polygon
    boundary_ranges_.clear();
    boundary_ranges_.push_back(min_range_);
    sqr_boundary_ranges_.clear();
    sqr_boundary_ranges_.push_back(pow(min_range_, 2));
    float    incident_angle = init_incident_angle;
    // ToDo: incident angle이 90도 이상이 되었을 때 멈추게끔 짜야 함
    if (sensor_model == "RGBD") {
        RGBD_PARAM params;
        int num_zones = params.NUM_RINGS_PER_ZONE.size();
        for (int i = 0; i < num_zones; ++i) {
            float boundary_interval = (params.MAX_RANGES_PER_ZONE[i] - boundary_ranges_.back()) / params.NUM_RINGS_PER_ZONE[i];
            for (int j = 0; j < params.NUM_RINGS_PER_ZONE[i]; ++j) {
                float boundary_range = boundary_ranges_.back() + boundary_interval;
                boundary_ranges_.push_back(boundary_range);
                sqr_boundary_ranges_.push_back(pow(boundary_range, 2));
            }
        }
    } else {
        for (int i = 0; i < num_laser_channels_per_zone_.size(); ++i) {
            vector<int> num_channels_per_ring = num_laser_channels_per_zone_[i];
            for (int    j                     = 0; j < num_channels_per_ring.size(); ++j) {
                incident_angle += static_cast<float>(num_channels_per_ring[j]) * laser_vertical_resolution_;
                float incident_angle_w_margin = incident_angle + 0.5 * laser_vertical_resolution_; // For safety margin
                float boundary_range          = tan(DEG2RAD(incident_angle_w_margin)) * sensor_height_;
                boundary_ranges_.push_back(boundary_range);
                sqr_boundary_ranges_.push_back(pow(boundary_range, 2));

                // Check degeneracy index
            }
        }
    }
}

template<typename PointT>
inline
void GSAP<PointT>::set_degeneracy_index() {
    int count = 0;
    for (int i = 0; i < num_laser_channels_per_zone_.size(); ++i) {
        vector<int> num_channels_per_ring = num_laser_channels_per_zone_[i];
        for (int    j                     = 0; j < num_channels_per_ring.size(); ++j) {
            // Check degeneracy index
            if (degeneracy_idx_ == INT_MAX && num_channels_per_ring[j] == 1) {
                degeneracy_idx_ = count;
                return;
            }
            count++;
        }
    }
}

template<typename PointT>
inline
void GSAP<PointT>::set_nonlinear_zone_model(const string &sensor_model) {
    // Seting ring boundaries to consider # of laser rings
    float init_incident_angle = 90.0 + lower_fov_boundary_;

//    cout << tan(DEG2RAD(init_incident_angle)) * sensor_height_ << "?!?!?!" <<endl;
    // For defensive programming
    if (tan(DEG2RAD(init_incident_angle)) * sensor_height_ < min_range_) {
        cout << tan(DEG2RAD(init_incident_angle)) * sensor_height_ << " vs " << min_range_ << endl;
        throw invalid_argument("[NZM] The parameter `min_r` is wrong. Check your sensor height or min. range");
    }
    set_sqr_boundary_ranges(sensor_model, init_incident_angle);
    // `initialize_patches` should follows `set_sqr_boundary_ranges`
    initialize_patches();
    initialize_sv_containers(sensor_model);
    if (use_vicinity_planes_) {
        initialize_vicinity_planes();
    }
}

template<typename PointT>
inline
PlaneInfo GSAP<PointT>::set_root_plane(float z_var, float planarity) {
    PlaneInfo plane_info;
    // It can be considered as definite ground
    plane_info.is_definite_ground_ = true;
    static MatrixXf normal;
    normal.resize(3, 1);
    normal(0, 0) = 0.0;
    normal(1, 0) = 0.0;
    normal(2, 0) = 1.0;

    static Eigen::Vector3f mean;
    mean(0, 0) = 0.0;
    mean(1, 0) = 0.0;
    mean(2, 0) = -sensor_height_;
    static float d = sensor_height_;

    static Eigen::Vector3f singular_values;
    singular_values(0, 0) = 3 * z_var; // Just in case 3 denotes 3 sigma
    singular_values(1, 0) = 3 * z_var;
    singular_values(2, 0) = z_var;

    plane_info.normal_          = normal;
    plane_info.mean_            = mean;
    plane_info.d_               = d;
    plane_info.singular_values_ = singular_values;
    plane_info.planarity_       = planarity;
    return plane_info;
}

template<typename PointT>
inline
void GSAP<PointT>::initialize_patches() {
    NZM_.clear();
    int num_rings = num_sectors_per_ring_.size();

    for (int j = 0; j < num_rings; j++) {
        Ring     ring;
        int      num_sectors = num_sectors_per_ring_[j];
        float    mean_r      = (boundary_ranges_[j] + boundary_ranges_[j + 1]) / 2.0;
        float    sector_size = 2.0 * M_PI / static_cast<float>(num_sectors);
        float    min_len = min(boundary_ranges_[j + 1] - boundary_ranges_[j], mean_r * sector_size);
        for (int i           = 0; i < num_sectors; i++) {
            Patch<PointT> patch = Patch<PointT>(j, i, &node_handle_);
            set_prior_idxes_of_patches(patch, j, i);
            patch.set_center_point(mean_r, sector_size * i + sector_size / 2.0);
            patch.set_min_len(min_len);
            ring.emplace_back(patch);
        }
        NZM_.emplace_back(ring);
    }
}

template<typename PointT>
inline
void GSAP<PointT>::initialize_vicinity_planes() {
    cout << "\033[1;32mVicinity Planes on \033[0m" << endl;
    int num_sectors_in_closest_ring = num_sectors_per_ring_[0];
    vicinity_planes_.reserve(num_sectors_in_closest_ring);
    for (int j = 0; j < num_sectors_in_closest_ring; j++) {
        vicinity_planes_.emplace_back(root_plane_);
    }
}

template<typename PointT>
inline
void GSAP<PointT>::initialize_sv_containers(const string sensor_model) {
    vector<int> num_rings_per_zone;
    if (sensor_model == "RGBD") {
        RGBD_PARAM params;
        num_rings_per_zone = params.NUM_RINGS_PER_ZONE;
    } else if (sensor_model == "HDL-64E") {
        HDL64E_PARAM params;
        num_rings_per_zone = params.NUM_RINGS_PER_ZONE;
    } else {throw invalid_argument("[Init. sv. containers]: Not implemented"); }

    deque<float> sv_dummy;
    for (auto& num_rings: num_rings_per_zone) {
        for (int i = 0; i < num_rings; ++i) {
            sv_container_.push_back(sv_dummy);
        }
    }
}

//
template<typename PointT>
inline
void GSAP<PointT>::flush_all_patches() {
    for (int i = 0; i < NZM_.size(); i++) {
        Ring     &ring = NZM_[i];
        for (int j     = 0; j < ring.size(); j++) {
            // point clouds in patch are flushed
            ring[j].clear();
        }
    }
}
template<typename PointT>
inline
int GSAP<PointT>::get_previous_sector_idx(const int ring_idx, const int sector_idx) {
    if (ring_idx == 0) {
        return -1;
    }
    int num_target_sectors   = num_sectors_per_ring_[ring_idx];
    int num_previous_sectors = num_sectors_per_ring_[ring_idx - 1];

    if (num_target_sectors == num_previous_sectors) {
        // They are in same zone
        return sector_idx;
    } else {
        float sector_idx_in_previous_ring = static_cast<float>(sector_idx + 1) * num_previous_sectors / num_target_sectors - 1;
        return static_cast<int>(floor(sector_idx_in_previous_ring + 0.5));
    }
}

template<typename PointT>
inline
void GSAP<PointT>::get_closest_plane_info(const int ring_idx, const int sector_idx,
                                          vector<PlaneInfo>& candidates, int num_cand) {
    candidates.clear();
    int target_ring_idx = ring_idx;
    int target_sector_idx = sector_idx;
    int num_previous_sectors;
    int prev_sector_idx, prev_sector_idx_plus_1, prev_sector_idx_minus_1;
    PlaneInfo p_closest;
    vector <int> s_idxes;

    // 1. Next patches
    int sector_idx_plus_1 = sector_idx + 1 == num_sectors_per_ring_[target_ring_idx]? 0: sector_idx + 1;
    int sector_idx_minus_1 = sector_idx - 1 == -1 ? num_sectors_per_ring_[target_ring_idx] - 1: sector_idx - 1;
    cout << sector_idx_minus_1 << "!!!!" << sector_idx_plus_1 << endl;
    s_idxes = {sector_idx_plus_1, sector_idx_minus_1};
    for (int s_idx: s_idxes) {
        auto &patch = NZM_[target_ring_idx][s_idx];
        if (patch.is_succeeded_) {
            patch.get_patch_info(p_closest);
            cout << "Closest: " << patch.ring_idx_ << " , " << patch.sector_idx_ << "!!!" << endl;
            candidates.emplace_back(p_closest);
        }
    }

    // 2. Previous patches
    while (target_ring_idx > 0 && candidates.size() < num_cand) {
        prev_sector_idx            = get_previous_sector_idx(target_ring_idx, target_sector_idx);
        num_previous_sectors       = num_sectors_per_ring_[target_ring_idx - 1] ;

        prev_sector_idx_plus_1     = (prev_sector_idx + 1) % num_previous_sectors;
        prev_sector_idx_minus_1    = (prev_sector_idx - 1) == -1 ? num_previous_sectors - 1 : prev_sector_idx - 1;
        --target_ring_idx;

        s_idxes = {prev_sector_idx, prev_sector_idx_plus_1, prev_sector_idx_minus_1};
        for (int s_idx: s_idxes) {
            auto &patch = NZM_[target_ring_idx][s_idx];
            if (patch.is_succeeded_) {
                patch.get_patch_info(p_closest);
                candidates.emplace_back(p_closest);
            }
        }
        target_sector_idx = prev_sector_idx;
    }

    if (candidates.empty()) {
        candidates.emplace_back(root_plane_);
    }
}

template<typename PointT>
inline
void GSAP<PointT>::set_prior_idxes_of_patches(Patch<PointT> &patch, int target_ring_idx, int target_sector_idx) {
    static string version = "V2";
    int num_target_sectors   = num_sectors_per_ring_[target_ring_idx];

    if (target_ring_idx == 0) {
        if (version == "V1") {
            return;
        } else if (version == "V2") {
            int quadrant = target_sector_idx * 4 / num_target_sectors + 1;
            bool is_in_first_or_third_quadrant = quadrant == 1 || quadrant == 3;
            int rel_idx = is_in_first_or_third_quadrant? -1 : +1;
            if (target_sector_idx + rel_idx != -1) {
                if (target_sector_idx + rel_idx == num_target_sectors) {
                    patch.neighbor_idx_set.emplace_back(make_pair(0, 0));
                } else {
                    patch.neighbor_idx_set.emplace_back(make_pair(0, target_sector_idx + rel_idx));
                }
            }
        }
        return;
    }

    int num_previous_sectors = num_sectors_per_ring_[target_ring_idx - 1];

    // 비율로 가장 가까운 idx를  찾음
    // target_sector_idx + 1 : current # sectors = previous_sector_idx + 1 : previous # sectors
    float patch_idx_in_previous_ring = static_cast<float>(target_sector_idx + 1) * num_previous_sectors / num_target_sectors - 1;
    // 0.5 for round
    int   prev_sector_idx            = static_cast<int>(floor(patch_idx_in_previous_ring + 0.5));
    int   prev_sector_idx_plus_1     = (prev_sector_idx + 1) % num_previous_sectors;
    int   prev_sector_idx_minus_1    = (prev_sector_idx - 1) == -1 ? num_previous_sectors - 1 : prev_sector_idx - 1;

    if (version == "V1") {
        // Ver. 1
        // 최소 3개의 이전 bin의 sector idx가 저장됨
        // (현재 ring_idx - 1, prev_sector_idx) <- id of prior bin!
        // 이전 링 -> 왼쪽 -> 오른쪽 -> 이전링 왼쪽 -> 이전링 오른쪽
        patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx - 1, prev_sector_idx));
        if (target_sector_idx > 0) {
            patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx, target_sector_idx - 1));
        }
        // 한바퀴 estimation이 다 되었을 때는 오른쪽도 가능!
        if (target_sector_idx == num_target_sectors - 1) {
            patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx, 0));
        }
        patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx - 1, prev_sector_idx_minus_1));
        patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx - 1, prev_sector_idx_plus_1));
    } else if (version == "V2") {
        // Ver. 2
        // 중심부가 가장 reliable하니, 앞/뒤 -> 좌우로 가는 게 더 나아 보임!
        int quadrant = target_sector_idx * 4 / num_target_sectors + 1;
        patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx - 1, prev_sector_idx));
        if (quadrant == 1 || quadrant == 3) { // 1, 3
            if (target_sector_idx != 0) {
                patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx, target_sector_idx - 1));
            }
            patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx - 1, prev_sector_idx_minus_1));
            patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx - 1, prev_sector_idx_plus_1));
        } else {
            int neighbor_idx = target_sector_idx + 1 == num_target_sectors? 0 : target_sector_idx + 1;
            patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx, neighbor_idx));
            patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx - 1, prev_sector_idx_plus_1));
            patch.neighbor_idx_set.emplace_back(make_pair(target_ring_idx - 1, prev_sector_idx_minus_1));
        }
    }
}


template<typename PointT>
inline
void GSAP<PointT>::calc_mean_and_std(const int ring_idx, float& mean, float& std) {
    if (sv_container_[ring_idx].size() < 2) {
        if (mean == NOT_INITIALIZED && std == NOT_INITIALIZED) {
            mean = sv_init_mean_;
            std = sv_init_std_;
        } else {
            return; // Do not update. Just use original values
        }
    } else if (sv_container_[ring_idx].size()){
        float sum = 0;
        float sqr_sum = 0;
        int N = sv_container_[ring_idx].size();
        for (auto& elem: sv_container_[ring_idx]) {
            sum += elem;
            sqr_sum += elem * elem;
        }
        mean = sum / N;
        std = sqrt(sqr_sum / N - mean * mean);

        // Interesting points: median is smaller than mean
//        size_t n = sv_container_[ring_idx].size() / 2;
//        vector<float> dummy(sv_container_[ring_idx].begin(), sv_container_[ring_idx].end());
//        sort(dummy.begin(), dummy.end());
//        cout << dummy[dummy.size() / 2] << endl;
    }
}

template<typename PointT>
inline
void GSAP<PointT>::update_sv_container() {
    // Past surface variables are removed
    for (auto& sv_deque: sv_container_) {
        while (sv_deque.size() > num_max_singular_values_) {
            sv_deque.pop_front();
        }
    }
}

template<typename PointT>
inline
void GSAP<PointT>::update_vicinity_planes() {
    // Past surface variables are removed
    int num_sectors_in_closest_ring = num_sectors_per_ring_[0];
    for (int sector_idx = 0; sector_idx < num_sectors_in_closest_ring; ++sector_idx) {
//        cout << "[Update "<< sector_idx <<"-th Vicinity]: " << NZM_[0][sector_idx].linearity_ << endl;
        if (NZM_[0][sector_idx].linearity_ < linearity_thr_ && NZM_[0][sector_idx].is_succeeded_ && NZM_[0][sector_idx].is_intercoupled_) {
            PlaneInfo& vicinity_plane = vicinity_planes_[sector_idx];
//            cout << "Prev.: " << vicinity_plane.d_ << ", " << vicinity_plane.singular_values_(2, 0) << endl;
            // Prediction
            float var1 = vicinity_plane.singular_values_(2, 0) + z_var_noise_;
            float var2 = NZM_[0][sector_idx].singular_values_(2, 0);

//            cout << var1 << " vs " << var2 << endl;
            // Calc. kalman gain
            float kalman_gain = var1 / (var1 + var2);
            // Update mean
            vicinity_plane.mean_(2, 0) = vicinity_plane.mean_(2, 0) + kalman_gain *
                    (NZM_[0][sector_idx].mean_(2, 0) - vicinity_plane.mean_(2, 0));
            vicinity_plane.d_ = -vicinity_plane.mean_(2, 0);

            // Update variance
            vicinity_plane.singular_values_(2, 0) = var1 - kalman_gain * var1;
            vicinity_plane.singular_values_(0, 0) = 3 * vicinity_plane.singular_values_(2, 0);
            vicinity_plane.singular_values_(1, 0) = 3 * vicinity_plane.singular_values_(2, 0);

//            cout << "After: " << vicinity_plane.d_ << ", " << vicinity_plane.singular_values_(2, 0) << endl;
        }
    }
}

template<typename PointT>
inline
void GSAP<PointT>::estimate_ground(
        const pcl::PointCloud<PointT> &cloud_in,
        pcl::PointCloud<PointT> &cloud_out,
        pcl::PointCloud<PointT> &cloud_nonground,
        pcl::PointCloud<PointT> &cloud_viz_nonground,
        double &time_taken) {
    poly_list_.header.stamp = ros::Time::now();
    if (!poly_list_.polygons.empty()) poly_list_.polygons.clear();
    if (!poly_list_.likelihood.empty()) poly_list_.likelihood.clear();

    static double start, t0, t1, t2, end;

    double t_total_ground   = 0.0;
    double t_total_estimate = 0.0;

    flush_all_patches();
    start = ros::Time::now().toSec();

    t1 = ros::Time::now().toSec();

    cloud_out.clear();
    cloud_nonground.clear();
    revert_pc.clear();
    reject_pc.clear();
    below_est_g.clear();

    /***
     * point cloud -> NZM
     * pc2czm is deprecated to prevent redundant copy of whole point cloud
     */
    for (auto const &pt : cloud_in.points) {
        if (is_in_boundary(pt)) {
            int ring_idx   = get_ring_idx(pt);
            int sector_idx = get_sector_idx(pt, ring_idx);
            if (ring_idx == 0) { // Closest ring: initially height can be estimated
                // 첫번째 ring: 로봇과 밀접하게 붙어있기 때문에 height에 대한 init guess 사용 가능하다는 전제
                if ((closest_ring_lower_z_thr_ < pt.z) && (closest_ring_upper_z_thr_ > pt.z)) {
                    NZM_[ring_idx][sector_idx].add_candidate(pt);
                } else {
                    NZM_[ring_idx][sector_idx].add_nonground(pt);
                }
            } else {
                NZM_[ring_idx][sector_idx].add_candidate(pt);
            }
        } else {
            cloud_nonground.points.emplace_back();
        }
    }

    if (mode_ == "original") {
        /***
         * Case 1. 원래 patchwork의 original 방식
         */
        estimate_ground_independently(cloud_out, cloud_nonground);
    } else if (mode_ == "intercoupled") {
        /***
          * Case 3. Patchwork를 bin간의 관계를 이용해 구하기
          */
        estimate_ground_intercoupled(cloud_out, cloud_nonground, cloud_viz_nonground);
    } else { throw invalid_argument("Wrong mode is coming!"); }


    end        = ros::Time::now().toSec();
    time_taken = end - start;

    /***
     * Polygon visualization 파트
     * 알고리즘과 상관 없음
     */
    visualization_msgs::MarkerArray marker_arr;
    if (visualize_) {
        init_seed_pc.clear();
        normals_.clear();
        if (mode_ != "intercoupled") {
            for (int j = 0; j < num_total_rings_; ++j) {
                int      num_sectors = num_sectors_per_ring_[j];
                for (int i           = 0; i < num_sectors; ++i) {
                    auto &patch = NZM_[j][i];
                    // Viz seed
                    patch.append_seeds_to(init_seed_pc);

                    // Viz GLE
                    double status_of_each_patch = patch.get_status();

//                    if (status_of_each_patch == NOT_ASSIGNED || status_of_each_patch == FEWER_NUM_PTS) { continue; }
                    auto                 normal   = patch.get_normal();
                    auto                 mean_pos = patch.get_mean_pos();
                    pcl::PointXYZINormal tmp_p;
                    tmp_p.x        = mean_pos(0, 0);
                    tmp_p.y        = mean_pos(1, 0);
                    tmp_p.z        = mean_pos(2, 0);
                    tmp_p.normal_x = normal(0, 0);
                    tmp_p.normal_y = normal(1, 0);
                    tmp_p.normal_z = normal(2, 0);

                    normals_.points.emplace_back(tmp_p);


                    auto polygons = set_polygons(j, i, num_sectors, 3);
                    polygons.header = poly_list_.header;
                    poly_list_.polygons.emplace_back(polygons);
                    poly_list_.likelihood.emplace_back(status_of_each_patch);
                }
            }
            if (poly_list_.polygons.size() > 0) {
                PlaneViz.publish(poly_list_);
            }
        } else {
            int idx_for_id = 0;
            for (int j = 0; j < num_total_rings_; ++j) {
                int      num_sectors = num_sectors_per_ring_[j];
                for (int i           = 0; i < num_sectors; ++i) {
                    auto &patch = NZM_[j][i];
                    // Viz seed
                    patch.append_seeds_to(init_seed_pc);

                    // Viz GLE
                    double status_of_each_patch = patch.get_status();

                    if (patch.is_succeeded_) {
                        auto                 normal   = patch.get_normal();
                        auto                 mean_pos = patch.get_mean_pos();
                        pcl::PointXYZINormal tmp_p;
                        tmp_p.x        = mean_pos(0, 0);
                        tmp_p.y        = mean_pos(1, 0);
                        tmp_p.z        = mean_pos(2, 0);
                        tmp_p.normal_x = normal(0, 0);
                        tmp_p.normal_y = normal(1, 0);
                        tmp_p.normal_z = normal(2, 0);
                        normals_.points.emplace_back(tmp_p);

                        visualization_msgs::Marker marker;
                        marker.header.frame_id = "/map";
                        marker.header.stamp = ros::Time::now();
                        marker.id = idx_for_id++;
                        marker.text = "(" + to_string(j) + ", " + to_string(i) + ")";
                        marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
                        marker.action = visualization_msgs::Marker::ADD;
                        marker.pose.position.x = tmp_p.x;
                        marker.pose.position.y = tmp_p.y;
                        marker.pose.position.z = tmp_p.z + 1.0;
                        marker.pose.orientation.w = 1.0;
//                    marker.scale.x = 1;
//                    marker.scale.y = 0.1;
                        marker.scale.z = 0.75;
                        marker.color.a = 1.0; // Don't forget to set the alpha!
                        marker.color.r = 0.0;
                        marker.color.g = 0.0;
                        marker.color.b = 0.0;
                        marker_arr.markers.push_back(marker);
                    }
                    if (status_of_each_patch != FEWER_NUM_PTS && status_of_each_patch != FEWER_NUM_PTS_INTERCOUPLED) {
                        if (PARTIAL_DRAW_ON) {
                            bool               is_in              = false;
                            for (auto &given_s_idx: given_sector_idxes) {
                                if (i == given_s_idx) {
                                    is_in = true;
                                    break;
                                }
                            }
                            if (j > 1 || (j == 1 && !is_in)) {
//                            if (j > 0 || !is_in) {
                                continue;
                            }
                        }

                        if (j > 12) {
                            continue;
                        }

                        auto polygons   = set_polygons(j, i, num_sectors, 3);
                        polygons.header = poly_list_.header;
                        poly_list_.polygons.emplace_back(polygons);
                        poly_list_.likelihood.emplace_back(status_of_each_patch);
                    }
                }
            }
            if (poly_list_.polygons.size() > 0) {
                PlaneViz.publish(poly_list_);
            }
        }
    }
    if (verbose_) {
        sensor_msgs::PointCloud2 cloud_ROS;
        if (marker_arr.markers.size() > 0) {
            cout << marker_arr.markers.size() << endl;
            viz_marker_pub.publish(marker_arr);
        }

        if ((revert_pc.points.size() > 0)) {
            pcl::toROSMsg(revert_pc, cloud_ROS);
            cloud_ROS.header.stamp    = ros::Time::now();
            cloud_ROS.header.frame_id = "/map";
            revert_pc_pub.publish(cloud_ROS);
        }

        if ((reject_pc.points.size() > 0)) {
            pcl::toROSMsg(reject_pc, cloud_ROS);
            cloud_ROS.header.stamp    = ros::Time::now();
            cloud_ROS.header.frame_id = "/map";
            reject_pc_pub.publish(cloud_ROS);
        }

        if ((below_est_g.points.size() > 0)) {
            pcl::toROSMsg(below_est_g, cloud_ROS);
            cloud_ROS.header.stamp    = ros::Time::now();
            cloud_ROS.header.frame_id = "/map";
            below_est_g_pub.publish(cloud_ROS);
        }

        if ((init_seed_pc.points.size() > 0)) {
            pcl::toROSMsg(init_seed_pc, cloud_ROS);
            cloud_ROS.header.stamp    = ros::Time::now();
            cloud_ROS.header.frame_id = "/map";
            init_seed_pub.publish(cloud_ROS);
        }

        if ((normals_.points.size() > 0)) {
            pcl::toROSMsg(normals_, cloud_ROS);
            cloud_ROS.header.stamp    = ros::Time::now();
            cloud_ROS.header.frame_id = "/map";
            normal_pub.publish(cloud_ROS);
        }

    }
}

template<typename PointT>
inline
void
GSAP<PointT>::estimate_ground_independently(
        pcl::PointCloud<PointT> &est_ground, pcl::PointCloud<PointT> &est_nonground) {
    /***
     * 1. 미리 normal vector와 regionwise_ground를 계산
     */
    for (int j = 0; j < num_total_rings_; ++j) {
        int      num_sectors = num_sectors_per_ring_[j];
        for (int i           = 0; i < num_sectors; ++i) {
            auto &patch = NZM_[j][i];
            if (patch.has_enough_pts(num_min_pts_)) { // 아래 GLE와 조건이 같아야 함!
                // ToDo. prior의 고도화가 필요해 보임: 내부가 Patchwork original과 같음
                float z_prior = get_z_prior(j, i);
                patch.arrange_candidates(z_prior);
                patch.estimate_regionwise_ground(num_lpr_, th_seeds_, num_iter_, th_dist_);
            } else {
                // 너무 수가 적으면 버린다?
                patch.update_status(FEWER_NUM_PTS);
                patch.append_candidates_to(est_nonground);
            }
        }
    }
    /***
     * 2. Ground Likelihood Estimation (GLE)
     * 모든 판단을 한 후에 patch에 status를 update해 둠
     * Viz를 for loop에서 분리함
     */
    for (int j = 0; j < num_total_rings_; ++j) {
        int      num_sectors = num_sectors_per_ring_[j];
        for (int i           = 0; i < num_sectors; ++i) {
            auto &patch = NZM_[j][i];
            if (patch.has_enough_pts(num_min_pts_)) {
                const float ground_z_vec       = patch.get_z_vec();
                const float ground_z_elevation = patch.get_z_elevation();
                const float surface_variable   = patch.get_surface_variable();

                double t_tmp2 = ros::Time::now().toSec();
                if (ground_z_vec < abs_uprightness_thr_) {
                    /***
                     * normal vector가 너무 누워있음 -> 다 reject
                     */
                    patch.update_status(TOO_TILTED);
                    patch.append_regionwise_ground_to(est_nonground);
                    patch.append_regionwise_nonground_to(est_nonground);
                } else { // satisfy uprightness
                    /***
                     * Elevation & Flatness
                     * Sensor와 충분히 가까이 있을때만 높이로 판별 가능
                     */
                    if (j < num_rings_of_interest_) {
                        if (ground_z_elevation > elevation_thr_[j]) {
                            if (flatness_thr_[j] > surface_variable) {
                                if (verbose_) {
                                    std::cout << "\033[1;36m[Flatness] Recovery operated. Check "
                                              << j
                                              << "th param. flatness_thr_: " << flatness_thr_[j]
                                              << " > "
                                              << surface_variable << "\033[0m" << std::endl;
                                    patch.append_regionwise_ground_to(revert_pc);
                                }
                                patch.update_status(FLAT_ENOUGH);
                                patch.append_regionwise_ground_to(est_ground);
                                patch.append_regionwise_nonground_to(est_nonground);
                            } else {
                                if (verbose_) {
                                    std::cout << "\033[1;34m[Elevation] Rejection operated. Check ("
                                              << j << ", " << i
                                              << ")-th param. of elevation_thr_: " << elevation_thr_[j]
                                              << " < "
                                              << ground_z_elevation << "\033[0m" << std::endl;
                                    patch.append_regionwise_ground_to(reject_pc);
                                }
                                patch.update_status(TOO_HIGH_ELEVATION);
                                patch.append_regionwise_ground_to(est_nonground);
                                patch.append_regionwise_nonground_to(est_nonground);
                            }
                        } else {
                            patch.update_status(LOW_ENOUGH_ELEVATION);
                            patch.append_regionwise_ground_to(est_ground);
                            patch.append_regionwise_nonground_to(est_nonground);
                        }
                    } else {
                        patch.update_status(UPRIGHT_ENOUGH);
                        patch.append_regionwise_ground_to(est_ground);
                        patch.append_regionwise_nonground_to(est_nonground);
                    }
                }
                double t_tmp3 = ros::Time::now().toSec();
            }
        }
    }
}

template<typename PointT>
inline
void
GSAP<PointT>::estimate_ground_intercoupled(
        pcl::PointCloud<PointT> &est_ground, pcl::PointCloud<PointT> &est_nonground,
        pcl::PointCloud<PointT> &cloud_viz_nonground) {
    /***
     * 1. 미리 normal vector와 regionwise_ground를 계산
     */

    INDEX++;

    float sv_mean = NOT_INITIALIZED;
    float sv_std = NOT_INITIALIZED;
    for (int j           = 0; j < num_total_rings_; ++j) {
        int      num_sectors = num_sectors_per_ring_[j];
        int   three_quarters = num_sectors * 3 / 4;
        int   seven_quarters = num_sectors * 7 / 4;

        calc_mean_and_std(j, sv_mean, sv_std);
        cout << "===============" << endl;
        cout << sv_mean << " , " << sv_std  << "(" << sv_container_[j].size() << ")" << endl;
        cout << "===============" << endl;

        for (int i           = 0; i < num_sectors; ++i) {
            int s_idx;
            float quadrant_rate = static_cast<float>(i) * 4.0 / num_sectors;
            if (quadrant_rate < 1.0 || (quadrant_rate < 3.0 && quadrant_rate >= 2.0)) { // 1, 3
                s_idx = i;
            } else if ( quadrant_rate < 2.0) {
                s_idx = num_sectors % 2 == 0? three_quarters - i - 1: three_quarters - i;
            } else {
                s_idx = num_sectors % 2 == 0? seven_quarters - i - 1: seven_quarters - i;
            }
            auto &patch = NZM_[j][s_idx];

            if (PARTIAL_DRAW_ON) {
                bool               is_in              = false;
                for (auto &given_s_idx: given_sector_idxes) {
                    if (s_idx == given_s_idx) {
                        is_in = true;
                        break;
                    }
                }
//                if (j > 0 || !is_in) {
                if (j > 1 || (j == 1 && !is_in)) {
                    cloud_viz_nonground += patch.candidates_;
                    cloud_viz_nonground += patch.regionwise_nonground_;
//                    if (j > 1 || (j == 1 && !is_in)) {
//                    if (j > 0 || !is_in) {
//                    est_nonground += patch.candidates_;
//                    est_nonground += patch.regionwise_nonground_;
                    continue;
                }
            }

            if (patch.has_enough_pts(num_min_pts_)) {
                prior_planes_.reserve(num_prior_patches_);
                set_prior_planes_near_the_patch(patch, prior_planes_, v_p_);
                // j > 0: Initialization을 위해...
                // KITTI 00 에서 옆의 벽이 높아서 recall이 낮은 경우가 있었음
                if (prior_planes_.size() > 0) { // && j < degeneracy_idx_) {
                    patch.truncated_inlier_selection(prior_planes_, tris_params_, v_t_);
                    if (gnc_on_ == "no_gnc") {
                        patch.estimate_advanced_regionwise_ground(num_iter_, th_dist_, rel_uprightness_thr_, sv_mean, sv_std,
                                                                  gseg_params_, revert_params_, v_g_);
                    } else if (gnc_on_ == "gnc") {
                        patch.estimate_advanced_regionwise_ground_GNC(num_iter_, th_dist_, rel_uprightness_thr_, revert_params_);
                    }
                    // Update sv containers
                    if (patch.is_succeeded_ && patch.is_intercoupled_
                    && (patch.status_ == UPRIGHT_ENOUGH || patch.status_ == REVERT_COMPLETE) ) {
                        if (!patch.is_definite_ground_) {
                            sv_container_[j].push_back(patch.singular_values_.minCoeff());
                        }
                    }

                } else {
                    if (use_independent_estimation_) {
                        if (v_i_) cout << "\033[1;37m[Independent] (" << j << ", " << s_idx << ") "; // << (patch.singular_values_(0, 0) - patch.singular_values_(1, 0)) / patch.singular_values_(0, 0) << endl;
                        patch.sort_cloud(patch.candidates_);
                        patch.estimate_regionwise_ground(num_lpr_, th_seeds_, num_iter_, th_dist_);

                        if (j >= degeneracy_idx_) {
                            if (v_i_) cout << patch.linearity_ << endl;
                            patch.compensate_linearity(0.75);
                        }
                        if (patch.get_z_vec() < abs_uprightness_thr_) {
                            if (v_i_) cout << patch.get_z_vec() << " => TOO TILTED!" << endl;
                            patch.update_status(TOO_TILTED_INDEPENDENT);
                            patch.is_succeeded_ = false;
                        } else {
                            patch.update_status(INDEPENDENT);

                            if (v_i_) {
                                cout << setprecision(5) << "Characteristics:" << patch.linearity_ << ", " << patch.planarity_ << " , ";
                                cout << setprecision(5) << 1 - patch.linearity_ - patch.planarity_ << "?!" << endl;
                                cout << patch.regionwise_ground_.size() << " <=> " << patch.regionwise_nonground_.size() << endl;
                            }
                            vector<PlaneInfo> close_planes;
                            get_closest_plane_info(j, s_idx, close_planes, 3);
                            float rel_z = FLT_MAX;
                            for (const auto& p: close_planes) {
                                rel_z = min(patch.mean_(2, 0) - p.mean_(2, 0), rel_z);
                            }
                            if (v_i_) cout << "and rel z: " << rel_z << endl;
                            float rel_z_thr = i >= degeneracy_idx_? 0.4: gseg_params_.rel_z_thr_independent;
                            if (rel_z > rel_z_thr) { // Not absolute. Lower parts exist in Seq.01
                                if (v_i_) cout << " => Too High!\033[0m" << endl;
                                patch.update_status(TOO_HIGH_ELEVATION);
                                patch.is_succeeded_ = false;
                            } else {
                                // 0.2 is important: if the coefficient is too large, the value becomes nagative
                                if (patch.singular_values_.minCoeff() < sv_mean - 0.2 * sv_std) {
                                    if (v_i_) cout << " => Considered as intercoupled!\033[0m" << endl;
                                    patch.is_succeeded_    = true;
                                    patch.is_intercoupled_ = true;
                                } else { patch.is_succeeded_ = true; }
                            }
                        }
                    } else {
                        patch.is_succeeded_         = false;
                        patch.regionwise_nonground_ = patch.candidates_;
                        patch.update_status(FEWER_NUM_PTS);
                    }
                }
            } else {
                // 너무 수가 적으면 버린다?
                patch.is_succeeded_ = false;
                patch.regionwise_nonground_ = patch.candidates_;
                patch.update_status(FEWER_NUM_PTS);
            }
//            if (j == 0) {
//                cout << patch.mean_(2, 0) << "!?!" << endl;
//            }
        }
    }

    if (use_vicinity_planes_) {
        update_vicinity_planes();
    }
    update_sv_container();

    /***
     * 모든 판단을 한 후에 patch에 status를 update해 둠
     * Viz를 for loop에서 분리함
     */
    for (int j           = 0; j < num_total_rings_; ++j) {
        const int num_sectors = num_sectors_per_ring_[j];
        for (int  i           = 0; i < num_sectors; ++i) {
            auto &patch = NZM_[j][i];

            if ((patch.get_status() != FEWER_NUM_PTS) && patch.is_succeeded_) {
                patch.append_regionwise_ground_to(est_ground);
                patch.append_regionwise_nonground_to(est_nonground);

                /*** 바닥의 너무 아래쪽 포인트들을 ground로 여기는게 Recall을 많이 올려줌
                 * 옮기는 것만 옮기고 patch의 normal에 영향을 주어서는 안될 듯
                 */
                patch.append_reflected_noises_to(est_ground);
                if (visualize_) {
                    patch.append_reverted_ground_to(revert_pc);
                    patch.append_reflected_noises_to(below_est_g);
                }
            }else {
                // 실패한 애들 -> 전부다 non-ground로!
//                if (patch.get_status() == TOO_TILTED) {
//                    patch.append_regionwise_nonground_to(est_nonground);
//                    patch.append_regionwise_ground_to(est_ground);
//                    patch.append_reflected_noises_to(est_ground); // 혹시나 짤리더라도 돌아옴!
//                } else {
                if (patch.regionwise_ground_.empty() && patch.regionwise_nonground_.empty()) {
                    patch.append_reflected_noises_to(est_ground); // 혹시나 짤리더라도 돌아옴!
                } else {
                    patch.append_reflected_noises_to(est_nonground);
                }
                // Important!!! It reduces some noises
                // Its role is similar to sub-clustering of LOAM variants
                if (patch.get_status() == FEWER_NUM_PTS_INTERCOUPLED) {
                    patch.append_regionwise_ground_to(est_ground);
                } else {
                    patch.append_regionwise_ground_to(est_nonground);
                }

                patch.append_regionwise_nonground_to(est_nonground);

                /***
                 * Below lines are important!
                 * 이유는 모르겠으나 flush가 가끔 안돼서 성공하지 못한 경우는 빠르게 flush함
                 */
                patch.regionwise_nonground_.clear();
                patch.reflected_noises_.clear();
                patch.regionwise_ground_.clear();
            }
        }
    }
}

template<typename PointT>
void
GSAP<PointT>::set_prior_planes_near_the_patch(Patch<PointT> &patch, vector<PlaneInfo> &prior_planes, bool verbose) {
    prior_planes.clear();
    PlaneInfo plane_info;

    int target_ring_idx = patch.ring_idx_;
    int num_target_sectors = num_sectors_per_ring_[target_ring_idx];

    if(verbose) {
        cout << "[Prior plane]: For " << target_ring_idx  << ", " << patch.sector_idx_ << ": ";
    }

    // In case of the closest ring
    if (target_ring_idx == 0) {
        // 1. Robot body 기준으로의 노말 벡터를 prior로 사용
        if (use_vicinity_planes_) {
            prior_planes.emplace_back(vicinity_planes_[patch.sector_idx_]);
        } else {
            prior_planes.emplace_back(root_plane_);
        }
        if (patch.sector_idx_ == 0) {
             return;
        }
    }

    // In General cases
    // Cand. in previous
    static int num_priors_thr = 2;

    int num_priors_in_prev_rings = 0;
    int i = 0;
    pair<int, int> cand_id = patch.neighbor_idx_set[i];
    if (cand_id.second >= 0) {
        auto &candidate = NZM_[cand_id.first][cand_id.second];
        if(verbose) {
            cout << "Cand (" << cand_id.first  << ", " << cand_id.second << ") => " << candidate.planarity_ << ", and thus" << is_available_as_prior_plane(candidate) << "?!" << endl;
        }
        if (is_available_as_prior_plane(candidate)) {
            candidate.get_patch_info(plane_info);
            prior_planes.emplace_back(plane_info);

            if (cand_id.first == target_ring_idx - 1) {
                ++num_priors_in_prev_rings;
            }
        }
    }
    // Check priority
    vector<pair<float, int> > priority_check;
    for (i = 1; i < patch.neighbor_idx_set.size(); ++i) {
        cand_id = patch.neighbor_idx_set[i];
        auto &candidate = NZM_[cand_id.first][cand_id.second];
        if (is_available_as_prior_plane(candidate)) {
            float score = candidate.planarity_;
            if (candidate.is_definite_ground_) {
               score += 1.0;
            }
            priority_check.push_back({score, i});
        }
    }
    sort(priority_check.begin(), priority_check.end());
    for (auto elem: priority_check) {
        cout << elem.first << " , " << elem.second << endl;
    }

    while (priority_check.size() > 0 && prior_planes.size() < num_prior_patches_ &&
                                        num_priors_in_prev_rings < num_priors_thr) {
        int priority_idx = priority_check.back().second;
        cand_id = patch.neighbor_idx_set[priority_idx];
        auto &candidate = NZM_[cand_id.first][cand_id.second];
        if(verbose) {
            cout << "" << cand_id.first  << ", " << cand_id.second << ", ";
        }
        candidate.get_patch_info(plane_info);
        prior_planes.emplace_back(plane_info);
        priority_check.pop_back();

        if (cand_id.first == target_ring_idx - 1) {
            ++num_priors_in_prev_rings;
        }
    }
    cout << "" << endl;
}

template<typename PointT>
inline
float GSAP<PointT>::xy2theta(const float &x, const float &y) { // 0 ~ 2 * PI
    if (y >= 0) {
        return atan2(y, x); // 1, 2 quadrant
    } else {
        return 2 * M_PI + atan2(y, x);// 3, 4 quadrant
    }
}

template<typename PointT>
inline
float GSAP<PointT>::xy2sqr_r(const float &x, const float &y) {
    return pow(x, 2) + pow(y, 2);
}

template<typename PointT>
inline
bool GSAP<PointT>::is_in_boundary(const PointT p) {
    static float min_sqr_r = sqr_boundary_ranges_.front();
    static float max_sqr_r = sqr_boundary_ranges_.back();
    float        sqr_r     = xy2sqr_r(p.x, p.y);

    if (min_sqr_r < sqr_r && sqr_r < max_sqr_r) { return true; }
    else { return false; }
}

template<typename PointT>
inline
int GSAP<PointT>::get_ring_idx(const PointT p) {
    static auto &b        = sqr_boundary_ranges_;
    float       sqr_r     = xy2sqr_r(p.x, p.y);
    int         bound_idx = lower_bound(b.begin(), b.end(), sqr_r) - b.begin();

    // bound_idx: idx whose value is larger than sqr_r
    // Thus, b[bound_idx - 1] < sqr_r < b[bound_idx]
    // And note that num_rings + 1 = b.size(); thus, minus 1 is needed
    return bound_idx - 1;
}

template<typename PointT>
inline
int GSAP<PointT>::get_sector_idx(const PointT p, const int ring_idx) {
    float theta       = xy2theta(p.x, p.y);
    int   num_sectors = num_sectors_per_ring_[ring_idx];
    float sector_size = 2.0 * M_PI / static_cast<float>(num_sectors);

    // min: for defensive programming
    return min(static_cast<int>(theta / sector_size), num_sectors - 1);
}

template<typename PointT>
inline
float GSAP<PointT>::get_z_prior(const int &ring_idx, const int &sector_idx) {
    if (ring_idx == 0 || ring_idx == 1) {
        return closest_ring_lower_z_thr_;
    } else {
        return sensor_height_ * -1.8;
    }
}


template<typename PointT>
inline
bool GSAP<PointT>::is_available_as_prior_plane(const Patch<PointT> &candidate) {
    static float perc = 0.3;
    if (candidate.ring_idx_ > 0) {
        if (candidate.is_succeeded_ && candidate.is_intercoupled_) {
            float dist_ratio  = candidate.get_dist_ratio();
            bool  is_centered = dist_ratio < perc;
            bool  is_flat_enough = candidate.planarity_ > 0.42;

            return (is_centered || is_flat_enough);
        } else { return false; }
    }

    return candidate.is_succeeded_ && candidate.is_intercoupled_;
}

//
template<typename PointT>
inline
geometry_msgs::PolygonStamped GSAP<PointT>::set_polygons(int ring_idx, int sector_idx, int num_sectors, int num_split) {
    geometry_msgs::PolygonStamped polygons;
    // Set point of polygon. Start from RL and ccw
    geometry_msgs::Point32        point;
    double                        sector_size       = 2.0 * M_PI / static_cast<double>(num_sectors);
    double                        angle_incremental = sector_size / static_cast<double>(num_split);
    // RL
    double                        r_len             = boundary_ranges_[ring_idx];
    double                        angle             = sector_idx * sector_size;

    point.x = r_len * cos(angle);
    point.y = r_len * sin(angle);
    point.z = MARKER_Z_VALUE;
    polygons.polygon.points.push_back(point);
    // RU
    r_len = boundary_ranges_[ring_idx + 1];
    point.x = r_len * cos(angle);
    point.y = r_len * sin(angle);
    point.z = MARKER_Z_VALUE;
    polygons.polygon.points.push_back(point);

    // RU -> LU
    for (int idx = 1; idx <= num_split; ++idx) {
        angle = angle + angle_incremental;
        point.x = r_len * cos(angle);
        point.y = r_len * sin(angle);
        point.z = MARKER_Z_VALUE;
        polygons.polygon.points.push_back(point);
    }

    r_len = boundary_ranges_[ring_idx];
    point.x = r_len * cos(angle);
    point.y = r_len * sin(angle);
    point.z = MARKER_Z_VALUE;
    polygons.polygon.points.push_back(point);

    for (int idx = 1; idx < num_split; ++idx) {
        angle = angle - angle_incremental;
        point.x = r_len * cos(angle);
        point.y = r_len * sin(angle);
        point.z = MARKER_Z_VALUE;
        polygons.polygon.points.push_back(point);
    }

    return polygons;
}


#endif
