//
// Created by shapelim on 21. 12. 1..
//

#ifndef PATCHWORK_BIN_H
#define PATCHWORK_BIN_H

#include <patchwork/utils.hpp>
#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <jsk_recognition_msgs/PolygonArray.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>
#include <Eigen/Dense>
#include <boost/format.hpp>
#include <pcl/common/common.h>
#include <pcl/point_types.h>
#include <pcl/common/centroid.h>
#include <pcl/conversions.h>
#include <algorithm>
#include <omp.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/vector_average.h>
#include <numeric>
#include <deque>

#define MARKER_Z_VALUE -2.2
#define UPRIGHT_ENOUGH 0.55
#define FLAT_ENOUGH 0.2
#define NOT_FLAT_ENOUGH 0.8
#define TOO_HIGH_ELEVATION 0.0
#define LOW_ENOUGH_ELEVATION 0.5
#define NOT_ASSIGNED 0.75


#define RELATIVELY_DRAMATIC 0.0
#define FEWER_NUM_PTS 0.69
#define FEWER_NUM_PTS_INTERCOUPLED 0.65
#define TOO_TILTED 1.0
#define TOO_TILTED_INDEPENDENT 0.99
#define TOO_TILTED_AFTER_MERGE 0.7
#define INDEPENDENT 0.4 // 진녹색


// For intercoupled
#define BECOME_MORE_STRICT 2.0 // visualization 안됨
#define BECOME_MORE_STRICT_YET_SUCEEDED 0.81 // 노르스름한 색
#define REVERT_COMPLETE 0.01
//#define UPRIGHT_ENOUGH 0.55
//#define FLAT_ENOUGH 0.02
//#define LOW_ENOUGH_ELEVATION 0.5
//#define NOT_ASSIGNED 0.75

#define NUM_HEURISTIC_MAX_PTS_IN_PATCH 5000
#define NUM_HEURISTIC_MAX_PTS_IN_CLOUD 150000

using namespace std;
using Eigen::MatrixXf;
using Eigen::JacobiSVD;
using Eigen::VectorXf;

// Just for debugging
bool   is_not_initialized = true;
string LOG_DIR            = "/home/shapelim/data/";
string SEQ;
int    INDEX              = -1; // Just for convenience

template<typename PointT>
bool z_cmp(PointT a, PointT b) {
    return a.z < b.z;
}

struct PlaneInfo {
    MatrixXf normal_;
    VectorXf singular_values_;
    VectorXf mean_;
    float    d_;
    bool     is_definite_ground_ = false;
    float    linearity_;
    float    planarity_;
};

struct TRISParams {
    int   num_min_pts_for_pseudo_occupancy;
    float rel_z_for_pseudo_occupancy;
    float scale_for_pseudo_occupancy;
    float upper_bound;
    float lower_bound;
    int   ng_to_g_ring_idx;
    int   degeneracy_idx;
};

struct GSegParams {
    float rel_z_thr_independent;
    float rel_z_thr_intercoupled;

    int   zone1_idx;
    int   zone2_idx;
};

struct RevertParams {
    bool     revert_on;

    int      ring_idx_boundary;
    int      num_min_pts_for_revert;
    float    th_revert_prob;
    float    th_mean_max_res;
    float    th_mean_inner_product;
    float    th_mean_singular_value;
    float    th_mean_surface_variable;

    float    th_std_max_res;
    float    th_std_inner_product;
    float    th_std_larger_singular_value;
    float    th_std_smaller_singular_value;
    float    th_std_surface_variable;

};

template<typename PointT>
class Patch {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    double center_x_;
    double center_y_;
    double min_len_;

    int  ring_idx_;
    int  sector_idx_;
    int  num_min_init_pts_;
    int  num_min_revert_pts_;
    bool is_succeeded_ = false;
    bool is_intercoupled_ = false;
    bool is_definite_ground_ = false;
    bool is_definite_ground_in_prior = false;
    bool is_single_line_ = false;
    bool do_obstacles_exist_ = false;

    vector<float> initial_gnc_weights_;
    vector<pair<int, int> > neighbor_idx_set;
    double      status_;

    float    upper_bound_;
    float    min_residual_;
    float    max_residual_;

    float    min_global_residual_;
    float    max_global_residual_;

    float    global_residual_diff_;
    float    local_residual_diff_;

    float    rel_z_;
    float    rel_normal_;
    float    d_;
    MatrixXf normal_;
    VectorXf singular_values_;
    float    th_dist_d_;
    float    decay_rate_uprightness_;
//    float    th_rel_z_th_rel_z_to_switch_noise_to_init_ground_;
    float    soft_normal_bound_ = 0.5; // sin(30 deg.)

    float    linearity_;
    float    planarity_;

//    int      th_num_pts_to_switch_noise_to_init_ground_;

    // For close ring revert
    bool close_rings_revert_on_;
    int close_idx_boundary_;
    int num_min_close_revert_pts_;
    int num_max_close_revert_pts_;

    Eigen::Matrix<float, 1, Eigen::Dynamic> init_residuals_;
    Eigen::Matrix<float, 1, Eigen::Dynamic> weights_; // For GNC
    Eigen::Matrix<float, 1, Eigen::Dynamic> sqr_residuals_; // For GNC
    Eigen::Matrix3f                         cov_;
    Eigen::Vector3f                         mean_;

    pcl::PointCloud<PointT> candidates_;
    pcl::PointCloud<PointT> reflected_noises_; // 바닥 아래 noise들
    pcl::PointCloud<PointT> init_seeds_;
    pcl::PointCloud<PointT> regionwise_ground_;
    pcl::PointCloud<PointT> potential_nonground_; // estimated as nonground in regionwise_ground_
    pcl::PointCloud<PointT> regionwise_nonground_;
    pcl::PointCloud<PointT> reverted_ground_;

    Eigen::Matrix<float, Eigen::Dynamic, 3> cand_xyz_;

    PlaneInfo prior_weighted_plane_;

    Patch<PointT>(int ring_idx, int sector_idx, ros::NodeHandle *nh) {
        nh->param<float>("/gsap/bin/decay_rate_uprightness", decay_rate_uprightness_, 0.7);
        nh->param<int>("/gsap/bin/num_min_init_pts", num_min_init_pts_, 12);

        nh->param<bool>("/gsap/bin/close_rings_revert_on", close_rings_revert_on_, true);
        nh->param<int>("/gsap/bin/revert_z1/close_idx_boundary", close_idx_boundary_, 3);
        nh->param<int>("/gsap/bin/revert_z1/num_min_close_revert_pts", num_min_close_revert_pts_, 25);
        nh->param<int>("/gsap/bin/revert_z1/num_max_close_revert_pts", num_max_close_revert_pts_, 70);

        ring_idx_   = ring_idx;
        sector_idx_ = sector_idx;

        candidates_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        reflected_noises_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        init_seeds_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        regionwise_ground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        potential_nonground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        regionwise_nonground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        reverted_ground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
    };

    Patch<PointT>(int ring_idx, int sector_idx) {
        ring_idx_   = ring_idx;
        sector_idx_ = sector_idx;

        candidates_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        reflected_noises_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        init_seeds_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        regionwise_ground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        potential_nonground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        regionwise_nonground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        reverted_ground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
    };

    inline
    void clear() {
        is_succeeded_ = false;
        is_intercoupled_ = false;
        is_definite_ground_ = false;
        is_definite_ground_in_prior = false;
        is_single_line_ = false;
        do_obstacles_exist_= false;

        status_       = NOT_ASSIGNED;
        // candidates_: the points located a certain bin
        candidates_.clear();
        // Outputs: reflected_noises, regionwise_ground_, regionwise_nonground_
        reflected_noises_.clear();
        regionwise_ground_.clear();
        regionwise_nonground_.clear();
        // For visualization
        init_seeds_.clear();
        reverted_ground_.clear();

        min_residual_ = std::numeric_limits<float>::max();
        max_residual_ = std::numeric_limits<float>::lowest();

        min_global_residual_ = std::numeric_limits<float>::max();
        max_global_residual_ = std::numeric_limits<float>::lowest();

    }

    inline
    void add_candidate(const PointT& p) {
        candidates_.points.emplace_back(p);
    }

    inline
    void add_nonground(const PointT& p) {
        regionwise_nonground_.points.emplace_back(p);
    }

    inline
    bool has_enough_pts(const int num_min_pts) {
        if (candidates_.points.size() > num_min_pts) { return true; }
        else { return false; }
    }

    inline
    bool is_in_closest_ring() {
        return ring_idx_ == 0;
    }

    inline
    void sort_cloud(pcl::PointCloud<PointT>& cloud) {
        sort(cloud.points.begin(), cloud.end(), z_cmp<PointT>);
    }

    inline
    void arrange_candidates(float z_prior) {
        /***
         * Step 1. Z 기반으로 sorting
         * Step 2. Height를 기반으로 noise를 제거해줘야 함!!
         */
        sort_cloud(candidates_);
        if (z_prior < candidates_.points[0].z) { return; }
        else {
            auto     it = candidates_.points.begin();
            for (int i  = 0; i < candidates_.points.size(); i++) {
                if (candidates_.points[i].z < z_prior) {
                    it++;
                } else {
                    break;
                }
            }
            candidates_.points.erase(candidates_.points.begin(), it);
        }
    }

    inline
    void extract_initial_seeds(const int num_lpr, const float th_seeds, pcl::PointCloud<PointT> &init_cloud) {
        float sum = 0;
        int   cnt = 0;

        /***
         * Lowest pts를 기반으로 init seeds를 뽑음
         *
         */

        int N = candidates_.points.size();


        int MIN_N = min(N, num_lpr);

        /***
         * Case 1. Average
         */
//        for (int i = 0; i < MIN_N; i++) {
//            sum += candidates_.points[i].z;
//        }
//
//        float lpr_height = MIN_N != 0 ? sum / MIN_N : 0;// in case divide by 0

        /***
         * Case 2. Median Selection
         */
        float lpr_height = candidates_.points[MIN_N / 2].z;
        cout << "Median: " << lpr_height << endl;

        /***
         *  Filter those height is less than lpr.height+th_seeds_
         * Redundant한 복사를 막고자 index만 받아옴
         */
        init_cloud.clear();
        init_cloud.reserve(N); // Large enough
        for (int i = 2; i < N; i++) {
            if (candidates_.points[i].z < lpr_height - 1.5 * min_len_) {
                cout << i << " th, " << candidates_.points[i].z << "skip!" << endl;
                continue;
            } else if (candidates_.points[i].z < lpr_height + th_seeds) {
                init_cloud.points.emplace_back(candidates_.points[i]);
            } else { break; } // Because pointcloud is ordered w.r.t z values
        }
        // init_seeds visualization을 위해 저장
        init_seeds_.points.clear();
        init_seeds_ = init_cloud;
    }

    inline
    void calc_residuals(const PlaneInfo &plane, const pcl::PointCloud<PointT> &cloud, Eigen::Matrix<float, 1, Eigen::Dynamic> &residuals) {
        residuals.resize(1, cloud.points.size());
        int             idx = 0;
        for (const auto &pt: cloud.points) {
            float residual = plane.normal_(0, 0) * pt.x
                             + plane.normal_(1, 0) * pt.y
                             + plane.normal_(2, 0) * pt.z + plane.d_;
            residuals(0, idx) = residual;
            idx++;
        }
    }
    inline void estimate_weighted_plane_(
            const pcl::PointCloud<PointT> &ground,
            const Eigen::Matrix<float, 1, Eigen::Dynamic> &weights) {

        pcl::VectorAverage<float, 3> WeightedAvg = pcl::VectorAverage<float, 3>();
        for (int                     n           = 0; n < ground.points.size(); ++n) {
            const auto &p = ground.points[n];
            WeightedAvg.add(Eigen::Vector3f(p.x, p.y, p.z), weights_(0, n));
        }
        cov_  = WeightedAvg.getCovariance();
        mean_ = WeightedAvg.getMean();
        // Singular Value Decomposition: SVD
        Eigen::JacobiSVD<Eigen::MatrixXf> svd(cov_, Eigen::DecompositionOptions::ComputeFullU);
        singular_values_ = svd.singularValues();

        // use the least singular vector as normal
        normal_ = (svd.matrixU().col(2));
        if (normal_(2, 0) < 0) { // z 방향 vector는 위를 무조건 향하게 해야 함
            normal_ = -normal_;
        }

        // according to normal.T*[x,y,z] = -d
        d_ = -(normal_.transpose() * mean_)(0, 0);
        // set distance threhold to `th_dist - d`
    }

    inline void weighted_average(const vector<PlaneInfo> &prior_planes, PlaneInfo& averaged_plane) {
        // First of all, check neighboring bins
        int N = prior_planes.size();
        for (int n = 0; n < N; ++n) {
            if (prior_planes[n].is_definite_ground_) {
                is_definite_ground_in_prior = true;
                break;
            }
        }

        // Check the edge case
        if (N == 1) {
            averaged_plane = prior_planes[0];
            if (ring_idx_ == 0) {
                cout << averaged_plane.d_ << " and " << averaged_plane.singular_values_(2, 0) << endl;
            }
            return;
        }

        vector<float> informations(N, 0.0);
        float weight_denominator = 0.0;
        static float epsilon = 0.0000000001; // For avoiding zero-dividing issue
        for (int n = 0; n < N; ++n) {
            // Version 1. using min. singular value
            // But sometimes tiny cluster cause wrongly estimate plane
//            informations[n] = 1.0 / (prior_planes[n].singular_values_(2, 0) + epsilon);
            // version 2. Using planarity
            informations[n] = prior_planes[n].planarity_;
            weight_denominator += informations[n];
        }

        for (int n = 0; n < N; ++n) {
            informations[n] /= weight_denominator;
        }

        // 2. Calc weighted mean & vector;
        averaged_plane.normal_ = Eigen::Vector3f::Zero();
        averaged_plane.mean_ = Eigen::Vector3f::Zero();
        averaged_plane.singular_values_ = Eigen::Vector3f::Zero();
        for (int n = 0; n < N; ++n) {
//            if (ring_idx_ == 0 && sector_idx_ == 15) {
//                cout<<prior_planes[n].normal_<<endl;
//            }
            averaged_plane.normal_ += informations[n] * prior_planes[n].normal_;
            averaged_plane.mean_ += informations[n] * prior_planes[n].mean_;
            averaged_plane.singular_values_ += prior_planes[n].singular_values_;
//            if (ring_idx_ < 4) {
//                cout << prior_planes[n].singular_values_ << endl;
//            }
        }

        // 3. Normalize the normal vector
        float norm = averaged_plane.normal_.norm();
        averaged_plane.normal_ /= norm;

        // 4. Calc
        averaged_plane.d_       = -(averaged_plane.normal_.transpose() * averaged_plane.mean_)(0, 0);
    }


    inline void estimate_plane_(const pcl::PointCloud<PointT> &ground) {
        Eigen::Vector4f pc_mean;
        pcl::computeMeanAndCovarianceMatrix(ground, cov_, pc_mean);
        // Singular Value Decomposition: SVD
        Eigen::JacobiSVD<Eigen::MatrixXf> svd(cov_, Eigen::DecompositionOptions::ComputeFullU);
        singular_values_ = svd.singularValues();

        linearity_ = ( singular_values_(0) - singular_values_(1) ) / singular_values_(0);
        planarity_ = ( singular_values_(1) - singular_values_(2) ) / singular_values_(0);

        // use the least singular vector as normal
        normal_ = (svd.matrixU().col(2));
        if (normal_(2, 0) < 0) { // z 방향 vector는 위를 무조건 향하게 해야 함
            normal_ = -normal_;
        }
        // mean ground seeds value
        mean_   = pc_mean.head<3>();

        // according to normal.T*[x,y,z] = -d
        d_         = -(normal_.transpose() * mean_)(0, 0);
    }

    inline void estimate_plane_(const pcl::PointCloud<PointT> &ground, PlaneInfo& p) {
        Eigen::Vector4f pc_mean;
        Eigen::Matrix3f cov;
        pcl::computeMeanAndCovarianceMatrix(ground, cov, pc_mean);

        // Singular Value Decomposition: SVD
        Eigen::JacobiSVD<Eigen::MatrixXf> svd(cov, Eigen::DecompositionOptions::ComputeFullU);
        p.singular_values_ = svd.singularValues();

        p.linearity_ = ( p.singular_values_(0) - p.singular_values_(1) ) / p.singular_values_(0);
        p.planarity_ = ( p.singular_values_(1) - p.singular_values_(2) ) / p.singular_values_(0);

        cout << svd.matrixU() << endl;

        // use the least singular vector as normal
        p.normal_ = (svd.matrixU().col(2));
        if (p.normal_(2, 0) < 0) { // z 방향 vector는 위를 무조건 향하게 해야 함
            p.normal_ = -p.normal_;
        }
        // mean ground seeds value
        p.mean_ = pc_mean.head<3>();
        // according to normal.T*[x,y,z] = -d
        p.d_ = -(p.normal_.transpose() * p.mean_)(0, 0);

    }

    inline void estimate_vertical_plane_(const pcl::PointCloud<PointT>& nonground, PlaneInfo& p, float verticalness, float th_d, bool verbose=false) {
        static float h_margin = 1.4;
        pcl::PointCloud<PointT> candidates;
        candidates.reserve(nonground.size());

        int N = nonground.size();
        Eigen::Matrix<float, Eigen::Dynamic, 3> ng_xyz;
        ng_xyz.resize(N, 3);
        for (int i = 0; i < N; ++i) {
            const auto &pt = nonground.points[i];
            ng_xyz.row(i) << pt.x, pt.y, pt.z;
            if (prior_weighted_plane_.mean_(2, 0) + h_margin > pt.z) {
                candidates.points.emplace_back(pt);
            }
        }

        for (int i = 0; i < 3; ++i ) {
            estimate_plane_(candidates, p);
            if (verbose) cout << "[V-GPF]: " << candidates.size() << " ==> " << endl;
            if (verbose) cout << "[V-GPF]: " << p.normal_.transpose() << endl;
            if (verbose) cout << "[V-GPF]: " << p.mean_.transpose() << endl;
            if (abs(p.normal_(2, 0)) < verticalness) {
                if (verbose) cout << "[V-GPF]: " << i << "th: break" << endl;
                return;
            }
            Eigen::VectorXf d_est = ng_xyz * p.normal_;
            candidates.clear();
            for (int j = 0; j < d_est.rows(); ++j) {
                if (d_est(j, 0) + p.d_ < th_d && d_est(j, 0) + p.d_ > - th_d) {
                    candidates.points.emplace_back(nonground.points[j]);
                }
            }
        }
    }

    inline void estimate_posteriori_plane_(const pcl::PointCloud<PointT> &ground) {
        // 별로 안 좋은듯...
        Eigen::Vector4f pc_mean;
        pcl::computeMeanAndCovarianceMatrix(ground, cov_, pc_mean);
        // Singular Value Decomposition: SVD
        Eigen::JacobiSVD<Eigen::MatrixXf> svd(cov_, Eigen::DecompositionOptions::ComputeFullU);
        singular_values_ = svd.singularValues();

        float denorm = 1.0 / singular_values_.minCoeff() + 1.0 / prior_weighted_plane_.singular_values_.minCoeff();
        // use the least singular vector as normal
        normal_ = (svd.matrixU().col(2));
        if (normal_(2, 0) < 0) { // z 방향 vector는 위를 무조건 향하게 해야 함
            normal_ = -normal_;
        }
        // mean ground seeds value
        mean_   = pc_mean.head<3>();

        normal_ = (1.0 / singular_values_.minCoeff() * normal_ + 1.0 / prior_weighted_plane_.singular_values_.minCoeff() * prior_weighted_plane_.normal_) / denorm;
        // mean ground seeds value
//        mean_ = (1.0 / singular_values_.minCoeff() * mean_ + 1.0 / prior_weighted_plane_.singular_values_.minCoeff() * prior_weighted_plane_.mean_) / denorm;

        // according to normal.T*[x,y,z] = -d
        d_         = -(normal_.transpose() * mean_)(0, 0);
    }

    inline void estimate_posteriori_plane_(const pcl::PointCloud<PointT> &ground, PlaneInfo& p) {
        Eigen::Vector4f pc_mean;
        Eigen::Matrix3f cov;
        pcl::computeMeanAndCovarianceMatrix(ground, cov, pc_mean);
        // Singular Value Decomposition: SVD
        Eigen::JacobiSVD<Eigen::MatrixXf> svd(cov, Eigen::DecompositionOptions::ComputeFullU);
        p.singular_values_ = svd.singularValues();

        float denorm = 1.0 / p.singular_values_.minCoeff() + 1.0 / prior_weighted_plane_.singular_values_.minCoeff();
        // use the least singular vector as normal
        p.normal_ = (svd.matrixU().col(2));
        if (p.normal_(2, 0) < 0) { // z 방향 vector는 위를 무조건 향하게 해야 함
            p.normal_ = -p.normal_;
        }
        p.normal_ = (1.0 / p.singular_values_.minCoeff() * p.normal_ + 1.0 / prior_weighted_plane_.singular_values_.minCoeff() * prior_weighted_plane_.normal_) / denorm;
        // mean ground seeds value
        p.mean_ = (1.0 / p.singular_values_.minCoeff() * pc_mean.head<3>() + 1.0 / prior_weighted_plane_.singular_values_.minCoeff() * prior_weighted_plane_.mean_) / denorm;

        // according to normal.T*[x,y,z] = -d
        p.d_ = -(p.normal_.transpose() * p.mean_)(0, 0);
    }

    inline void update_th_plane_dist(float th_plane_dist, float& d_est) {
        th_dist_d_ = th_plane_dist - d_est;
    }

    inline void estimate_regionwise_ground(
            const int num_lpr, const float th_seeds, const int num_iter, const float th_plane_dist,
            bool is_closest_to_origin = false) {
        /***
         * 1. inittial_seeds를 세팅한다
         * NOTE: a) 첫번 째 ring일 때는 이미 아래쪽 노이즈가 margin에 의해서 필터링되어 있음
         *       b) candidates가 미리 sorting 되어 있어야 함!
         * ToDo: 다른 bin들의 lowest error는 어떻게 없애줘야할까...
         */
        extract_initial_seeds(num_lpr, th_seeds, regionwise_ground_);
        // 2. Set candidates
        int N = candidates_.points.size();
        cand_xyz_.resize(N, 3);
        for (int i = 0; i < N; ++i) {
            const auto &p = candidates_.points[i];
            cand_xyz_.row(i) << p.x, p.y, p.z;
        }

        // 3. Iterative ground plane fitting
        for (int i = 0; i < num_iter; i++) {
            estimate_plane_(regionwise_ground_);
            update_th_plane_dist(th_plane_dist, d_);
            regionwise_ground_.clear();

            // ground plane model
            Eigen::VectorXf d_est = cand_xyz_ * normal_;
            // threshold filter
            for (int        r     = 0; r < d_est.rows(); r++) {
                if (i < num_iter - 1) {
                    if (d_est[r] < th_dist_d_ && (d_est[r] + d_ > -3 * th_plane_dist)) {
                        regionwise_ground_.points.emplace_back(candidates_[r]);
                    } else {
                        if (is_closest_to_origin) {
                            // 가장 원점에 가까운 bin -> normal vector is close to [0, 0, 1]
                            break;
                        }
                    }
                } else { // Final stage
                    if (d_est[r] < th_dist_d_) {
                        regionwise_ground_.points.push_back(candidates_[r]);
                    } else {
                        if (i == num_iter - 1) {
                            regionwise_nonground_.push_back(candidates_[r]);
                        }
                    }
                }
            }
        }
        is_succeeded_ = true; // ground seg.를 완료했다는 flag
    }

    inline void cloud2eigen(pcl::PointCloud<PointT> &cloud, Eigen::Matrix<float, Eigen::Dynamic, 3> &cand_xyz) {
        int N = cloud.points.size();
        cand_xyz.resize(N, 3);
        for (int i = 0; i < N; ++i) {
            const auto &p = cloud.points[i];
            cand_xyz.row(i) << p.x, p.y, p.z;
        }
    }

    inline
    void truncated_inlier_selection(vector<PlaneInfo> &prior_planes, const TRISParams& tris_params, bool verbose=true) {
        /***
         * NOTE: 그리고 lower bound 값은 음수여야 함! (plane 기준임)
         *     : 이미 candidates_와 regionwise_nonground로 나뉘어져 있음!
         */

        Eigen::Matrix<float, 1, Eigen::Dynamic> residuals;

        weighted_average(prior_planes, prior_weighted_plane_);
        calc_residuals(prior_weighted_plane_, candidates_, residuals);

        upper_bound_ = tris_params.upper_bound;
        for (int idx                  = 0; idx < candidates_.points.size(); ++idx) {
            float  &residual = residuals(0, idx);
            PointT &pt       = candidates_.points[idx];

            if (residual < tris_params.lower_bound) { // lower bound 보다 아래 있으면?? -> Noise!
                reflected_noises_.points.emplace_back(pt);
            } else if (residual < tris_params.upper_bound) {
                regionwise_ground_.points.emplace_back(pt);

                if (residual < min_residual_) { min_residual_ = residual; }
                if (residual > max_residual_) { max_residual_ = residual; }
            } else {
                regionwise_nonground_.points.emplace_back(pt);
            }

            if (residual < min_global_residual_) { min_global_residual_ = residual; }
            if (residual > max_global_residual_) { max_global_residual_ = residual; }

        }
        global_residual_diff_ = max_global_residual_ - min_residual_;
        local_residual_diff_ = max_residual_ - min_residual_;
        // Debugging
        if (verbose) {
            cout << "\033[1;33m[TRIS] (" << ring_idx_ << ", " << sector_idx_ << "): ";
            cout << global_residual_diff_ << " | " << local_residual_diff_ << "( " << max_residual_ << "), ";
            cout << " => " << reflected_noises_.size() << " vs " << regionwise_ground_.size() << " vs " << regionwise_nonground_.size()
                 << endl;

            pcl::PointCloud<PointT> TP;
            pcl::PointCloud<PointT> FP;
            discern_ground(regionwise_ground_, TP, FP);
            if (FP.points.size() > 0 && TP.points.size() > 0) {
                Eigen::Matrix<float, 1, Eigen::Dynamic> residuals_tp;
                Eigen::Matrix<float, 1, Eigen::Dynamic> residuals_fp;
                calc_residuals(prior_weighted_plane_, TP, residuals_tp);
                calc_residuals(prior_weighted_plane_, FP, residuals_fp);
                cout << "\033[1;35mTP residual (" << TP.size() << ") : " << residuals_tp.minCoeff() << " ~ " << residuals_tp.maxCoeff()
                     << endl;
                cout << "FP residual (" << FP.size() << ") : " << residuals_fp.minCoeff() << " ~ " << residuals_fp.maxCoeff() << "\033[0m"
                     << endl;
            } else {
                cout << "\033[1;35m#. TP:  " << TP.size() << ", " << " #. FP:  " << FP.size() << "\033[0m" << endl;
            }
        }

        // Pseudo-occupancy check
        // ring_idx가 충분히 커졌을 때 - 잘 안될 때가 있음!
        if (reflected_noises_.points.size() > tris_params.num_min_pts_for_pseudo_occupancy) {
            PointT min_pt, max_pt;
            pcl::getMinMax3D (reflected_noises_, min_pt, max_pt);
            // If it is too thick, then it may not be a reflected noises

            float mean_to_min_dist = prior_weighted_plane_.mean_(2, 0) - min_pt.z;
            if (verbose) {
                cout << "[Pseudo Occupancy]: ";
                cout << max_pt.z << " - " <<  min_pt.z << " => " << max_pt.z - min_pt.z << "| ";
                cout << mean_to_min_dist << " vs " << -tris_params.scale_for_pseudo_occupancy * tris_params.lower_bound << endl;
                cout << "" << endl;
                cout << (max_pt.z - min_pt.z > tris_params.rel_z_for_pseudo_occupancy) << endl;
                cout << (mean_to_min_dist < - tris_params.scale_for_pseudo_occupancy * tris_params.lower_bound) << endl;
            }
            if (max_pt.z - min_pt.z > tris_params.rel_z_for_pseudo_occupancy &&
             mean_to_min_dist < - tris_params.scale_for_pseudo_occupancy * tris_params.lower_bound) {
                // regionwise_ground_ -> regionwise_nonground_
                Patch<PointT> subpatch = Patch<PointT>(ring_idx_, sector_idx_);
                subpatch.clear();
                subpatch.candidates_ = reflected_noises_;
                subpatch.candidates_ += regionwise_ground_;
                subpatch.sort_cloud(subpatch.candidates_);
                subpatch.estimate_regionwise_ground(20, 0.4, 3, 0.15);
                if (subpatch.get_z_vec() > 0.707) {
                    regionwise_ground_ = subpatch.regionwise_ground_;
                    regionwise_nonground_ += subpatch.regionwise_nonground_;
                    is_definite_ground_in_prior = false;
                    cout << "\033[1;32m[Pseudo Occupancy]: Samples are reassigned!: ";
                    cout << "0, " << regionwise_ground_.size() << ", " << regionwise_nonground_.size() << "\033[0m" << endl;
                } else {
                    for (const auto& pt: regionwise_ground_.points) {
                        regionwise_nonground_.points.emplace_back(pt);
                    }
                    sort_cloud(reflected_noises_);
                    regionwise_ground_.clear();
                    int num_below_g = reflected_noises_.points.size();
                    int num_lpr = num_below_g > 20? 20 : num_below_g;
                    for (int i = 0; i < num_below_g; ++i) {
                        const auto& pt = reflected_noises_.points[i];
                        if (i < num_lpr) {
                            regionwise_ground_.points.emplace_back(pt);
                        } else {
                            regionwise_nonground_.points.emplace_back(pt);
                        }
                    }
                }
                reflected_noises_.clear();
            }
        }
        calc_residuals(prior_weighted_plane_, regionwise_ground_, init_residuals_);

//        cout << regionwise_ground_.points.size() << endl;
//        cout << init_residuals_.rows()<< " , " << init_residuals_.cols() << endl;

        /*** 만약 initial seed 선정에 실패해서 모두다 regionwise_nonground로 가버리면?
         * 어쩔수 없이 모든 포인트를 regionwise_ground로 선정해야 할듯?
         * 주석처리한 결과
         * wo_veg: P: 92.93 -> 93.31, 97.478 -> 97.45
         * w_veg:  P: 90.16 -> 91.11, 94.246 -> 94.22
         * */
//        if (regionwise_ground_.points.empty()) {
        if (regionwise_ground_.points.size() < num_min_init_pts_) {
            // For the closest ring, it should not performed!
            if (ring_idx_ < tris_params.ng_to_g_ring_idx && ring_idx_ > 0 && regionwise_nonground_.size() > 20) { // when ring idx is close enough
                cout << "A few points so switch is performed!!!!" << endl;
                Patch<PointT> subpatch = Patch<PointT>(ring_idx_, sector_idx_);
                subpatch.clear();
                subpatch.candidates_ = regionwise_nonground_;
                subpatch.candidates_ += regionwise_ground_;
                subpatch.sort_cloud(subpatch.candidates_);
                subpatch.estimate_regionwise_ground(20, 0.4, 3, 0.15);
                cout << "=> " << subpatch.regionwise_ground_.size() << " and " << subpatch.regionwise_nonground_.size() << endl;
                if (subpatch.get_z_vec() > 0.707) {
                    regionwise_ground_ = subpatch.regionwise_ground_;
                    regionwise_nonground_ = subpatch.regionwise_nonground_;
                    is_definite_ground_in_prior = false;
                }
//                regionwise_nonground_.clear();
            }
        }
        init_seeds_.clear();
        init_seeds_ = regionwise_ground_;
    }

    inline void estimate_advanced_regionwise_ground(const int num_iter, const float th_plane_dist, const float uprightness_thr,
                                                    const float sv_mean, const float sv_std, GSegParams& gseg_params, RevertParams& revert_params, bool verbose=true) {
        int init_num = regionwise_ground_.points.size();
        // If num. of points is small, then the result of PCA is not reliable
        if (regionwise_ground_.points.size() < num_min_init_pts_) {
            is_succeeded_         = false;
            status_               = FEWER_NUM_PTS_INTERCOUPLED;
            return;
        }
        int num_cand = regionwise_ground_.points.size();

        Eigen::Matrix<bool, 1, Eigen::Dynamic> is_ground;
        is_ground.resize(1, num_cand);
        for (int i = 0; i < num_cand; ++i) {
            is_ground(0, i) = true;
        }

        cloud2eigen(regionwise_ground_, cand_xyz_);

        pcl::PointCloud<PointT> updated_g_candidates;
        updated_g_candidates.reserve(num_cand);
        // 3. Iterative ground plane fitting
        int num_ng = regionwise_nonground_.points.size();
        float adaptive_th_plane_dist = th_plane_dist;

        if (ring_idx_ < gseg_params.zone2_idx && is_definite_ground_in_prior && static_cast<float>(num_ng) / num_cand > 1.5 && global_residual_diff_ > 2.2) {
            do_obstacles_exist_ = true;
            if (verbose) cout << "\033[1;31mThreshold becomes conservative!\033[0m" << endl;
            adaptive_th_plane_dist = 0.075;
        }

        for (int i = 0; i < num_iter; i++) {
            // More liberal way!
            if (regionwise_nonground_.empty() && is_definite_ground_in_prior) {
                estimate_plane_(regionwise_ground_);
                update_th_plane_dist(adaptive_th_plane_dist, d_);
                if (normal_(2, 0) > soft_normal_bound_) {
                    if (verbose) cout << "\033[1;32m[V2] " << ring_idx_ << ", " << sector_idx_ << " is definite ground! " << endl;
                    is_definite_ground_ = true;
                    break;
                }
            }

            static float cutoff_thr = max(0.1, upper_bound_ - local_residual_diff_ * 2.0 / 3.0);
            bool is_cutoff = false;
            if (i == 0) {
//                float cutoff_thr = max(upper_bound_/2.0, 0.15);
                if (ring_idx_ < gseg_params.zone1_idx) {
                    PlaneInfo p_prev;
                    estimate_plane_(regionwise_ground_, p_prev);
                    if (!regionwise_nonground_.empty() && local_residual_diff_ > upper_bound_ * 0.8) {
                        if (!cutoff_initial_inliers(is_ground, cutoff_thr, sv_mean + 0.1 * sv_std, verbose)) {
                            return;
                        }

                        updated_g_candidates.clear();
                        for (int j = 0; j < num_cand; ++j) {
                            if (is_ground(0, j)) {
                                updated_g_candidates.points.emplace_back(regionwise_ground_.points[j]);
                            }
                        }
                        estimate_plane_(updated_g_candidates);
                        is_cutoff = true;
                    } else { estimate_plane_(regionwise_ground_); }
                } else {
                    // All initial inliers are considered as ground
                    estimate_plane_(regionwise_ground_);
                }
                update_th_plane_dist(adaptive_th_plane_dist, d_);
            } else {
                updated_g_candidates.clear();
                for (int j = 0; j < num_cand; ++j) {
                    if (is_ground(0, j)) {
                        updated_g_candidates.points.emplace_back(regionwise_ground_.points[j]);
                    }
                }
                estimate_plane_(updated_g_candidates);
                update_th_plane_dist(adaptive_th_plane_dist, d_);
            }

            /***
             * Check uprightness in an intercoupled way
             */
            rel_normal_ = (prior_weighted_plane_.normal_.transpose() * normal_).sum();
            if (rel_normal_ < uprightness_thr) {
                if (verbose) cout << "\033[1;31m" << i << "th: sv - " << singular_values_.minCoeff() << ", rel. nor. - " << rel_normal_ << "\033[0m" << endl;
                float prob_var = get_prob(singular_values_.minCoeff(), sv_mean, 1.96 * sv_std, "TSTB");
                float prob_normal = get_prob(normal_(2, 0), 0.7070, 0.02, "TGTB");

                if (verbose) cout << "\033[1;31m p_sv - " << prob_var << ", (" << normal_(2, 0) << ") p_absn - " << prob_normal << " => " << prob_normal * prob_var << "\033[0m" << endl;

                // It's not a target for the closest ring, but planar plane yet tilted one. For example 430 in Seq. 02
                if (prob_var * prob_normal > 0.3 && ring_idx_ > 0 || (is_cutoff && i == 0)) {
                    if (verbose) cout << "Yet, its quite flat enough!" << endl;
                    // For nonground -> ground revert
                    if (i > 0 && prob_var * prob_normal < 0.62) {
                        status_ = BECOME_MORE_STRICT;
                    }
                } else {
                    // Too tilted
                    adaptive_th_plane_dist *= decay_rate_uprightness_;

                    status_ = BECOME_MORE_STRICT;
                    if (i == 0) {
                        update_th_plane_dist(adaptive_th_plane_dist, d_);
                    }
                    if (i == (num_iter - 1)) {
                        static bool USE_V_GPF = false;
                        if (USE_V_GPF) {
                            PlaneInfo vertical_p;
                            if (verbose)
                                cout << "[V-GPF]: " << static_cast<float>(regionwise_nonground_.size()) / regionwise_ground_.size() * 100
                                     << "%" << endl;
                            if (static_cast<float>(regionwise_nonground_.size()) / regionwise_ground_.size() > 0.8 && ring_idx_ < 6) {
                                static float verticalness = 0.068;
                                estimate_vertical_plane_(regionwise_nonground_, vertical_p, verticalness, 0.125);
                                if (verbose) cout << "[V-GPF]: " << vertical_p.normal_.transpose() << endl;
                                if (abs(vertical_p.normal_(2.0)) < verticalness) { // If x and y is dominant
                                    Eigen::VectorXf d_est = cand_xyz_ * vertical_p.normal_;
                                    vector<int>     vertical_idxes;
                                    vertical_idxes.reserve(regionwise_ground_.size() / 2);
                                    for (int j = 0; j < d_est.cols(); ++j) {
                                        static float th_d_vertical = 0.1;
                                        if (d_est(0, j) + vertical_p.d_ < th_d_vertical && d_est(0, j) + vertical_p.d_ > -th_d_vertical) {
                                            vertical_idxes.push_back(j);
                                        }
                                    }
                                    move_to_points(regionwise_ground_, regionwise_nonground_, vertical_idxes);
                                    is_succeeded_ = true;

                                    // Again, ground is estimated!!
                                    num_cand = regionwise_ground_.points.size();
                                    is_ground.resize(1, num_cand);
                                    for (int a = 0; a < num_cand; ++a) {
                                        is_ground(0, a) = true;
                                    }
                                    cloud2eigen(regionwise_ground_, cand_xyz_);
                                    for (int l = 0; l < num_iter; ++l) {
                                        if (l == 0) {
                                            estimate_plane_(regionwise_ground_);
                                        } else {
                                            updated_g_candidates.clear();
                                            for (int b = 0; b < num_cand; ++b) {
                                                if (is_ground(0, b)) {
                                                    updated_g_candidates.points.emplace_back(regionwise_ground_.points[b]);
                                                }
                                            }
                                            estimate_plane_(updated_g_candidates);
                                        }
                                        update_th_plane_dist(0.1, d_);

                                        d_est = cand_xyz_ * normal_;

                                        float num_valid_init = 0;
                                        for (int r           = 0; r < d_est.rows(); r++) {
                                            if (d_est[r] < th_dist_d_) {
                                                is_ground(0, r) = true;
                                                num_valid_init++;
                                            } else {
                                                is_ground(0, r) = false;
                                            }
                                        }
                                    }

                                    vector<int> nonground_idxes;
                                    nonground_idxes.reserve(num_cand); // large-enough size
                                    for (int j = 0; j < num_cand; ++j) {
                                        if (!is_ground(0, j)) {
                                            nonground_idxes.push_back(j);
                                        }
                                    }
                                    move_to_points(regionwise_ground_, regionwise_nonground_, nonground_idxes);
                                } else {
                                    is_succeeded_ = false;
                                }
                            } else {
                                is_succeeded_ = false;
                            }
                        } else { is_succeeded_ = false; }
//                        is_succeeded_ = false;
                        if (status_ == NOT_ASSIGNED || BECOME_MORE_STRICT) status_ = TOO_TILTED;
                        return;
                    }
                }
            }

            Eigen::VectorXf d_est = cand_xyz_ * normal_;

            float num_valid_init = 0;
            for (int r = 0; r < d_est.rows(); r++) {
                if (d_est[r] < th_dist_d_) {
                    is_ground(0, r) = true;
                    num_valid_init++;
                } else {
                    is_ground(0, r) = false;
                }
            }
        }

        compensate_linearity(0.93, verbose);

        vector<int> g_to_ng_idxes;
        vector<int> ng_to_g_idxes;
        if (!is_definite_ground_) {
            g_to_ng_idxes.reserve(num_cand); // large-enough size
            for (int j = 0; j < num_cand; ++j) {
                if (!is_ground(0, j)) {
                    g_to_ng_idxes.push_back(j);
                }
            }
            int M = regionwise_ground_.size();
            if (verbose) cout << "# of pts: "<< M << " => " << M - g_to_ng_idxes.size() << " + " << g_to_ng_idxes.size() << endl;

            // Only targets scattered, noisy FNs in a conservative way
            static int heuristic_close_ring_idx = 8;
            if (ring_idx_ < heuristic_close_ring_idx && close_rings_revert_on_ && status_ != BECOME_MORE_STRICT) {
                int n = g_to_ng_idxes.size();
                // W/o regionwise_nonground condition is important!!
                if (n > 0  && regionwise_nonground_.points.size() == 0) {
                    if (verbose) cout << "\033[1;35m[Z1 Revert] ("<< ring_idx_ <<", " << sector_idx_ << ") Start ->";
                    int num_min_criteria, num_max_criteria;
                    if (ring_idx_ < close_idx_boundary_) { // Zone 0
                        num_min_criteria = num_min_close_revert_pts_;
                        num_max_criteria = num_max_close_revert_pts_;
                    } else if (ring_idx_ < heuristic_close_ring_idx) {
                        num_min_criteria = num_min_close_revert_pts_ / 2;
                        num_max_criteria = num_max_close_revert_pts_ / 2;
                    }

                    if (n < num_min_criteria) {
                        if (verbose) {
                            for (auto& idx: g_to_ng_idxes) {
                                reverted_ground_.points.emplace_back(regionwise_ground_.points[idx]);
                            }
                        }
                        g_to_ng_idxes.clear();
                        if (verbose) cout << "Success: Has a few number!\033[0m" <<endl;
                    } else if (n < num_max_criteria) {
                        potential_nonground_.clear();
                        for (auto& idx: g_to_ng_idxes) {
                            potential_nonground_.points.emplace_back(regionwise_ground_.points[idx]);
                        }
                        PlaneInfo p_ng;
                        estimate_plane_(potential_nonground_, p_ng);
                        if (p_ng.normal_(2, 0) > 0.99619 && p_ng.singular_values_.minCoeff() < sv_mean) {
                            if (verbose) {
                                for (auto& idx: g_to_ng_idxes) {
                                    reverted_ground_.points.emplace_back(regionwise_ground_.points[idx]);
                                }
                            }
                            g_to_ng_idxes.clear();
                            if (verbose) cout << "Success: Flat & orthogonal enough!\033[0m" <<endl;
                        }else {
                            if (verbose) cout << "Fail: "<< p_ng.normal_(2, 0) << ", " << p_ng.singular_values_.minCoeff() << "\033[0m" <<endl;
                        }
                    }
                }
            }
            if (verbose) cout << "G -> NG Reject is operated!: " << g_to_ng_idxes.size() << endl;

            // nonground -> ground
            // For steep slopes
            if (!do_obstacles_exist_ && regionwise_nonground_.size() > 0) {
                if (!is_single_line_ && status_ != BECOME_MORE_STRICT) {
                    Eigen::Matrix<float, 1, Eigen::Dynamic> residuals_ng;
                    PlaneInfo                               p_est;
                    get_patch_info(p_est);
                    calc_residuals(p_est, regionwise_nonground_, residuals_ng);
                    ng_to_g_idxes.reserve(1000);
                    bool     is_done = false;
                    for (int i       = 0; i < regionwise_nonground_.size(); ++i) {
                        if (residuals_ng(0, i) < th_plane_dist) {
                            ng_to_g_idxes.push_back(i);
                            if (verbose) {
                                if (!is_done) {
                                    is_done = true;
                                }
                                reverted_ground_.points.emplace_back(regionwise_nonground_.points[i]);
                            }
                        }
                    }
                    if (is_done) {
                        if (verbose) cout << "NG -> G Revert is operated!: " << ng_to_g_idxes.size() << endl;
                    }
                }
            }
        }
        move_to_points(regionwise_nonground_, regionwise_ground_, ng_to_g_idxes);
        if (static_cast<float>(ng_to_g_idxes.size()) / init_num > 0.1 && ng_to_g_idxes.size() > num_min_init_pts_) {
            estimate_plane_(regionwise_ground_);
        }

        /***
         * Revert Procedure
         * Check the prob. that whether the `regionwise_nonground_` is ground or not
         */
        PlaneInfo p;
        get_patch_info(p);
        // Height Filtering
//        if (p.mean_(2, 0) - prior_weighted_plane_.mean_(2, 0) > gseg_params.rel_z_thr_intercoupled) {
//            if (verbose) cout << "\033[1;36m[Height Diff.]: "<< p.mean_(2, 0) - prior_weighted_plane_.mean_(2, 0) << " => Fail! \033[0m" << endl;
//            status_ = TOO_HIGH_ELEVATION;
//            is_succeeded_ = false;
//            return;
//        } else {
//            if (verbose) cout << "\033[0m" <<endl;
//        }

        bool is_reverted_successfully = false;
//        ring_idx_ > 1 &&
        if(ring_idx_ >= revert_params.ring_idx_boundary && !regionwise_nonground_.points.empty()
            && revert_params.revert_on && is_definite_ground_in_prior) {
            is_reverted_successfully = revert_nonground(regionwise_nonground_, regionwise_ground_, p, th_plane_dist, sv_mean, sv_std, revert_params, verbose);
            estimate_plane_(regionwise_ground_);
        }
        if (!is_reverted_successfully && !regionwise_nonground_.empty()) {
            // If revert is conducted, then these minor idxes are highly likely to be ground
            // In other words, if revert fails, `move_to_points` is performed
            move_to_points(regionwise_ground_, regionwise_nonground_, g_to_ng_idxes);
        }

        if (regionwise_nonground_.empty() && is_definite_ground_in_prior && normal_(2, 0) > soft_normal_bound_) {
            // After all procedure, sometimes regionwise_nonground becomes zero
            is_definite_ground_ = true;
        }

        /*** Update plane info */
        // For noise, g_to_ng_idxes should be updated lately
//        if (ring_idx_ == 0) { cout << mean_(2, 0) << endl; }

        is_succeeded_ = true; // ground seg.를 완료했다는 flag
        if (status_ == NOT_ASSIGNED) {
            status_       = UPRIGHT_ENOUGH;
            is_intercoupled_ = true; // 후의 바닥을 찾을 때 prior로 사용된다는 flag
        } else if (status_ == REVERT_COMPLETE) {
            is_intercoupled_ = true;
            if (regionwise_nonground_.empty()) {
                is_definite_ground_ = true;
            }
        } else if (status_ == BECOME_MORE_STRICT) {
            status_ = BECOME_MORE_STRICT_YET_SUCEEDED;
            is_intercoupled_ = false;
        }
    }

    inline void estimate_advanced_regionwise_ground_GNC(const int num_iter, const float th_plane_dist, const float uprightness_thr,
                                                        const RevertParams revert_params) {
        // If num. of points is small, then the result of PCA is not reliable
        if (regionwise_ground_.points.size() < num_min_init_pts_) {
            is_succeeded_         = false;
            status_               = FEWER_NUM_PTS;
            return;
        }
        /*** Q. 승재 말대로, 전부 다에서 찾을 필요가 있을까?
         * 그래서 'is_ground' boolean set으로 판별!
         * Init ground 내에서 ground / nonground로 나눔
         * `regionwise_ground_`가 이미 ground를 모두 포함하고 있다는 가정
        */

        int num_cand = regionwise_ground_.points.size();

        Eigen::Matrix<bool, 1, Eigen::Dynamic> is_ground;
        is_ground.resize(1, num_cand);
        for (int i = 0; i < num_cand; ++i) {
            is_ground(0, i) = true;
        }

        cloud2eigen(regionwise_ground_, cand_xyz_);

        pcl::PointCloud<PointT> updated_g_candidates;
        updated_g_candidates.reserve(num_cand);

        // 3. Iterative ground plane fitting
        float adaptive_th_plane_dist = th_plane_dist;

        float sqr_noise_bound = pow(th_plane_dist, 2);
        sqr_residuals_.resize(1, init_residuals_.cols());
        weights_.resize(1, init_residuals_.cols());
        for (int i = 0; i < init_residuals_.cols(); ++i) {
            sqr_residuals_(0, i) = init_residuals_(0, i) * init_residuals_(0, i);
        }

        float mu = 1 / (2 * sqr_residuals_.maxCoeff() / sqr_noise_bound - 1);
        if (mu <= 0) {
            cout << sqr_residuals_.maxCoeff() << endl;
            cout << "\033[1;32mAll pts are inliers!! \033[0m" << endl;
            estimate_plane_(regionwise_ground_);
        } else {
            for (int i = 0; i < num_iter; i++) {
                double th1 = (mu + 1) / mu * sqr_noise_bound;
                double th2 = mu / (mu + 1) * sqr_noise_bound;

                for (int r = 0; r < init_residuals_.cols(); ++r) {
                    float sqr_residual = static_cast<float>(sqr_residuals_(0, r));
                    if (sqr_residual >= th1) {
                        weights_(0, r) = 0;
                    } else if (sqr_residual <= th2) {
                        weights_(0, r) = 1;
                    } else {
                        weights_(0, r) = sqrt(sqr_noise_bound * mu * (mu + 1) / sqr_residual) - mu;
                        assert(weights_(j, 0) >= 0 && weights_(j, 0) <= 1);
                    }
                }
                if (i == 0) {
                    cout <<weights_.minCoeff() << " ~ " << weights_.maxCoeff()<< endl;
                }
                estimate_weighted_plane_(regionwise_ground_, weights_);
                update_th_plane_dist(adaptive_th_plane_dist, d_);

                /***
                 * Check uprightness in an intercoupled way
                 */
                rel_normal_ = (prior_weighted_plane_.normal_.transpose() * normal_).sum();
                if (rel_normal_ < uprightness_thr) {
                    mu *= 1.1;
                    status_ = BECOME_MORE_STRICT;
                    if (i == (num_iter - 1)) {
                        is_succeeded_  = false;
                        if (status_ == NOT_ASSIGNED || BECOME_MORE_STRICT) status_ = TOO_TILTED;
                        return;
                    }
                }
                Eigen::VectorXf d_est = cand_xyz_ * normal_;
                float num_valid_init = 0;
                for (int r = 0; r < d_est.rows(); r++) {
                    if (d_est[r] < th_dist_d_) {
                        is_ground(0, r) = true;
                        num_valid_init++;
                    } else {
                        is_ground(0, r) = false;
                    }
                }

                // regionwise_ground is exactly same with the initial candidates
                if (regionwise_nonground_.empty() && (num_valid_init == num_cand) ) {
                    cout << "\033[1;32m" << ring_idx_ << ", " << sector_idx_ << " is definite ground" << endl;
                    if (ring_idx_ == 2) {
                        cout << singular_values_.minCoeff() << endl;
                    }
                    is_definite_ground_ = true;
                    break;
                }
            }
            mu *= 1.4; // gnc_factor
        }

        /**
         * Removal procedure
         */
        if (!is_definite_ground_) {
            cout << "coming?!" << endl;
            vector<int> nonground_idxes;
            nonground_idxes.reserve(num_cand); // large-enough size
            for (int j = 0; j < num_cand; ++j) {
                if (!is_ground(0, j)) {
                    nonground_idxes.push_back(j);
                }
            }
            cout << "nonground_idx setting complete" << endl;
            potential_nonground_.clear();
            for (auto &idx: nonground_idxes) {
                potential_nonground_.points.emplace_back(regionwise_ground_.points[idx]);
            }
            move_to_points(regionwise_ground_, regionwise_nonground_, nonground_idxes);
        }

        /***
         * Revert Procedure
         * Check the prob. that whether the `regionwise_nonground_` is ground or not
         */
        PlaneInfo p;
        get_patch_info(p);
//        rel_z_ = mean_(2, 0) - prior_weighted_plane_.mean_(2, 0);
//        rel_normal_ = (prior_weighted_plane_.normal_.transpose() * p.normal_).sum();
//        float prob_rel_z = 1 / (1 + exp(rel_z_ - 1.75));
//        float prob_rel_normal = 1 / (1 + exp(-50 * (rel_normal_ - 0.9)));

        /*** DO NOT ERASE!!! */
//        if (ring_idx_ == 0 && sector_idx_ > 12) {
//            cout << ring_idx_ << ", " << sector_idx_ << " | " << rel_z_ << ", " << rel_normal_ << ", " << get_surface_variable() << " ? "
//                 << regionwise_ground_.points.size() << " <-> " << init_num << endl;
//        }
        /*** DO NOT ERASE!!! */

//        cout<<"  =>  " <<prob_rel_z<<", " << prob_rel_normal;
//        if (prob_rel_z * prob_rel_normal < 0.5) {
//            cout<<"\033[1;31m" << "CUTT!!!" << "\033[0m"<<endl;
//            is_succeeded_ = false;
//            status_ = RELATIVELY_DRAMATIC;
//            return;
//        }else { cout<<endl; }

//        if(!regionwise_nonground_.points.empty() && revert_on_) {
//            revert_nonground(regionwise_nonground_, regionwise_ground_, p, revert_params);
//
//            /*** Update plane info */
//            estimate_plane_(regionwise_ground_);
//        }
//        if (normal_(2, 0) < 0.707) {
//            is_intercoupled_ = false; // 후의 바닥을 찾을 때 prior로 사용된다는 flag
//        }

        is_succeeded_ = true; // ground seg.를 완료했다는 flag
        is_intercoupled_ = true; // 후의 바닥을 찾을 때 prior로 사용된다는 flag
        if (status_ == NOT_ASSIGNED)       status_ = UPRIGHT_ENOUGH;
        if (status_ == BECOME_MORE_STRICT) status_ = BECOME_MORE_STRICT_YET_SUCEEDED;
        cout << "gamma => " << status_ << endl;

//        if ((ring_idx_ < 12) && (regionwise_ground_.points.size() > 5) && (regionwise_nonground_.points.size() > 0)) {
//            print_properties_for_analysis();
//        }
    }

    inline void move_to_points(pcl::PointCloud<PointT>& src, pcl::PointCloud<PointT>& target, vector<int>& src_indices) {
        while (!src_indices.empty()) {
            int idx = src_indices.back();
            src_indices.pop_back();
            if (src.points.size() > 1) {
                std::iter_swap(src.points.begin() + idx, src.points.end() - 1);
            } else if (src.points.size() == 0) {
                break;
            }
            auto target_candidate = src.back();
            target.points.emplace_back(target_candidate);
            src.points.pop_back();
        }
    }

    inline bool revert_nonground(pcl::PointCloud<PointT>& nonground, pcl::PointCloud<PointT>& ground, const PlaneInfo plane,
                                 const float th_dist, const float sv_mean, const float sv_std,
                                 RevertParams revert_params, bool verbose=false) {
//        cout << revert_params.th_mean_singular_value << endl;
//        cout << revert_params.th_std_smaller_singular_value << endl;
        reverted_ground_.clear();
        // 1. Residual criteria

        Eigen::Matrix<float, 1, Eigen::Dynamic> residuals;
        calc_residuals(plane, nonground, residuals);

        float max_res = residuals.maxCoeff();
        float min_res = residuals.minCoeff();
//        static float continuity_thr = ring_idx_ < 10? 0.13 : 0.36;
        static float continuity_thr = 0.36;
        if (verbose) cout << "\033[1;31m[Revert]: " << min_res << ", " << max_res<< "\033[0m"<< endl;
        if (min_res > th_dist + continuity_thr) {
            if (verbose) cout << "\033[1;31mThe pts are not continous!!!!!\033[0m"<< endl;
            return false;
        }

        // If only a few nonground pts exists
        if (nonground.points.size() < revert_params.num_min_pts_for_revert) {
            // Scattered one -> Conduct naive revert
            if (max_res < revert_params.th_mean_max_res) {
                for (const auto &pt: nonground.points) {
                    ground.points.emplace_back(pt);
                    if (verbose) { reverted_ground_.points.emplace_back(pt); }
                }
                nonground.points.clear();
            }
            return true;
        }
        revert_params.th_mean_inner_product = ring_idx_ < 10? 0.906 : 0.800;
        revert_params.th_mean_singular_value = sv_mean;
        revert_params.th_std_larger_singular_value = sv_std;
        revert_params.th_std_smaller_singular_value = sv_std * 5.0;

        PlaneInfo p_ng;
        estimate_plane_(nonground, p_ng);
        float inner_product = abs(plane.normal_(0, 0) * p_ng.normal_(0, 0)
                                  + plane.normal_(1, 0) * p_ng.normal_(1, 0)
                                  + plane.normal_(2, 0) * p_ng.normal_(2, 0));

        float smallest_sv = p_ng.singular_values_.minCoeff();
        float surface_variable = smallest_sv / (p_ng.singular_values_(0, 0) + p_ng.singular_values_(1, 0) + p_ng.singular_values_(2, 0));

        /*** Set probability */
        float prob_ip = get_prob(inner_product, revert_params.th_mean_inner_product, revert_params.th_std_inner_product, "TGTB");
        float prob_mr = get_prob(max_res, revert_params.th_mean_max_res, revert_params.th_std_max_res, "TSTB");
        float prob_var;
        if (smallest_sv < revert_params.th_mean_singular_value) {
            prob_var = get_prob(smallest_sv, revert_params.th_mean_singular_value, revert_params.th_std_smaller_singular_value, "TSTB");
        } else {
            prob_var = get_prob(smallest_sv, revert_params.th_mean_singular_value, revert_params.th_std_larger_singular_value, "TSTB");
        }
        float prob_sv = get_prob(surface_variable, revert_params.th_mean_surface_variable, revert_params.th_std_surface_variable, "TSTB");

        /*** Calc probability */
//        float final_prob = prob_ip * prob_mr * prob_var;
        float final_prob = prob_ip * prob_var;
        float final_prob2 = prob_ip * prob_sv;

        cout << "thr: " << revert_params.th_revert_prob << endl;
        if (true) {
            int num_FN = count_num_ground_without_vegetation(nonground);
            int num_ng = nonground.points.size();
            static float perc = 0.8;
            bool is_true = (static_cast<float>(num_FN) / num_ng) > perc;
            if (verbose) cout << "[Revert] ("<< ring_idx_ << ", " << sector_idx_ << "): \033[1;32m" << inner_product << " => " << prob_ip;
            if (verbose) cout<< ", \033[1;33m" << max_res << " => " << prob_mr << ", \033[1;37m"<< smallest_sv << " => " << prob_var << " == ";
            if ((final_prob > revert_params.th_revert_prob && is_true) || (final_prob < revert_params.th_revert_prob && !is_true) ) {
                // True Positive & True Negative
                if (verbose) cout << "\033[1;32m";
            } else if ((final_prob > revert_params.th_revert_prob) && !is_true) {
                // False Positive
                if (verbose) cout << "\033[1;31m";
            } else {
                if (verbose) cout << "\033[1;34m but " << surface_variable << "  ";
            }
            if (verbose) cout << final_prob << ", " << final_prob2 << " \033[0m | "  << num_FN << " / " << nonground.points.size() << endl;
        }

//        cout << "      " << prob_sv << " => " << final_prob2 << "?? " <<endl;
//        bool revert_condition;
//        static float h_diff = 0.9;
//        if (h_diff > p_ng.mean_(2, 0) - plane.mean_(2, 0)) {
//        } else {
//            revert_condition = final_prob > revert_params.th_revert_prob;
//        }

//        bool revert_condition = final_prob > revert_params.th_revert_prob || final_prob2 > 0.3;
        bool revert_condition = final_prob > revert_params.th_revert_prob;
        if (revert_condition) {
            for (const auto& pt: nonground.points) {
                ground.points.emplace_back(pt);
                if (verbose) reverted_ground_.points.emplace_back(pt);
            }
            nonground.points.clear();
            if (status_ == NOT_ASSIGNED) status_ = REVERT_COMPLETE;
            return true;
        }else { return false; }
    }

    inline float get_prob(const float value, const float mean, const float std, string MODE) {
        // The greater, the better
        if (MODE == "TGTB") {
            return static_cast<float>( (1 + erf( (value - mean) / (std * sqrt(2)) ) ) ) / 2.0;

        } else if (MODE == "TSTB") {
            return static_cast<float>( (1 + erf( -(value - mean) / (std * sqrt(2)) ) ) ) / 2.0;
        }
    }

    inline void print_plane_info(const PlaneInfo& p) {
        float sv = p.singular_values_.minCoeff() / (p.singular_values_(0) + p.singular_values_(1) + p.singular_values_(2));
        cout<<"\033[1;32m"<< setw(2) << ring_idx_ << ", " << setw(2) << sector_idx_ << "| ";
        cout<<p.normal_(0, 0) <<", " <<p.normal_(1, 0) <<", " <<p.normal_(2, 0) <<endl;
        cout<< setw(8) << "" << p.mean_(0, 0) <<", " <<p.mean_(1, 0) <<", " <<p.mean_(2, 0) << ", ";
        cout<<p.singular_values_(2, 0) <<", " << sv << "\033[0m"<<endl;
    }
    /*** For analysis
     * It's just for debugging! */
//    inline void print_properties_for_analysis() {
//        std::string count_str        = std::to_string(INDEX);
//        std::string count_str_padded = std::string(NUM_ZEROS - count_str.length(), '0') + count_str;
//        std::string filepath         = LOG_DIR + SEQ + "/" + count_str_padded + ".csv";
//
//        cout << filepath << endl;
//        std::ofstream FileObj;
//        FileObj.open(filepath, std::ios::app);
//
//        /*** Extract What I want */
//        int num_TP = count_num_ground_without_vegetation(regionwise_ground_);
//        int num_FP = regionwise_ground_.points.size() - num_TP;
//        int num_FN = count_num_ground_without_vegetation(regionwise_nonground_);
//        int num_TN = regionwise_nonground_.points.size() - num_FN;
//        int num_N  = regionwise_nonground_.points.size();
//
//        pcl::PointCloud<PointT> FN;
//        pcl::PointCloud<PointT> TN;
//        discern_ground(regionwise_nonground_, FN, TN);
//
//        Eigen::Matrix3f cov_N, cov_FN;
//        Eigen::Vector4f pc_mean_dummy;
//
//        // For all negative points
//        pcl::computeMeanAndCovarianceMatrix(regionwise_nonground_, cov_N, pc_mean_dummy);
//        Eigen::JacobiSVD<Eigen::MatrixXf> svd_N(cov_N, Eigen::DecompositionOptions::ComputeFullU);
//        VectorXf                          sv_N;
//        MatrixXf                          normal_N;
//        sv_N                               = svd_N.singularValues();
//        normal_N                           = (svd_N.matrixU().col(2));
//        if (normal_N(2, 0) < 0) { // z 방향 vector는 위를 무조건 향하게 해야 함
//            normal_N = -normal_N;
//        }
//        vector<float>   residuals_N;
//        for (const auto &pt: regionwise_nonground_) {
//            float residual = normal_(0, 0) * pt.x + normal_(1, 0) * pt.y + normal_(2, 0) * pt.z + d_;
//            residuals_N.push_back(residual);
//        }
//
//        float max_res_N          = *max_element(residuals_N.begin(), residuals_N.end());
//        float min_res_N          = *min_element(residuals_N.begin(), residuals_N.end());
//        float surface_variable_N = sv_N.minCoeff() / (sv_N(0) + sv_N(1) + sv_N(2));
//
//        float surface_variable = singular_values_.minCoeff() / (singular_values_(0, 0) + singular_values_(1, 0) + singular_values_(2, 0));
//
//        FileObj << ring_idx_ << "," << sector_idx_ << ","
//                << num_TP << "," << num_FP << "," << num_FN << "," << num_TN << ","
//                << normal_(0, 0) << "," << normal_(1, 0) << "," << normal_(2, 0) << ","
//                << singular_values_.minCoeff() << "," << surface_variable << "," << rel_z_ <<","
//                << prior_weighted_plane_.normal_(0, 0) << "," << prior_weighted_plane_.normal_(1, 0) << "," << prior_weighted_plane_.normal_(2,0) <<",";
//
//        if (num_N > 0) {
//            FileObj << normal_N(0, 0) << "," << normal_N(1, 0) << "," << normal_N(2, 0) << ","
//                    << max_res_N << "," << min_res_N << "," << sv_N.minCoeff() << "," << surface_variable_N << "\n";
//        } else {
//            FileObj << 0.0 << "," << 0.0 << "," << 0.0 << ","
//                    << 0.0 << "," << 0.0 << "," << 0.0 << "," << 0.0 << "\n";
//        }
//        FileObj.close();
//    }

    inline void estimate_weighted_plane_(
            const Eigen::Matrix<float, Eigen::Dynamic, 3> & points_xyz,
            const Eigen::Matrix<float, 1, Eigen::Dynamic> &weights) {
        // It is too slow!!!!
        int N = weights.cols();
        float sum_weights = 0;
        for (int i = 0; i < N; ++i) {
            sum_weights += weights(0, i);
        }
        // Set normalized weights for matrix operation
        Eigen::Matrix<float, 1, Eigen::Dynamic> weights_norm;
        weights_norm.resize(1, N);
        Eigen::MatrixXf weights_norm_diag = Eigen::MatrixXf::Zero(N, N);

        for (int i = 0; i < N; ++i) {
            weights_norm(0, i) = weights(0, i) / sum_weights;
            weights_norm_diag(i, i) = weights(0, i) / sum_weights;
        }
        mean_ = (weights_norm * points_xyz).transpose();
        // Calculate weighted covariance
        Eigen::Matrix<float, Eigen::Dynamic, 3> xyz_minus_mean = points_xyz.rowwise() - mean_.transpose();
        cov_  = xyz_minus_mean.transpose() * weights_norm_diag * xyz_minus_mean;

        // Singular Value Decomposition: SVD
        Eigen::JacobiSVD<Eigen::MatrixXf> svd(cov_, Eigen::DecompositionOptions::ComputeFullU);
        singular_values_ = svd.singularValues();

        // use the least singular vector as normal
        normal_ = (svd.matrixU().col(2));
        if (normal_(2, 0) < 0) { // z 방향 vector는 위를 무조건 향하게 해야 함
            normal_ = -normal_;
        }
        d_ = -(normal_.transpose() * mean_)(0, 0);
    }

    inline void estimate_regionwise_ground_via_GNC(
            const int num_lpr, const float th_seeds, const int num_iter, const float th_plane_dist,
            float noise_bound, float gnc_factor) {
        throw invalid_argument("Not implemented!");
    }
//        cout << "\033[1;32m ==========\033[0m" << endl;
//        /***
//         * 1. inittial_seeds를 세팅한다
//         * NOTE: 첫번 째 ring일 때는 이미 아래쪽 노이즈가 margin에 의해서 필터링되어 있음
//         * ToDo: 다른 bin들의 lowest error는 어떻게 없애줘야할까...
//         */
//        extract_initial_seeds(num_lpr, th_seeds, regionwise_ground_);
//        if (regionwise_ground_.points.size() == 0) {
//            regionwise_nonground_ = candidates_;
//            return;
//        }
//
//        int num_all_candidates = candidates_.points.size();
//        cand_xyz_.resize(num_all_candidates, 3);
//        for (int i                                               = 0; i < num_all_candidates; ++i) {
//            const auto &p = candidates_.points[i];
//            cand_xyz_.row(i) << p.x, p.y, p.z;
//        }
//
//        Eigen::Matrix<float, Eigen::Dynamic, 3> inliers_xyz;
//        int                                     num_init_inliers = regionwise_ground_.points.size();
//        inliers_xyz.resize(num_init_inliers, 3);
//        for (int l = 0; l < num_init_inliers; ++l) {
//            const auto &p = regionwise_ground_.points[l];
//            inliers_xyz.row(l) << p.x, p.y, p.z;
//        }
//
//        // Weight Initialization
//        weights_ = Eigen::Matrix<float, 1, Eigen::Dynamic>::Ones(1, num_init_inliers);
//
//        float mu;
//        float sqr_noise_bound = pow(noise_bound, 2);
//        // 3. Iterative ground plane fitting
//
//        // For final state
//        Eigen::Array<float, Eigen::Dynamic, 1> residuals_all;
//        Eigen::Array<float, Eigen::Dynamic, 1> sqr_residuals_all;
//
//        for (int i = 0; i < num_iter; i++) {
//            cout << "\033[1;32m " << i << " \033[0m, # pts: " << regionwise_ground_.points.size() << endl;
//            cout << "1 | " << weights_ << endl;
//            cout << "2 | " << regionwise_ground_.points.size() << " == " << weights_.cols() << " ? " << endl;
//            estimate_weighted_plane_(regionwise_ground_, weights_);
//
//            auto residuals     = (inliers_xyz * normal_).array() + d_;
//            auto sqr_residuals = residuals.square();
//
//            if (i == 0) {
//                // Initial state
//                float max_sqr_residual = sqr_residuals.maxCoeff();
//                mu = 1 / (2 * max_sqr_residual / sqr_noise_bound - 1);
//                if (mu <= 0) {
//                    regionwise_nonground_.clear();
//                    regionwise_ground_ = candidates_;
//                    return;
//                }
//            }
//            if (i == (num_iter - 1)) {
//                // Final state
//                residuals_all = (cand_xyz_ * normal_).array() + d_;
//            }
//
//            double th1 = (mu + 1) / mu * sqr_noise_bound;
//            double th2 = mu / (mu + 1) * sqr_noise_bound;
//
//            // 전체 points에 대해서 확인해야 함!
//            if (i < num_iter - 1) {
//                for (int r = 0; r < num_init_inliers; ++r) {
//                    float sqr_residual = static_cast<float>(sqr_residuals(r, 0));
//                    if (sqr_residual >= th1) {
//                        weights_(0, r) = 0;
//                    } else if (sqr_residual <= th2) {
//                        weights_(0, r) = 1;
//                    } else {
//                        weights_(0, r) = sqrt(sqr_noise_bound * mu * (mu + 1) / sqr_residual) - mu;
//                        assert(weights_(j, 0) >= 0 && weights_(j, 0) <= 1);
//                    }
//                }
//            } else {
//                regionwise_ground_.clear();
//                regionwise_nonground_.clear();
//                for (int r = 0; r < num_all_candidates; ++r) {
//                    if (residuals_all(r, 0) < noise_bound) {
//                        regionwise_ground_.points.emplace_back(candidates_[r]);
//                    } else {
//                        regionwise_nonground_.points.emplace_back(candidates_[r]);
//                    }
//                }
//
//                mu = mu * gnc_factor;
//            }
//        }
//    }
    inline bool cutoff_initial_inliers(Eigen::Matrix<bool, 1, Eigen::Dynamic>& is_ground, float cutoff_thr, float sv_mean, bool verbose=false) {
        if (verbose) cout << "\033[2;32m[Cutoff]: Cutoff is operated: ";
        PlaneInfo p;
        estimate_plane_(regionwise_ground_, p);
        cout << p.singular_values_(2, 0) << " vs " << sv_mean << endl;
        if (p.singular_values_(2, 0) < sv_mean) {
            cout << "It's flat enough!" << endl;
            // Note that all elements in is_ground is already set to `true`
            return true;
        }
        Eigen::Matrix<float, 1, Eigen::Dynamic> residuals_tmp;
        calc_residuals(prior_weighted_plane_, regionwise_ground_, residuals_tmp);
        int num_false = 0;
        for (int i = 0; i < residuals_tmp.cols(); ++i) {
            if (residuals_tmp(0, i) > cutoff_thr) {
                is_ground(0, i) = false;
                ++num_false;
            }
        }
        if (verbose) cout << "Total "<< num_false<< " pts are rejected! (" << regionwise_ground_.points.size() - num_false << ")\033[0m" << endl;
        // 2: Minimum # of points for estimating plane is 3
        if (regionwise_ground_.size() < num_false + 2) {
            if (verbose) cout << "Total " << num_false << "is filtered out! " << endl;
            is_succeeded_ = false;
            status_ = TOO_HIGH_ELEVATION;
            return false;
        } else {
            return true;
        }
    }

    inline void compensate_linearity(float linearity_thr, bool verbose=false) {
        if (verbose) cout << "Linearity: " << "\033[0m" << linearity_ << ", " << planarity_ << endl;
        if (ring_idx_ > 0) {
            // linearity_ is calculated in estimate_plane_
            if (linearity_ > linearity_thr) {
                cout << " Normal changes " << normal_.transpose();
                normal_ = (1.0 - linearity_) * normal_;
                normal_(2, 0) += linearity_ * 1.0; // orthogonal
                normal_ /= normal_.norm();
                cout << " => " << normal_.transpose() << endl;
                cout << "\033[1;31m";
                is_single_line_ = true;
            }
        }
    }
    inline float calc_point_to_plane(const PlaneInfo& p) {
        float dist0 = (normal_.transpose() * (p.mean_ - mean_))(0, 0);
        float dist1 = (p.normal_.transpose() * (mean_ - p.mean_))(0, 0);
        return max(dist0, dist1);
    }

    inline void update_success(bool is_succeeded) {
        is_succeeded_ = is_succeeded;
    }

    inline void update_status(double status) {
        status_ = status;
    }

    /***
     * 아래 append 함수들:
     * 쓸데 없는 복사를 막기 위해서 call-by-referenece로 불러서 emplace_back해 줌
     */
    inline void append_seeds_to(pcl::PointCloud<PointT> &pc) const {
        for (const auto &pt: init_seeds_.points) {
            pc.points.emplace_back(pt);
        }
    }

    inline void append_candidates_to(pcl::PointCloud<PointT> &pc) const {
        for (const auto &pt: candidates_.points) {
            pc.points.emplace_back(pt);
        }
    }

    inline void append_regionwise_ground_to(pcl::PointCloud<PointT> &pc) const {
        for (const auto &pt: regionwise_ground_.points) {
            pc.points.emplace_back(pt);
        }
    }

    inline void append_regionwise_nonground_to(pcl::PointCloud<PointT> &pc) const {
        for (const auto &pt: regionwise_nonground_.points) {
            pc.points.emplace_back(pt);
        }
    }

    inline void append_reflected_noises_to(pcl::PointCloud<PointT> &pc) const {
        for (const auto &pt: reflected_noises_.points) {
            pc.points.emplace_back(pt);
        }
    }

    inline void append_reverted_ground_to(pcl::PointCloud<PointT> &pc) const {
        for (const auto &pt: reverted_ground_.points) {
            pc.points.emplace_back(pt);
        }
    }

    inline void get_patch_info(PlaneInfo &p) const {
        p.normal_            = normal_;
        p.singular_values_   = singular_values_;
        p.mean_              = mean_;
        p.d_                 = d_;
        p.is_definite_ground_ = is_definite_ground_;
        p.planarity_          = planarity_;
    }

    inline double get_status() const { return status_; }

    inline float get_z_vec() const { return abs(normal_(2, 0)); }

    inline float get_z_elevation() const { return mean_(2, 0); }

    inline float get_surface_variable() const {
        return singular_values_.minCoeff() / (singular_values_(0) + singular_values_(1) + singular_values_(2));
    }

    inline float get_mean_to_center_dist() const {
        return sqrt(pow(mean_(0, 0) - center_x_, 2) + pow(mean_(1, 0) - center_y_, 2));
    }

    inline float get_dist_ratio() const {
        return get_mean_to_center_dist() / min_len_;
    }

    inline Eigen::MatrixXf get_normal() const { return normal_; }

    inline Eigen::Vector3f get_mean_pos() const { return mean_; }


    inline void set_center_point(float mean_r, float theta) {
       center_x_ =  mean_r * cos(theta);
       center_y_ =  mean_r * sin(theta);
    }

    inline void set_min_len(float min_len) {
        min_len_ = min_len;
    }
};
#endif //PATCHWORK_BIN_H
