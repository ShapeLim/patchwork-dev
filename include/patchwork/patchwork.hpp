#ifndef PATCHWORK_H
#define PATCHWORK_H

#include <sensor_msgs/PointCloud2.h>
#include <ros/ros.h>
#include <jsk_recognition_msgs/PolygonArray.h>
#include <Eigen/Dense>
#include <boost/format.hpp>
#include <numeric>

#include <patchwork/utils.hpp>
#include <patchwork/Revert.h>
#include <patchwork/MeanNormal.h>

#define MARKER_Z_VALUE -2.2
#define UPRIGHT_ENOUGH 0.55
#define FLAT_ENOUGH 0.2
#define TOO_HIGH_ELEVATION 0.0
#define TOO_TILTED 1.0

#define NUM_HEURISTIC_MAX_PTS_IN_PATCH 3000

using Eigen::MatrixXf;
using Eigen::JacobiSVD;
using Eigen::VectorXf;

using namespace std;

/*
    @brief PathWork ROS Node.
*/
template <typename PointT>
bool point_z_cmp(PointT a, PointT b) {
    return a.z < b.z;
}

template <typename PointT>
class PatchWork {

public:
    typedef std::vector<pcl::PointCloud<PointT> > Ring;
    typedef std::vector<Ring> Zone;

    PatchWork() {};

    PatchWork(ros::NodeHandle *nh) : node_handle_(*nh) {
        // Init ROS related
        ROS_INFO("Inititalizing PatchWork...");

        node_handle_.param("sensor_height", sensor_height_, 1.723);
        node_handle_.param<bool>("/patchwork/verbose", verbose_, false);
        node_handle_.param<bool>("/patchwork/matlab_analyze", matlab_analyze_, false);

        node_handle_.param("/patchwork/num_iter", num_iter_, 3);
        node_handle_.param("/patchwork/num_lpr", num_lpr_, 20);
        node_handle_.param("/patchwork/num_min_pts", num_min_pts_, 10); 
        node_handle_.param("/patchwork/th_seeds", th_seeds_, 0.4);
        node_handle_.param("/patchwork/th_dist", th_dist_, 0.3);
        node_handle_.param("/patchwork/th_seeds_v", th_seeds_v_, 0.4);
        node_handle_.param("/patchwork/th_dist_v", th_dist_v_, 0.3);
        node_handle_.param("/patchwork/max_r", max_range_, 80.0);
        node_handle_.param("/patchwork/min_r", min_range_, 2.7); // It indicates bodysize of the car.
        node_handle_.param("/patchwork/uniform/num_rings", num_rings_, 30);
        node_handle_.param("/patchwork/uniform/num_sectors", num_sectors_, 108);
        node_handle_.param("/patchwork/uprightness_thr", uprightness_thr_, 0.5); // The larger, the more strict
        node_handle_.param("/patchwork/adaptive_seed_selection_margin", adaptive_seed_selection_margin_,
                           -1.1); // The more larger, the more soft
        node_handle_.param("/patchwork/flatness_margin", flatness_margin_, 0.0001);
        node_handle_.param("/patchwork/noise_filter_channel_num", noise_filter_channel_num_, 5);
        node_handle_.param("/patchwork/pc_num_channel", pc_num_channel_, 2048);
        node_handle_.param("/patchwork/intensity_thr", intensity_thr_, 0.2);
        node_handle_.param("/patchwork/original", original_, false);

        ROS_INFO("Sensor Height: %f", sensor_height_);
        ROS_INFO("Num of Iteration: %d", num_iter_);
        ROS_INFO("Num of LPR: %d", num_lpr_);
        ROS_INFO("Num of min. points: %d", num_min_pts_);
        ROS_INFO("Seeds Threshold: %f", th_seeds_);
        ROS_INFO("Distance Threshold: %f", th_dist_);
        ROS_INFO("Max. range:: %f", max_range_);
        ROS_INFO("Min. range:: %f", min_range_);
        ROS_INFO("Num. rings: %d", num_rings_);
        ROS_INFO("Num. sectors: %d", num_sectors_);
        ROS_INFO("Normal vector threshold: %f", uprightness_thr_);
        ROS_INFO("adaptive_seed_selection_margin: %f", adaptive_seed_selection_margin_);

        // CZM denotes 'Concentric Zone Model'. Please refer to our paper
        node_handle_.getParam("/patchwork/czm/num_zones", num_zones_);
        node_handle_.getParam("/patchwork/czm/num_sectors_each_zone", num_sectors_each_zone_);
        node_handle_.getParam("/patchwork/czm/mum_rings_each_zone", num_rings_each_zone_);
        node_handle_.getParam("/patchwork/czm/elevation_thresholds", elevation_thr_);
        node_handle_.getParam("/patchwork/czm/flatness_thresholds", flatness_thr_);

        ROS_INFO("Num. zones: %d", num_zones_);

        if (num_zones_ != 4 || num_sectors_each_zone_.size() != num_rings_each_zone_.size()) {
            throw invalid_argument("Some parameters are wrong! Check the num_zones and num_rings/sectors_each_zone");
        }
        if (elevation_thr_.size() != flatness_thr_.size()) {
            throw invalid_argument("Some parameters are wrong! Check the elevation/flatness_thresholds");
        }

        cout << (boost::format("Num. sectors: %d, %d, %d, %d") % num_sectors_each_zone_[0] % num_sectors_each_zone_[1] %
                 num_sectors_each_zone_[2] %
                 num_sectors_each_zone_[3]).str() << endl;
        cout << (boost::format("Num. rings: %01d, %01d, %01d, %01d") % num_rings_each_zone_[0] %
                 num_rings_each_zone_[1] %
                 num_rings_each_zone_[2] %
                 num_rings_each_zone_[3]).str() << endl;
        cout << (boost::format("elevation_thr_: %0.4f, %0.4f, %0.4f, %0.4f ") % elevation_thr_[0] % elevation_thr_[1] %
                 elevation_thr_[2] %
                 elevation_thr_[3]).str() << endl;
        cout << (boost::format("flatness_thr_: %0.4f, %0.4f, %0.4f, %0.4f ") % flatness_thr_[0] % flatness_thr_[1] %
                 flatness_thr_[2] %
                 flatness_thr_[3]).str() << endl;
        num_rings_of_interest_ = elevation_thr_.size();

        node_handle_.param("/patchwork/visualize", visualize_, true);
        poly_list_.header.frame_id = "/map";
        poly_list_.polygons.reserve(130000);

        revert_pc.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        ground_pc_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        non_ground_pc_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        regionwise_ground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);
        regionwise_nonground_.reserve(NUM_HEURISTIC_MAX_PTS_IN_PATCH);

        elevation_limit_.clear();
        for (int i=0; i<num_rings_of_interest_; i++) elevation_limit_.push_back(0);

        // update_flatness_.reserve(num_rings_of_interest_);
        // update_elevation_.reserve(num_rings_of_interest_);

        if (matlab_analyze_)
        {
            for (int i=0; i<num_rings_of_interest_; i++)
            {
                update_flatness_[i].reserve(75000);
                update_elevation_[i].reserve(75000);
                update_def_num_[i].reserve(75000);
                nonground_flatness_[i].reserve(75000);
                ground_flatness_[i].reserve(75000);
                nonground_elevation_[i].reserve(75000);
                ground_elevation_[i].reserve(75000);
                nonground_num_[i].reserve(75000);
                ground_num_[i].reserve(75000);
                right_bin_num_[i] = 0;
                wrong_bin_num_[i] = 0;
            }          
        }
        
        PlaneViz = node_handle_.advertise<jsk_recognition_msgs::PolygonArray>("/gpf/plane", 100, true);
        revert_pc_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/revert_pc", 100, true);
        reject_pc_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/reject_pc", 100, true);
        normal_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/normals", 100, true);
        pnormal_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/pnormals", 100, true);
        init_seeds_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/init_seeds", 100, true);
        noise_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/noise", 100, true);
        wall_pub = node_handle_.advertise<sensor_msgs::PointCloud2>("/wall", 100, true);

        min_range_z2_ = (7 * min_range_ + max_range_) / 8.0;
        min_range_z3_ = (3 * min_range_ + max_range_) / 4.0;
        min_range_z4_ = (min_range_ + max_range_) / 2.0;

        min_ranges = {min_range_, min_range_z2_, min_range_z3_, min_range_z4_};
        ring_sizes = {(min_range_z2_ - min_range_) / num_rings_each_zone_.at(0),
                      (min_range_z3_ - min_range_z2_) / num_rings_each_zone_.at(1),
                      (min_range_z4_ - min_range_z3_) / num_rings_each_zone_.at(2),
                      (max_range_ - min_range_z4_) / num_rings_each_zone_.at(3)};
        sector_sizes = {2 * M_PI / num_sectors_each_zone_.at(0), 2 * M_PI / num_sectors_each_zone_.at(1),
                        2 * M_PI / num_sectors_each_zone_.at(2),
                        2 * M_PI / num_sectors_each_zone_.at(3)};
        cout << "INITIALIZATION COMPLETE" << endl;

        for (int iter = 0; iter < num_zones_; ++iter) {
            Zone z;
            initialize_zone(z, num_sectors_each_zone_.at(iter), num_rings_each_zone_.at(iter));
            ConcentricZoneModel_.push_back(z);
        }
    }

    void estimate_ground(
            const pcl::PointCloud<PointT> &cloudIn,
            pcl::PointCloud<PointT> &cloudOut,
            pcl::PointCloud<PointT> &cloudNonground,
            double &time_taken);

    geometry_msgs::PolygonStamped set_plane_polygon(const MatrixXf &normal_v, const float &d);

    void get_reverted_TPFP(int &reverted_TP, int &reverted_FP, int &revert_well, int &revert_bad)
    {
        reverted_TP = accumulate(reverted_TP_.begin(), reverted_TP_.end(), 0);
        reverted_FP = accumulate(reverted_FP_.begin(), reverted_FP_.end(), 0);
        revert_well = revert_well_;
        revert_bad = revert_bad_;
    }

    void get_reverted_TPFP(int &reverted_TP, int &reverted_FP)
    {
        reverted_TP = accumulate(reverted_TP_.begin(), reverted_TP_.end(), 0);
        reverted_FP = accumulate(reverted_FP_.begin(), reverted_FP_.end(), 0);
    }

    void get_noise(int &num_noise)
    {
        num_noise = num_noise_;
    }

    void get_mean_flatness(std::vector<double> &update_flatness)
    {
        for (int i=0; i<4; i++)
        {
            double update_avg = 0.0, update_std = 0.0;
            calc_mean_stdev(update_flatness_[i], update_avg, update_std);
            update_flatness.push_back(update_avg);
        }        
    }

    void get_flatness(std::vector<double> &flatness, int i)
    {
        flatness = update_flatness_[i];
    }

    void get_elevation(std::vector<double> &elevation, int i)
    {
        elevation = update_elevation_[i];
    }

    void get_def_num(std::vector<int> &def_num, int i)
    {
        def_num = update_def_num_[i];
    }

    void get_nonground_flatness(std::vector<double> &flatness, int i)
    {
        flatness = nonground_flatness_[i];
    }

    void get_ground_flatness(std::vector<double> &flatness, int i)
    {
        flatness = ground_flatness_[i];
    }

    void get_nonground_elevation(std::vector<double> &elevation, int i)
    {
        elevation = nonground_elevation_[i];
    }

    void get_ground_elevation(std::vector<double> &elevation, int i)
    {
        elevation = ground_elevation_[i];
    }

    void get_nonground_num(std::vector<int> &num, int i)
    {
        num = nonground_num_[i];
    }

    void get_ground_num(std::vector<int> &num, int i)
    {
        num = ground_num_[i];
    }

    void get_right_wrong_bin_num(int &right, int &wrong, int i)
    {
        right = right_bin_num_[i];
        wrong = wrong_bin_num_[i];
    }

    void get_thr(double &flatness, double &elevation, int i)
    {
        flatness = flatness_thr_[i];
        elevation = elevation_thr_[i];
    }
    
private:
    ros::NodeHandle node_handle_;

    int num_iter_;
    int num_lpr_;
    int num_min_pts_;
    int num_rings_;
    int num_sectors_;
    int num_zones_;
    int num_rings_of_interest_;

    double sensor_height_;
    double th_seeds_;
    double th_dist_;
    double th_seeds_v_;
    double th_dist_v_;
    double max_range_;
    double min_range_;
    double uprightness_thr_;
    double adaptive_seed_selection_margin_;
    double min_range_z2_; // 12.3625
    double min_range_z3_; // 22.025
    double min_range_z4_; // 41.35

    bool verbose_;
    bool original_;
    bool matlab_analyze_;

    std::vector<double> update_flatness_[4];
    std::vector<double> update_elevation_[4];
    std::vector<int> update_def_num_[4];
    std::vector<double> nonground_flatness_[4];
    std::vector<double> ground_flatness_[4];
    std::vector<double> nonground_elevation_[4];
    std::vector<double> ground_elevation_[4];
    std::vector<int> nonground_num_[4];
    std::vector<int> ground_num_[4];
    int right_bin_num_[4];
    int wrong_bin_num_[4];

    double flatness_margin_;
    int noise_filter_channel_num_;
    int pc_num_channel_;
    double intensity_thr_;

    float d_;
    MatrixXf normal_;
    MatrixXf pnormal_;
    VectorXf singular_values_;
    float th_dist_d_;
    Eigen::Matrix3f cov_;
    Eigen::Vector4f pc_mean_;
    double ring_size;
    double sector_size;
    // For visualization
    bool visualize_;

    int revert_well_, revert_bad_;
    std::vector<int> reverted_TP_;
    std::vector<int> reverted_FP_;
    int num_noise_;

    vector<int> num_sectors_each_zone_;
    vector<int> num_rings_each_zone_;

    vector<double> sector_sizes;
    vector<double> ring_sizes;
    vector<double> min_ranges;
    vector<double> elevation_thr_;
    vector<double> flatness_thr_;
    vector<double> elevation_limit_;

    vector<Zone> ConcentricZoneModel_;

    jsk_recognition_msgs::PolygonArray poly_list_;

    ros::Publisher PlaneViz, revert_pc_pub, reject_pc_pub, normal_pub, pnormal_pub, init_seeds_pub, noise_pub, wall_pub;
    pcl::PointCloud<PointT> revert_pc, reject_pc;
    pcl::PointCloud<PointT> ground_pc_;
    pcl::PointCloud<PointT> non_ground_pc_;
    pcl::PointCloud<pcl::PointXYZINormal> normals_;
    pcl::PointCloud<pcl::PointXYZINormal> pnormals_;
    pcl::PointCloud<PointT> init_seeds_pc_;
    pcl::PointCloud<PointT> noise_pc_;
    pcl::PointCloud<PointT> wall_pc_;

    pcl::PointCloud<PointT> regionwise_ground_;
    pcl::PointCloud<PointT> regionwise_nonground_;

    void initialize_zone(Zone &z, int num_sectors, int num_rings);

    void flush_patches_in_zone(Zone &patches, int num_sectors, int num_rings);

    void revert_candidates(pcl::PointCloud<PointT> &cloud_out, pcl::PointCloud<PointT> &cloud_nonground,
                            std::vector<double> ring_flatness,  std::vector<double> ring_elevation,
                            std::vector<patchwork::Revert> candidates,
                            // std::vector<patchwork::MeanNormal> &mean_normals,
                            // std::vector<patchwork::MeanNormal> prev_mean_normals,
                            int concentric_idx);
    
    void calc_orthogonality(std::vector<patchwork::MeanNormal> mean_normals, std::vector<patchwork::MeanNormal> prev_mean_normals, 
                                           patchwork::Revert candidate, bool &revert_elevate, double mean_ortho, double stdev_ortho);

    void revert_candidates(pcl::PointCloud<PointT> &cloud_out, pcl::PointCloud<PointT> &cloud_nonground,
                            std::vector<std::tuple<int, double, double, pcl::PointCloud<PointT>>> candidates,
                            const double mean_flatness, const double stdev_flatness,
                            std::vector<std::tuple<int, double, double>> orthogonality_of_sectors,
                            bool check_adjflat=false, bool check_avgflat=true, bool check_ortho=false);

    void revert_candidates(pcl::PointCloud<PointT> &cloud_out, pcl::PointCloud<PointT> &cloud_nonground,
                            std::vector<std::tuple<int, double, pcl::PointCloud<PointT>>> candidates,
                            const double mean_flatness, const double stdev_flatness,
                            std::vector<std::tuple<int, double, double>> orthogonality_of_sectors,
                            bool check_adjflat=false, bool check_avgflat=true, bool check_ortho=false);


    void gen_orthogonality_of_sectors(std::vector<std::tuple<int, double, double>> &orthogonality_of_sectors,
                                      std::vector<std::tuple<int, Eigen::Vector4f, Eigen::MatrixXf>> prev_ring_mean_normals,
                                      int concentric_idx, int sector_idx);
    
    void calc_mean_stdev(std::vector<patchwork::MeanNormal> mean_normals, double &mean_ortho, double &stdev_ortho);

    void calc_mean_stdev(std::vector<double> vec, double &mean, double &stdev);
    void calc_mean_stdev(std::vector<std::tuple<int, double, pcl::PointCloud<PointT>>> vec, double &mean, double &stdev);

    void update_flatness_thr(void);
    void update_elevation_thr(void);

    double calc_principal_variance(const Eigen::Matrix3f &cov, const Eigen::Vector4f &centroid);

    double xy2theta(const double &x, const double &y);

    double xy2radius(const double &x, const double &y);

    void pc2czm(const pcl::PointCloud<PointT> &src, std::vector<Zone> &czm);

    void estimate_plane_(const pcl::PointCloud<PointT> &ground);

    void estimate_plane_(const pcl::PointCloud<PointT> &ground, double th_dist);

    void extract_piecewiseground(
            const int zone_idx, const pcl::PointCloud<PointT> &src,
            pcl::PointCloud<PointT> &dst,
            pcl::PointCloud<PointT> &non_ground_dst);

    void estimate_plane_(const int zone_idx, const pcl::PointCloud<PointT> &ground);

    void extract_initial_seeds_(
            const int zone_idx, const pcl::PointCloud<PointT> &p_sorted,
            pcl::PointCloud<PointT> &init_seeds);

    void extract_initial_seeds_(
            const int zone_idx, const pcl::PointCloud<PointT> &p_sorted,
            pcl::PointCloud<PointT> &init_seeds, double th_seed);

    void extract_initial_seeds_reverse_(
            const int zone_idx, const pcl::PointCloud<PointT> &p_sorted,
            pcl::PointCloud<PointT> &init_seeds, double th_seed);

    /***
     * For visulization of Ground Likelihood Estimation
     */
    geometry_msgs::PolygonStamped set_polygons(int r_idx, int theta_idx, int num_split);

    geometry_msgs::PolygonStamped set_polygons(int zone_idx, int r_idx, int theta_idx, int num_split);

    void set_ground_likelihood_estimation_status(
            const int k, const int ring_idx,
            const int concentric_idx,
            const double z_vec,
            const double z_elevation,
            const double surface_variable);

};

template<typename PointT> inline
void PatchWork<PointT>::initialize_zone(Zone &z, int num_sectors, int num_rings) {
    z.clear();
    pcl::PointCloud<PointT> cloud;
    cloud.reserve(1000);
    Ring ring;
    for (int i = 0; i < num_sectors; i++) {
        ring.emplace_back(cloud);
    }
    for (int j = 0; j < num_rings; j++) {
        z.emplace_back(ring);
    }
}

template<typename PointT> inline
void PatchWork<PointT>::flush_patches_in_zone(Zone &patches, int num_sectors, int num_rings) {
    for (int i = 0; i < num_sectors; i++) {
        for (int j = 0; j < num_rings; j++) {
            if (!patches[j][i].points.empty()) patches[j][i].points.clear();
        }
    }
}

template<typename PointT> inline
void PatchWork<PointT>::estimate_plane_(const pcl::PointCloud<PointT> &ground, double th_dist) {
    pcl::computeMeanAndCovarianceMatrix(ground, cov_, pc_mean_);
    // Singular Value Decomposition: SVD
    Eigen::JacobiSVD<Eigen::MatrixXf> svd(cov_, Eigen::DecompositionOptions::ComputeFullU);
    singular_values_ = svd.singularValues();

    // use the least singular vector as normal
    normal_ = (svd.matrixU().col(2));
    pnormal_ = (svd.matrixU().col(0));

    if (normal_(2) < 0) { for(int i=0; i<3; i++) normal_(i) *= -1; }

    // mean ground seeds value
    Eigen::Vector3f seeds_mean = pc_mean_.head<3>();

    // according to normal.T*[x,y,z] = -d
    d_ = -(normal_.transpose() * seeds_mean)(0, 0);
    // set distance threhold to `th_dist - d`
    th_dist_d_ = th_dist - d_;
}

template<typename PointT> inline
void PatchWork<PointT>::estimate_plane_(const pcl::PointCloud<PointT> &ground) {
    pcl::computeMeanAndCovarianceMatrix(ground, cov_, pc_mean_);
    // Singular Value Decomposition: SVD
    Eigen::JacobiSVD<Eigen::MatrixXf> svd(cov_, Eigen::DecompositionOptions::ComputeFullU);
    singular_values_ = svd.singularValues();

    // use the least singular vector as normal
    normal_ = (svd.matrixU().col(2));
    pnormal_ = (svd.matrixU().col(0));

    if (normal_(2) < 0) { for(int i=0; i<3; i++) normal_(i) *= -1; }

    // mean ground seeds value
    Eigen::Vector3f seeds_mean = pc_mean_.head<3>();

    // according to normal.T*[x,y,z] = -d
    d_ = -(normal_.transpose() * seeds_mean)(0, 0);
    // set distance threhold to `th_dist - d`
    th_dist_d_ = th_dist_ - d_;
}

template<typename PointT> inline
void PatchWork<PointT>::extract_initial_seeds_(
        const int zone_idx, const pcl::PointCloud<PointT> &p_sorted,
        pcl::PointCloud<PointT> &init_seeds, double th_seed) {
    init_seeds.points.clear();

    // LPR is the mean of low point representative
    double sum = 0;
    int cnt = 0;

    int init_idx = 0;
    if (zone_idx == 0) {
        for (int i = 0; i < p_sorted.points.size(); i++) {
            if (p_sorted.points[i].z < adaptive_seed_selection_margin_ * sensor_height_) {
                ++init_idx;
            } else {
                break;
            }
        }
    }

    // Calculate the mean height value.
    for (int i = init_idx; i < p_sorted.points.size() && cnt < num_lpr_; i++) {
        sum += p_sorted.points[i].z;
        cnt++;
    }
    double lpr_height = cnt != 0 ? sum / cnt : 0;// in case divide by 0

    // iterate pointcloud, filter those height is less than lpr.height+th_seeds_
    for (int i = 0; i < p_sorted.points.size(); i++) {
        if (p_sorted.points[i].z < lpr_height + th_seed) {
            init_seeds.points.push_back(p_sorted.points[i]);
        }
    }
}

template<typename PointT> inline
void PatchWork<PointT>::extract_initial_seeds_(
        const int zone_idx, const pcl::PointCloud<PointT> &p_sorted,
        pcl::PointCloud<PointT> &init_seeds) {
    init_seeds.points.clear();

    // LPR is the mean of low point representative
    double sum = 0;
    int cnt = 0;

    int init_idx = 0;
    if (zone_idx == 0) {
        for (int i = 0; i < p_sorted.points.size(); i++) {
            if (p_sorted.points[i].z < adaptive_seed_selection_margin_ * sensor_height_) {
                ++init_idx;
            } else {
                break;
            }
        }
    }

    // ---- Extract initial seeds in new method... (Not Good) ---- //
    // int p_size = p_sorted.points.size()-init_idx;

    // std::vector<double> h;
    // int num = 25;
    // for (int i=0; i<=num; i++)
    // {
    //     h.push_back(p_sorted.points[ int((i/num)*p_size + init_idx) ].z);
    // }
    // std::vector<double> dh;

    // for (int i=0; i<h.size()-1; i++)
    // {
    //     dh.push_back(h.at(i+1)-h.at(i));
    // }

    // int dh_min_idx = std::min_element(dh.begin(), dh.end())-dh.begin();
    // double ground_h = h.at(dh_min_idx);

    // for (int i = 0; i < p_sorted.points.size(); i++) {
    //     if (p_sorted.points[i].z < ground_h) {
    //         ++init_idx;
    //     } else {
    //         break;
    //     }
    // }

    // ---- Extract initial seeds in new method2... (Not Good) ---- //
    // double min_h = p_sorted.points[init_idx].z;
    // double max_h = p_sorted.points[p_sorted.points.size()-1].z;

    // if (max_h - min_h > 1.0 && zone_idx == 0)
    // {
    //     std::vector<double> count_num_h;
    //     int num = 15;
    //     int accumulated = init_idx;
    //     for (int i=0; i<num; i++)
    //     {
    //         double ratio = double(i+1)/double(num);
    //         double local_max_h = ratio*max_h + (1-ratio)*min_h;

    //         double accum_range = 0.0, accum_theta = 0.0;
    //         double accum_range_sq = 0.0, accum_theta_sq = 0.0;
    //         double accum_rt = 0.0, accum_rt_sq = 0.0;
    //         for (int j=accumulated; j<p_sorted.points.size(); j++)
    //         {
    //             double range_j = sqrt(p_sorted.points[j].x*p_sorted.points[j].x+p_sorted.points[j].y*p_sorted.points[j].y);
    //             double theta_j = atan2(p_sorted.points[j].y, p_sorted.points[j].x);
    //             accum_range += range_j;
    //             accum_range_sq += range_j*range_j;
    //             accum_theta += theta_j;
    //             accum_theta_sq += theta_j*theta_j;
    //             accum_rt += range_j*theta_j;
    //             accum_rt_sq += range_j*theta_j*range_j*theta_j;

    //             if (p_sorted.points[j].z <= local_max_h) continue;
    //             else
    //             {
    //                 double c = j-accumulated;
    //                 double var_x = accum_range_sq/c-(accum_range/c)*(accum_range/c);
    //                 double var_y = accum_theta_sq/c-(accum_theta/c)*(accum_theta/c);
    //                 double var_z = accum_rt_sq/c-(accum_rt/c)*(accum_rt/c);
    //                 if (c != 0) count_num_h.push_back( var_x*var_y*var_z );
    //                 else count_num_h.push_back(0);
    //                 accumulated = j;
    //                 break;
    //             }
    //         }
    //     }

    //     int max_count_num_h = std::max_element(count_num_h.begin(), count_num_h.end())-count_num_h.begin();
        
    //     for (int i=0; i<max_count_num_h; i++) { init_idx += count_num_h.at(i); }
    // }
    
    // Calculate the mean height value.
    for (int i = init_idx; i < p_sorted.points.size() && cnt < num_lpr_; i++) {
        sum += p_sorted.points[i].z;
        cnt++;
    }
    double lpr_height = cnt != 0 ? sum / cnt : 0;// in case divide by 0

    // iterate pointcloud, filter those height is less than lpr.height+th_seeds_
    for (int i = 0; i < p_sorted.points.size(); i++) {
        if (p_sorted.points[i].z < lpr_height + th_seeds_) {
            init_seeds.points.push_back(p_sorted.points[i]);
        }
    }
}

template<typename PointT> inline
void PatchWork<PointT>::extract_initial_seeds_reverse_(
        const int zone_idx, const pcl::PointCloud<PointT> &p_sorted,
        pcl::PointCloud<PointT> &init_seeds, double th_seed) {
    init_seeds.points.clear();

    // LPR is the mean of low point representative
    double sum = 0;
    int cnt = 0;

    // Calculate the mean height value.
    for (int i = p_sorted.points.size()-1; i >=0 && cnt < num_lpr_; i--) {
        sum += p_sorted.points[i].z;
        cnt++;
    }
    double lpr_height = cnt != 0 ? sum / cnt : 0;// in case divide by 0

    // iterate pointcloud, filter those height is less than lpr.height+th_seeds_
    for (int i = p_sorted.points.size()-1; i >=0; i--) {
        if (p_sorted.points[i].z > lpr_height - th_seed) {
            init_seeds.points.push_back(p_sorted.points[i]);
        }
    }
}

/*
    @brief Velodyne pointcloud callback function. The main GPF pipeline is here.
    PointCloud SensorMsg -> Pointcloud -> z-value sorted Pointcloud
    ->error points removal -> extract ground seeds -> ground plane fit mainloop
*/

template<typename PointT> inline
void PatchWork<PointT>::estimate_ground(
        const pcl::PointCloud<PointT> &cloud_in,
        pcl::PointCloud<PointT> &cloud_out,
        pcl::PointCloud<PointT> &cloud_nonground,
        double &time_taken) {
    poly_list_.header.stamp = ros::Time::now();
    if (!poly_list_.polygons.empty()) poly_list_.polygons.clear();
    if (!poly_list_.likelihood.empty()) poly_list_.likelihood.clear();

    static double start, t0, t1, t2, end;

    double pca_time_ = 0.0;
    double t_revert = 0.0;

    double t_total_ground = 0.0;
    double t_total_estimate = 0.0;
    // 1.Msg to pointcloud
    pcl::PointCloud<PointT> laserCloudIn;
    laserCloudIn = cloud_in;

    start = ros::Time::now().toSec();

    if (!original_) {
        for (int i=laserCloudIn.size()-1; i>laserCloudIn.size()-pc_num_channel_*noise_filter_channel_num_; i--) 
        {
            if (laserCloudIn[i].z > -1.5*sensor_height_) continue;

            double theta = fabs(atan2(laserCloudIn[i].y, laserCloudIn[i].x));
            // if ( min(theta, M_PI-theta) > 45.0*(M_PI/180) ) cout << "outside intensity: " << laserCloudIn[i].intensity << endl;
            // if ( min(theta, M_PI-theta) < 45.0*(M_PI/180) ) cout << "inside  intensity: " << laserCloudIn[i].intensity << endl;
            if ( min(theta, M_PI-theta) > 45.0*(M_PI/180) && laserCloudIn[i].intensity > 0.1) 
                continue;
            
            noise_pc_.points.emplace_back(laserCloudIn[i]);

            laserCloudIn[i].z = std::numeric_limits<double>::min();
        }
    }
    
    // double t_noise = ros::Time::now().toSec();
    // cout << "Find noise " << t_noise-start << endl;

    if (verbose_) cout << "noise num: " << noise_pc_.points.size() << endl;

    // ========================== Deprecated ==========================
    // 2.Sort on Z-axis value. 
    // -- slower than regionwise sorting method
    // -- it's much faster to sort in each bin ( region-wise sorting )
    
    if (original_) {
        sort(laserCloudIn.points.begin(), laserCloudIn.end(), point_z_cmp<PointT>);

        t0 = ros::Time::now().toSec();
        // 3.Error point removal
        // As there are some error mirror reflection under the ground,
        // here regardless point under 1.8* sensor_height
        // Sort point according to height, here uses z-axis in default
        auto it = laserCloudIn.points.begin();
        for (int i = 0; i < laserCloudIn.points.size(); i++) {
            if (laserCloudIn.points[i].z < -1.8 * sensor_height_) {
                noise_pc_.points.emplace_back(laserCloudIn[i]);
                it++;
            } else {
                break;
            }
        }
        laserCloudIn.points.erase(laserCloudIn.points.begin(), it);
    }
    // =================================================================

    t1 = ros::Time::now().toSec();
    // 4. pointcloud -> regionwise setting
    for (int k = 0; k < num_zones_; ++k) {
        flush_patches_in_zone(ConcentricZoneModel_[k], num_sectors_each_zone_[k], num_rings_each_zone_[k]);
    }
    pc2czm(laserCloudIn, ConcentricZoneModel_);

    t2 = ros::Time::now().toSec();

    cloud_out.clear();
    cloud_nonground.clear();
    revert_pc.clear();
    reject_pc.clear();
    init_seeds_pc_.clear();
    normals_.clear();
    pnormals_.clear();
    
    reverted_TP_.clear();
    reverted_FP_.clear();
    revert_well_ = 0;
    revert_bad_ = 0;

    int concentric_idx = 0;

    //std::vector<std::tuple<int, Eigen::Vector4f, Eigen::MatrixXf>> curr_ring_mean_normals;
    //std::vector<std::tuple<int, Eigen::Vector4f, Eigen::MatrixXf>> prev_ring_mean_normals;

    double t_sort = 0;

    for (int k = 0; k < num_zones_; ++k) {
        auto zone = ConcentricZoneModel_[k];

        // std::vector<patchwork::MeanNormal> prev_mean_normals;
        
        for (uint16_t ring_idx = 0; ring_idx < num_rings_each_zone_[k]; ++ring_idx) {
            
            std::vector<double> ring_flatness;
            std::vector<double> ring_elevation;
            std::vector<double> ring_width;
            
            // Revert candidates
            std::vector<patchwork::Revert> candidates;
            
            // MeanNormal
            // std::vector<patchwork::MeanNormal> mean_normals;
            // mean_normals.reserve(num_sectors_each_zone_[k]);

            // patchwork::MeanNormal mn;
            // mn.save = false;
            // for (size_t i=0; i<num_sectors_each_zone_[k]; i++) { mean_normals.push_back(mn); }

            //std::vector<std::tuple<int, double, double>> orthogonality_of_sectors;
            //curr_ring_mean_normals.clear();

            //for (uint16_t sector_idx = 0; sector_idx < num_sectors_each_zone_[k]; ++sector_idx) {
            for (int sector_idx = 0; sector_idx < num_sectors_each_zone_[k]; ++sector_idx) {
                if (zone[ring_idx][sector_idx].points.size() > num_min_pts_) {

                    // region-wise sorting (faster than global sorting method)
                    if (!original_) {
                        double delta_t = ros::Time::now().toSec();
                        sort(zone[ring_idx][sector_idx].points.begin(), zone[ring_idx][sector_idx].points.end(), point_z_cmp<PointT>);
                        auto it = laserCloudIn.points.begin();
                        for (int i = 0; i < laserCloudIn.points.size(); i++) {                        
                            if (laserCloudIn[i].z == std::numeric_limits<double>::min()) it++;
                            else break;
                        }
                        laserCloudIn.points.erase(laserCloudIn.points.begin(), it);

                        double t = ros::Time::now().toSec();
                        t_sort += (t - delta_t);
                    }

                    double t_tmp0 = ros::Time::now().toSec();
                    extract_piecewiseground(k, zone[ring_idx][sector_idx], regionwise_ground_, regionwise_nonground_);
                    
                    // To analyze the flatness property of ground points only !!!!!
                    // Don't uncomment below lines !!!!
                    // estimate_plane_(zone[ring_idx][sector_idx]);
                    // cloud_out += zone[ring_idx][sector_idx];

                    double t_tmp1 = ros::Time::now().toSec();
                    t_total_ground += t_tmp1 - t_tmp0;
                    pca_time_ += t_tmp1 - t_tmp0;

                    // Status of each patch
                    // used in checking uprightness, elevation, and flatness, respectively
                    const double ground_z_vec = abs(normal_(2, 0));
                    const double ground_z_elevation = pc_mean_(2, 0);
                    const double surface_variable = singular_values_.minCoeff();
                    // surface_variable = singular_values_.minCoeff()/(singular_values_(0) + singular_values_(1) + singular_values_(2));
                    // if (surface_variable == 0) 
                    // { 
                    //     cout << "sector_idx: " << sector_idx << endl;
                    //     cout << "sv: " << singular_values_(0) << ", " << singular_values_(1) << ", " << singular_values_(2) << endl;
                    // }
                    // const double surface_width = 4*sqrt(singular_values_(0)*singular_values_(1));
                    // const double surface_variable = singular_values_.minCoeff() /
                    //         (singular_values_(0) + singular_values_(1) + singular_values_(2));
                    const double line_variable = singular_values_(0)/singular_values_(1);
                    double line_dir = 0.0; // 0 ~ M_PI/2 rad larger, worse
                    for (int i=0; i<2; i++) { line_dir += pnormal_(i,0)*pc_mean_(i,0); }
                    double r = sqrt(pc_mean_(0,0)*pc_mean_(0,0)+pc_mean_(1,0)*pc_mean_(1,0));
                    if (r != 0.0) line_dir = acos(fabs(line_dir/r));
                    else line_dir = 0.0;

                    if (visualize_) {
                        auto polygons = set_polygons(k, ring_idx, sector_idx, 3);
                        polygons.header = poly_list_.header;
                        poly_list_.polygons.push_back(polygons);
                        set_ground_likelihood_estimation_status(k, ring_idx, concentric_idx, ground_z_vec,
                                                                ground_z_elevation, surface_variable);

                        pcl::PointXYZINormal tmp_p;
                        tmp_p.x = pc_mean_(0,0);
                        tmp_p.y = pc_mean_(1,0);
                        tmp_p.z = pc_mean_(2,0);
                        tmp_p.normal_x = normal_(0,0);
                        tmp_p.normal_y = normal_(1,0);
                        tmp_p.normal_z = normal_(2,0);
                        normals_.points.emplace_back(tmp_p);

                        pcl::PointXYZINormal tmp_s;
                        tmp_s.x = pc_mean_(0,0);
                        tmp_s.y = pc_mean_(1,0);
                        tmp_s.z = pc_mean_(2,0);
                        tmp_s.normal_x = pnormal_(0,0);
                        tmp_s.normal_y = pnormal_(1,0);
                        tmp_s.normal_z = pnormal_(2,0);
                        pnormals_.points.emplace_back(tmp_s);
                    }

                    // if (ground_z_vec >= uprightness_thr_ && concentric_idx < 2)
                    // {
                    //     patchwork::MeanNormal mn;
                    //     mn.save = true;
                    //     mn.elevated = (ground_z_elevation > elevation_thr_[concentric_idx]);
                    //     mn.mean.x = pc_mean_(0,0);
                    //     mn.mean.y = pc_mean_(1,0);
                    //     mn.mean.z = pc_mean_(2,0);
                    //     mn.normal.x = normal_(0,0);
                    //     mn.normal.y = normal_(1,0);
                    //     mn.normal.z = normal_(2,0);
                    //     mean_normals.at(sector_idx) = mn;
                    // }

                    if (poly_list_.likelihood.back() == (float)UPRIGHT_ENOUGH && concentric_idx < num_rings_of_interest_ && !original_)
                    {
                        ring_flatness.push_back(surface_variable);
                        ring_elevation.push_back(ground_z_elevation);
                        // ring_width.push_back(surface_width);
                        
                        update_flatness_[concentric_idx].push_back(surface_variable);
                        update_elevation_[concentric_idx].push_back(ground_z_elevation);
                        if (matlab_analyze_) update_def_num_[concentric_idx].push_back(regionwise_ground_.size());
                    }

                    // To analyze the flantess of nonground which needs to be removed
                    if ( concentric_idx < num_rings_of_interest_ && matlab_analyze_)
                    {
                        int num_veg = 0;
                        for (auto const& pt: regionwise_ground_.points)
                        {
                            if (pt.label == 70) num_veg++;
                        }
                        int regionwise_TP = count_num_ground_without_vegetation(regionwise_ground_);
                        int regionwise_FP = regionwise_ground_.size() - num_veg - regionwise_TP;

                        if (poly_list_.likelihood.back() == (float)UPRIGHT_ENOUGH)
                        {
                            right_bin_num_[concentric_idx] += regionwise_TP;
                            wrong_bin_num_[concentric_idx] += regionwise_FP;
                        }

                        if (regionwise_FP > regionwise_TP && ground_z_vec > uprightness_thr_ && ground_z_elevation > elevation_thr_[concentric_idx])
                        {
                            nonground_flatness_[concentric_idx].push_back(surface_variable);
                            nonground_elevation_[concentric_idx].push_back(ground_z_elevation);
                            nonground_num_[concentric_idx].push_back(regionwise_FP);
                        }

                        if (regionwise_FP <= regionwise_TP && ground_z_vec > uprightness_thr_ && ground_z_elevation > elevation_thr_[concentric_idx])
                        {
                            ground_flatness_[concentric_idx].push_back(surface_variable);
                            ground_elevation_[concentric_idx].push_back(ground_z_elevation);
                            ground_num_[concentric_idx].push_back(regionwise_TP);
                        }

                        // if (regionwise_FP > regionwise_TP )
                        // {
                        //     nonground_flatness_[concentric_idx].push_back(surface_variable);
                        //     nonground_elevation_[concentric_idx].push_back(ground_z_elevation);
                        //     nonground_num_[concentric_idx].push_back(regionwise_FP);
                        // }

                        // if (regionwise_FP <= regionwise_TP )
                        // {
                        //     ground_flatness_[concentric_idx].push_back(surface_variable);
                        //     ground_elevation_[concentric_idx].push_back(ground_z_elevation);
                        //     ground_num_[concentric_idx].push_back(regionwise_TP);
                        // }
                    }          
                    
                    // heading should be smaller than 0 ( < 0 ) (theoretically)
                    // however, when there's not enough ring since sector is far awaw,
                    // it could be smaller than 0 even if it's ground.
                    // Therefore, we only check this value when concentric_idx < num_rings_of_interest

                    bool necessary_condition = true;

                    if (!original_)
                    {
                        double heading = 0.0;
                        for(int i=0; i<3; i++) heading += pc_mean_(i,0)*normal_(i,0);

                        double pc_mean_rad = sqrt(pc_mean_(0,0)*pc_mean_(0,0)+pc_mean_(1,0)*pc_mean_(1,0));
                        double min_rad = abs(sensor_height_*2.14450692051)-0.1;

                        necessary_condition = (heading < 0.0);// && pc_mean_rad > min_rad);

                        if (pc_mean_rad < min_rad)
                        {
                            int low_intensity_num_f = 0;
                            int total_num_f = 0;
                            double ratio = 0.0;
                            for (auto const &pt:regionwise_ground_.points)
                            {
                                if (sqrt(pt.x*pt.x+pt.y*pt.y) < pc_mean_rad)
                                {
                                    if (pt.intensity < intensity_thr_) low_intensity_num_f++;
                                    total_num_f++;
                                }
                            }

                            if (total_num_f != 0)
                            {
                                ratio = (double)low_intensity_num_f/(double)total_num_f;
                                if ( ratio > 0.3) necessary_condition = false;
                            }

                            if (verbose_)  cout << "ratio: " << ratio << "= " << low_intensity_num_f << "/" << total_num_f << endl;
                        }

                        if (verbose_ && !necessary_condition  && concentric_idx < num_rings_of_interest_) 
                        {
                            cout << sector_idx << "th pc_mean_rad: " << pc_mean_rad << ", " << min_rad << endl;
                            cout << "Heading: " << heading << endl;
                        }

                        // necessary_condition = heading < 0.0;
                    } 

                    double t_tmp2 = ros::Time::now().toSec();
                    if (ground_z_vec < uprightness_thr_) {
                        // All points are rejected
                        cloud_nonground += regionwise_ground_;
                        cloud_nonground += regionwise_nonground_;
                    } else { // satisfy uprightness
                        if (concentric_idx < num_rings_of_interest_) {
                            if ( necessary_condition ) {
                                if (ground_z_elevation > elevation_thr_[concentric_idx]) {
                                    if (flatness_thr_[concentric_idx] > surface_variable) {
                                        if (verbose_) 
                                        {
                                            std::cout << "\033[1;36m[Flatness] Recovery operated. Check "
                                                    << concentric_idx
                                                    << "th param[" << sector_idx << "]. flatness_thr_: " << flatness_thr_[concentric_idx]
                                                    << " > "
                                                    << surface_variable << "\033[0m" << std::endl;
                                        }
                                        // revert_pc += regionwise_ground_;
                                        
                                        cloud_out += regionwise_ground_;
                                        cloud_nonground += regionwise_nonground_;
                                    } else {
                                        if (original_) {
                                            if (verbose_) {
                                                std::cout << "\033[1;34m[Flatness] Recovery rejected. Check "
                                                        << concentric_idx
                                                        << "th param[" << sector_idx << "]. flatness_thr_: " << flatness_thr_[concentric_idx]
                                                        << " < "
                                                        << surface_variable << "\033[0m" << std::endl;
                                            
                                                std::cout << "\033[1;34m[Elevation] Rejection operated. Check "
                                                        << concentric_idx
                                                        << "th param[" << sector_idx << "]. of elevation_thr_: " << elevation_thr_[concentric_idx]
                                                        << " < "
                                                        << ground_z_elevation << "\033[0m" << std::endl;
                                            }
                                            reject_pc += regionwise_ground_;
                                            cloud_nonground += regionwise_ground_;
                                            cloud_nonground += regionwise_nonground_;
                                        } else {
                                            
                                            patchwork::Revert candidate;
                                            sensor_msgs::PointCloud2 cloud_msg;
                                            pcl::toROSMsg(regionwise_ground_, cloud_msg);
                                            candidate.sector_idx = sector_idx;
                                            candidate.surface_variable = surface_variable;
                                            // candidate.surface_width = surface_width;
                                            candidate.line_variable = line_variable;
                                            // candidate.ground_z_elevation = ground_z_elevation;
                                            // candidate.ground_z_vec = ground_z_vec;
                                            candidate.regionwise_ground = cloud_msg;
                                            // candidate.non_ground_num = regionwise_nonground_.size();
                                            candidate.line_dir = line_dir;
                                            candidate.pc_mean_x = pc_mean_(0,0);
                                            candidate.pc_mean_y = pc_mean_(1,0);
                                            candidate.pc_mean_z = pc_mean_(2,0);
                                            candidates.push_back(candidate);

                                            cloud_nonground += regionwise_nonground_;
                                        }
                                    }
                                } else {
                                    cloud_out += regionwise_ground_;
                                    cloud_nonground += regionwise_nonground_;
                                }
                            } else {
                                cloud_nonground += regionwise_ground_;
                                cloud_nonground += regionwise_nonground_;
                            }
                        } else {
                            cloud_out += regionwise_ground_;
                            cloud_nonground += regionwise_nonground_;
                        }
                    }
                    double t_tmp3 = ros::Time::now().toSec();
                    t_total_estimate += t_tmp3 - t_tmp2;
                }
            }

            // prev_ring_mean_normals = curr_ring_mean_normals;

            double t_bef_revert = ros::Time::now().toSec();

            if(ring_flatness.size()>1 && !original_)
            {
                revert_candidates(cloud_out, cloud_nonground, ring_flatness, ring_width, candidates, concentric_idx);
                // revert_candidates(cloud_out, cloud_nonground, ring_flatness, ring_width, candidates, mean_normals, prev_mean_normals, concentric_idx);
            }

            double t_aft_revert = ros::Time::now().toSec();

            t_revert += t_aft_revert - t_bef_revert;

            // prev_mean_normals = mean_normals;

            ++concentric_idx;
        }
    }    
    
    cloud_nonground += noise_pc_;

    double t_update = ros::Time::now().toSec();

    if (!original_) update_flatness_thr();
    if (!original_) update_elevation_thr();
    
    end = ros::Time::now().toSec();
    time_taken = end - start;

    cout << "Time taken : " << time_taken << endl;
    // cout << "Time taken to sort: " << t_sort << endl;
    // cout << "Time taken to pca : " << pca_time_ << endl;
    // cout << "Time taken to estimate: " << t_total_estimate << endl;
    // cout << "Time taken to revert: " <<  t_revert << endl;
    // cout << "Time taken to update : " << end - t_update << endl;
//    ofstream time_txt("/home/shapelim/patchwork_time_anal.txt", std::ios::app);
//    time_txt<<t0 - start<<" "<<t1 - t0 <<" "<<t2-t1<<" "<<t_total_ground<< " "<<t_total_estimate<<"\n";
//    time_txt.close();

    if (verbose_) {
        sensor_msgs::PointCloud2 cloud_ROS;
        pcl::toROSMsg(revert_pc, cloud_ROS);
        cloud_ROS.header.stamp = ros::Time::now();
        cloud_ROS.header.frame_id = "/map";
        revert_pc_pub.publish(cloud_ROS);
        pcl::toROSMsg(reject_pc, cloud_ROS);
        cloud_ROS.header.stamp = ros::Time::now();
        cloud_ROS.header.frame_id = "/map";
        reject_pc_pub.publish(cloud_ROS);

        pcl::toROSMsg(normals_, cloud_ROS);
        cloud_ROS.header.stamp = ros::Time::now();
        cloud_ROS.header.frame_id = "/map";
        normal_pub.publish(cloud_ROS);

        pcl::toROSMsg(pnormals_, cloud_ROS);
        cloud_ROS.header.stamp = ros::Time::now();
        cloud_ROS.header.frame_id = "/map";
        pnormal_pub.publish(cloud_ROS);

        pcl::toROSMsg(init_seeds_pc_, cloud_ROS);
        cloud_ROS.header.stamp = ros::Time::now();
        cloud_ROS.header.frame_id = "/map";
        init_seeds_pub.publish(cloud_ROS);

        pcl::toROSMsg(noise_pc_, cloud_ROS);
        cloud_ROS.header.stamp = ros::Time::now();
        cloud_ROS.header.frame_id = "/map";
        noise_pub.publish(cloud_ROS);

        pcl::toROSMsg(wall_pc_, cloud_ROS);
        cloud_ROS.header.stamp = ros::Time::now();
        cloud_ROS.header.frame_id = "/map";
        wall_pub.publish(cloud_ROS);

        num_noise_ = noise_pc_.size();
        noise_pc_.clear();
        wall_pc_.clear();
    }
    PlaneViz.publish(poly_list_);
}

template<typename PointT> inline
void PatchWork<PointT>::update_flatness_thr(void)
{
    // if (update_flatness_.empty()) return;

    for (int i=0; i<num_rings_of_interest_; i++)
    {
        if (update_flatness_[i].empty()) break;
        if (update_flatness_[i].size() <= 1) break;

        double update_mean = 0.0, update_stdev = 0.0;
        calc_mean_stdev(update_flatness_[i], update_mean, update_stdev);
        flatness_thr_[i] = update_mean+update_stdev;

        if (verbose_) { cout << "flatness threshold [" << i << "]: " << flatness_thr_[i] << endl; }
    }
    
    double mean_flat_thr = (sqrt(flatness_thr_[0])+sqrt(flatness_thr_[1])+sqrt(flatness_thr_[2])+sqrt(flatness_thr_[3]))/4;
    th_dist_ = min(4*mean_flat_thr, 0.2);
    th_seeds_ = 2*th_dist_;

    if (verbose_) cout << "th_dist_: " << th_dist_ << ", th_seeds_: " << th_seeds_ << endl;

    return;
}

template<typename PointT> inline
void PatchWork<PointT>::update_elevation_thr(void)
{
    // if (update_elevation_.empty()) return;

    for (int i=0; i<num_rings_of_interest_; i++)
    {
        if (update_elevation_[i].empty()) continue;

        double update_mean = 0.0, update_stdev = 0.0;
        calc_mean_stdev(update_elevation_[i], update_mean, update_stdev);
        if (i==0) {
            elevation_thr_[i] = update_mean + 3*update_stdev;
            sensor_height_ = -update_mean;
        }
        else elevation_thr_[i] = update_mean + 2*update_stdev;

        if (verbose_) cout << "elevation threshold [" << i << "]: " << update_mean << ", " << update_stdev << ", " << elevation_thr_[i] << endl;
        if (verbose_) cout << "sensor height: " << sensor_height_ << endl;
    }


    return;
}


template<typename PointT> inline
void PatchWork<PointT>::revert_candidates(pcl::PointCloud<PointT> &cloud_out, pcl::PointCloud<PointT> &cloud_nonground,
                                          std::vector<double> ring_flatness, std::vector<double> ring_width,
                                          std::vector<patchwork::Revert> candidates,
                                        //   std::vector<patchwork::MeanNormal> &mean_normals,
                                        //   std::vector<patchwork::MeanNormal> prev_mean_normals,
                                          int concentric_idx)
{
    if (verbose_) std::cout << "=========== Let's Revert : " << concentric_idx << " ===========" << endl;

    std::vector<int> reverted_candidates;

    double mean_flatness = 0.0, stdev_flatness = 0.0;
    calc_mean_stdev(ring_flatness, mean_flatness, stdev_flatness);
    
    if (verbose_) cout << "mean_flatness: " << mean_flatness << ", stdev_flatness: " << stdev_flatness;

    for( size_t i=0; i<candidates.size(); i++ )
    {
        patchwork::Revert candidate = candidates.at(i);

        // auto it = find(reverted_candidates.begin(), reverted_candidates.end(), candidate.sector_idx);
        // if (it != reverted_candidates.end()) continue;

        // For counting reverted TPs and FPs
        pcl::PointCloud<PointT> pc;
        pcl::fromROSMsg(candidate.regionwise_ground, pc);
        int num_veg = 0, TP = 0, FP = 0;
        for (auto const& pt: pc.points) { if (pt.label == 70) num_veg++; } // 70 for vegetation label
        TP = count_num_ground_without_vegetation(pc);
        FP = pc.size() - num_veg - TP;
                
        // Debug
        if(verbose_)
        {
            cout << "\033[1;33m" << candidate.sector_idx << "th flat_sector_candidate"
                 << " / flatness: " << candidate.surface_variable
                //  << " / surface_width: " << candidate.surface_width
                 << " / line_variable: " << candidate.line_variable
                //  << " / elevation height : " << candidate.ground_z_elevation
                //  << " / uprightness : " << candidate.ground_z_vec
                //  << " / non_ground_num : " << candidate.non_ground_num
                 << " / ground_num : " << pc.size() 
                 << "\033[0m" << endl;
        }
            
        // Caculate orthogonality
        // bool revert_elevate = true;
        // if ( concentric_idx < 1 ) revert_elevate = !mean_normals.at(candidate.sector_idx).elevated;
        // if ( concentric_idx < 2 ) calc_orthogonality(mean_normals, prev_mean_normals, candidate, revert_elevate);

        // double mu_flatness = max_flatness;
        double mu_flatness = mean_flatness + 1.5*stdev_flatness + flatness_margin_;
        // mu_flatness = max( mean_flatness + 1.5*stdev_flatness, m_flatness + 1.5*s_flatness);
        // if (concentric_idx == 0) mu_flatness = mean_flatness + 0.5*stdev_flatness;
        // double mu_flatness = mean_flatness*2.4564438;
        double prob_flatness = 1/(1+exp( (candidate.surface_variable-mu_flatness)/(mu_flatness/10) ));

        if (pc.size() > 1500 && candidate.surface_variable < th_dist_*th_dist_) prob_flatness = 1.0;

        // double mu_nonground = pc.size()/2.5;
        // double prob_nonground = 1/(1+exp( (candidate.non_ground_num-mu_nonground)/(mu_nonground/2) ));
        // if (concentric_idx > 0) prob_nonground = 0.5;
        
        // double mu_ground = 1500/(1+2*log(concentric_idx+1));
        // double prob_ground = 1 - 1/(1+exp( (pc.size()-mu_ground)/(mu_ground/5) ));
        // if (concentric_idx > 0) prob_ground = 0.5;

        // double w_ground = 0.6;
        // double w_nonground = 1 - w_ground;

        double prob_cluster = 1.0;
        if (concentric_idx < 2)
        {
            int count = 0;
            for (const auto &pt: pc.points)
            {
                double dx = pt.x-candidate.pc_mean_x;
                double dy = pt.y-candidate.pc_mean_y;
                double r = sqrt(dx*dx+dy*dy);

                if ( r < 0.5 ) count++;
                if ( count > 10 ) break;
            }

            // if (verbose_) cout << "count: " << count << endl; 
            if (count <= 10) prob_cluster = 0.0;
        }
        
        double prob_line = 1.0;
        if (candidate.line_variable > 8.0 && candidate.line_dir > M_PI/4)// candidate.ground_z_elevation > elevation_thr_[concentric_idx]) 
        {
            if (verbose_) cout << "line_dir: " << candidate.line_dir << endl;
            prob_line = 0.0;
        }

        double prob_intensity = 1.0;
        if (concentric_idx == 0)
        {
            double range = sqrt(candidate.pc_mean_x*candidate.pc_mean_x+candidate.pc_mean_y*candidate.pc_mean_y);

            int low_intensity_num_f = 0;
            int low_intensity_num = 0;
            int total_num_f = 0;
            int total_num = 0;
            for (auto const &pt:pc.points)
            {
                if (sqrt(pt.x*pt.x+pt.y*pt.y) < range)
                {
                    if (pt.intensity < intensity_thr_) low_intensity_num_f++;
                    total_num_f++;
                }

                if (pt.intensity < intensity_thr_) low_intensity_num++;
                total_num++;                
            }

            if (total_num_f != 0)
            {
                if ( (double)low_intensity_num_f/(double)total_num_f > 0.3) prob_intensity = 0.0;
                if ( (double)low_intensity_num/(double)total_num > 0.3) prob_intensity = 0.0;
                if (verbose_) cout << "low intensity_num_f : " << low_intensity_num_f << ", total_num_f : " << total_num_f << endl;
                if (verbose_) cout << "low intensity_num: " << low_intensity_num << ", total_num : " << total_num << endl;
            }
        }

        // double mu_rad = abs(sensor_height_*2.14450692051) + 0.2; // 2.14450692051 = tan(65 deg)
        // double pc_mean_rad = sqrt(candidate.pc_mean_x*candidate.pc_mean_x+candidate.pc_mean_y*candidate.pc_mean_y);
        // double prob_near =  1 - 1/(1+exp( (pc_mean_rad-mu_rad)/(mu_rad/2) ));

        // bool revert = prob_near*prob_cluster*prob_line*sqrt(prob_flatness)*(w_nonground*prob_nonground+w_ground*prob_ground) > 0.125;

        // bool revert = prob_flatness > 0.5;

        double prob_num = 1.0;
        // if (pc.size() < 300) prob_num = 0.0;

        bool revert = prob_num*prob_line*prob_flatness*prob_intensity*prob_cluster > 0.5;

        // Most inner two rings
        // Consider elevation
        if ( concentric_idx < num_rings_of_interest_ )
        {
            if (verbose_)
            {
                cout << "prob flat: " << prob_flatness 
                     << ", prob line: " << prob_line 
                     << ", prob intensity:" << prob_intensity 
                     << ", prob num:" << prob_num
                     << ", prob cluster" << prob_cluster
                     << endl;
            }
            
            if (revert)
            {
                if (verbose_)
                {
                    // if (revert_flatness) cout << "\033[1;31m" << "REVERT TRUE :: FLATNESS" << endl;
                    // if (revert_width) cout << "\033[1;31m" << "REVERT TRUE :: WIDTH" << endl;
                    cout << "\033[1;32m" << "REVERT TRUE - " << "After / Catch TPs: " << TP << " , FPs: " << FP << ", num_veg: " << num_veg << "\033[0m" << endl;
                }
                
                revert_pc += pc;
                cloud_out += pc;
                reverted_TP_.push_back(TP);
                reverted_FP_.push_back(FP);
                
                if (TP>FP) revert_well_++;
                if (TP==0 && FP>TP) revert_bad_++;

                reverted_candidates.push_back(candidate.sector_idx);

                // mean_normals.at(candidate.sector_idx).elevated = false;
            }
            else
            {
                if (verbose_) 
                {
                    cout << "\033[1;31m" << "FINAL REJECT - " << "After / Loss TPs: " << TP << ", FPs: " << FP << "\033[0m" << endl;
                }
                reject_pc += pc;
                cloud_nonground += pc;
            }                
        }
    }

    // if (true)
    // {
    //     std::vector<int> adjacent_flat_sector;
    //     for ( int i=0; i<candidates.size(); i++ )
    //     {
    //         patchwork::Revert candidate = candidates.at(i);

    //         // Save adjacent sectors into adjacent_flat_sector
    //         bool end_of_adjacent_sector = false;
    //         if (adjacent_flat_sector.empty())
    //         {
    //             adjacent_flat_sector.push_back(candidate.sector_idx);
    //         }
    //         else if ( adjacent_flat_sector.back() == candidate.sector_idx-1 )
    //         {
    //             adjacent_flat_sector.push_back(candidate.sector_idx);
    //         }
    //         else end_of_adjacent_sector = true;

    //         if (i == candidates.size()-1) end_of_adjacent_sector = true;

    //         // If the sector is not continuous anymore, analyze the previous adjacent sectors
    //         if (end_of_adjacent_sector)
    //         {
    //             bool adj_flat = false;
    //             if (concentric_idx < num_rings_of_interest_)
    //             {
    //                 for (int j=0; j<adjacent_flat_sector.size(); j++)
    //                 {
    //                     auto it = find(reverted_candidates.begin(), reverted_candidates.end(), adjacent_flat_sector.at(j));
    //                     if (it != reverted_candidates.end()) adj_flat = true;
    //                     if (adj_flat) break;
    //                 }
    //             }
    //             else adj_flat = true;
                
    //             if (adjacent_flat_sector.size() < 2) adj_flat = false;

    //             int idx_c = -1;
    //             for ( int k=0; k<candidates.size(); k++)
    //             {
    //                 if (adjacent_flat_sector.at(0) == candidates.at(k).sector_idx)
    //                 {
    //                     idx_c = k;
    //                     break;
    //                 }
    //             }

    //             if (idx_c == -1) cout << "ERROR!!!!!!!!!!!!!!" << endl;

    //             for (int j=0; j<adjacent_flat_sector.size(); j++)
    //             {
    //                 auto it = find(reverted_candidates.begin(), reverted_candidates.end(), adjacent_flat_sector.at(j));
    //                 if (it != reverted_candidates.end()) continue;

    //                 it = find(no_reverted_candidates.begin(), no_reverted_candidates.end(), adjacent_flat_sector.at(j));
    //                 if (it != no_reverted_candidates.end()) continue;

    //                 pcl::PointCloud<PointT> pc;
    //                 pcl::fromROSMsg(candidates.at(idx_c+j).regionwise_ground, pc);
    //                 int num_veg = 0, TP = 0, FP = 0;
    //                 for (auto const& pt: pc.points) { if (pt.label == 70) num_veg++; } // 70 for vegetation label
    //                 TP = count_num_ground_without_vegetation(pc);
    //                 FP = pc.size() - num_veg - TP;

    //                 if (adj_flat) {
    //                     revert_pc += pc;
    //                     cloud_out += pc;
    //                     reverted_TP_.push_back(TP);
    //                     reverted_FP_.push_back(FP);
                        
    //                     if (TP>FP) revert_well_++;
    //                     if (TP==0 && FP>TP) revert_bad_++;
                        
    //                     if (verbose_) cout << "\033[1;32m" << adjacent_flat_sector.at(j) << "th *ADJ* REVERT TRUE - " << "After / Catch TPs: " << TP << " , FPs: " << FP << ", num_veg: " << num_veg << "\033[0m" << endl;
    //                 }
    //                 else {
    //                     if (verbose_) cout << "\033[1;31m" << "FINAL REJECT - " << "After / Loss TPs: " << TP << ", FPs: " << FP << "\033[0m" << endl;
                        
    //                     reject_pc += pc;
    //                     cloud_nonground += pc;
    //                 }
    //             }
    //             adjacent_flat_sector.clear();
    //             adjacent_flat_sector.push_back(candidate.sector_idx);
    //         }
    //     }
    // }

    if (verbose_) cout << "Revert well: " << revert_well_ << ", Revert bad: " << revert_bad_ << endl;

    // Deprecated --------------- orthogonality analysis ----------------------

    // double mean_ortho = 0.0, stdev_ortho = 0.0;
    // if (concentric_idx < 1) calc_mean_stdev(mean_normals, mean_ortho, stdev_ortho);
    // if (verbose_) std::cout << "mean_ortho: " << mean_ortho << ", stdev_ortho: " << stdev_ortho << endl;      
    
    // //CW
    // std::vector<int> reverted_candidates_idx;
    // for( size_t i=0; i<candidates.size(); i++ )
    // {
    //     patchwork::Revert candidate = candidates.at(i);

    //     bool revert_elevate = false;
    //     if ( concentric_idx < 1 ) calc_orthogonality(mean_normals, prev_mean_normals, candidate, revert_elevate, mean_ortho, stdev_ortho);

    //     if ( revert_elevate ) // && candidate.surface_variable < 0.01 )
    //     {
    //         ring_flatness.push_back(candidate.surface_variable);

    //         pcl::PointCloud<PointT> pc;
    //         pcl::fromROSMsg(candidate.regionwise_ground, pc);
    //         revert_pc += pc;
    //         cloud_out += pc;

    //         mean_normals.at(candidate.sector_idx).elevated = false;
    //         reverted_candidates_idx.push_back(candidate.sector_idx);
    //     }
    // }

    // // CCW
    // for( size_t i=0; i<candidates.size(); i++ )
    // {
    //     patchwork::Revert candidate = candidates.at(candidates.size()-1-i);

    //     auto it = find(reverted_candidates_idx.begin(), reverted_candidates_idx.end(), candidate.sector_idx);
    //     if (it != reverted_candidates_idx.end()) continue;

    //     bool revert_elevate = false;
    //     if ( concentric_idx < 1 ) calc_orthogonality(mean_normals, prev_mean_normals, candidate, revert_elevate, mean_ortho, stdev_ortho);

    //     if ( revert_elevate ) //&& candidate.surface_variable < 0.01 )
    //     {
    //         ring_flatness.push_back(candidate.surface_variable);

    //         pcl::PointCloud<PointT> pc;
    //         pcl::fromROSMsg(candidate.regionwise_ground, pc);
    //         revert_pc += pc;
    //         cloud_out += pc;

    //         mean_normals.at(candidate.sector_idx).elevated = false;
    //         reverted_candidates_idx.push_back(candidate.sector_idx);
    //     }
    // }
}

template<typename PointT> inline
void PatchWork<PointT>::calc_mean_stdev(std::vector<patchwork::MeanNormal> mean_normals, 
                                        double &mean_ortho, double &stdev_ortho)
{
    int count = 0;
    for ( size_t i=0; i<mean_normals.size(); i++ )
    {
        patchwork::MeanNormal mn = mean_normals.at(i);

        if (mn.elevated) continue;

        count++;

        double left_ortho = std::numeric_limits<double>::quiet_NaN();
        double right_ortho = std::numeric_limits<double>::quiet_NaN();

        int left_idx = i-1;
        int right_idx = i+1;

        if ( left_idx == -1 ) left_idx = mean_normals.size()-1;
        if ( right_idx == mean_normals.size() ) right_idx = 0;

        // ortho = normal*vec_pc_mean_ = difference in the direction of normal
        patchwork::MeanNormal left_mn = mean_normals.at(left_idx);
        if (left_mn.save)
        {
            if (!left_mn.elevated)
            {
                left_ortho = 0.0;
                left_ortho += mn.normal.x*(left_mn.mean.x-mn.mean.x);
                left_ortho += mn.normal.y*(left_mn.mean.y-mn.mean.y);
                left_ortho += mn.normal.z*(left_mn.mean.z-mn.mean.z);
            }
        }

        patchwork::MeanNormal right_mn = mean_normals.at(right_idx);
        if (right_mn.save)
        {
            if (!right_mn.elevated)
            {   
                right_ortho = 0.0;
                right_ortho += mn.normal.x*(right_mn.mean.x-mn.mean.x);
                right_ortho += mn.normal.y*(right_mn.mean.y-mn.mean.y);
                right_ortho += mn.normal.z*(right_mn.mean.z-mn.mean.z);
            }   
        }

        if(!isnan(left_ortho)) 
        {
            mean_ortho += abs(left_ortho);
            stdev_ortho += left_ortho*left_ortho;
        }

        if(!isnan(right_ortho)) 
        {
            mean_ortho += abs(right_ortho);
            stdev_ortho += right_ortho*right_ortho;
        }
    }

    if ( count > 1 )
    {
        mean_ortho = mean_ortho/count;

        stdev_ortho -= mean_ortho*mean_ortho;
        stdev_ortho /= count-1;
    }
}

template<typename PointT> inline
void PatchWork<PointT>::calc_orthogonality(std::vector<patchwork::MeanNormal> mean_normals, std::vector<patchwork::MeanNormal> prev_mean_normals, 
                                           patchwork::Revert candidate, bool &revert_elevate, double mean_ortho, double stdev_ortho)
{
    // Check orthogonality corresponding to left/right ground
    double left_ortho = std::numeric_limits<double>::quiet_NaN();
    double right_ortho = std::numeric_limits<double>::quiet_NaN();
    double front_ortho = std::numeric_limits<double>::quiet_NaN();
 
    patchwork::MeanNormal cand_mn = mean_normals.at(candidate.sector_idx);
 
    if (cand_mn.save)
    {
        int left_idx = candidate.sector_idx-1;
        int right_idx = candidate.sector_idx+1;

        if ( left_idx == -1 ) left_idx = mean_normals.size()-1;
        if ( right_idx == mean_normals.size() ) right_idx = 0;

        // ortho = normal*vec_pc_mean_ = difference in the direction of normal
        patchwork::MeanNormal left_mn = mean_normals.at(left_idx);
        if (left_mn.save)
        {
            if (!left_mn.elevated)
            {
                left_ortho = 0.0;
                left_ortho += cand_mn.normal.x*(left_mn.mean.x-cand_mn.mean.x);
                left_ortho += cand_mn.normal.y*(left_mn.mean.y-cand_mn.mean.y);
                left_ortho += cand_mn.normal.z*(left_mn.mean.z-cand_mn.mean.z);
            }
        }

        patchwork::MeanNormal right_mn = mean_normals.at(right_idx);
        if (right_mn.save)
        {
            if (!right_mn.elevated)
            {   
                right_ortho = 0.0;
                right_ortho += cand_mn.normal.x*(right_mn.mean.x-cand_mn.mean.x);
                right_ortho += cand_mn.normal.y*(right_mn.mean.y-cand_mn.mean.y);
                right_ortho += cand_mn.normal.z*(right_mn.mean.z-cand_mn.mean.z);
            }   
        }

        if (!prev_mean_normals.empty())
        {
            patchwork::MeanNormal front_mn = prev_mean_normals.at(candidate.sector_idx);
            if (front_mn.save)
            {
                if (!front_mn.elevated)
                {   
                    front_ortho = 0.0;
                    front_ortho += cand_mn.normal.x*(front_mn.mean.x-cand_mn.mean.x);
                    front_ortho += cand_mn.normal.y*(front_mn.mean.y-cand_mn.mean.y);
                    front_ortho += cand_mn.normal.z*(front_mn.mean.z-cand_mn.mean.z);
                }   
            }
        }
        else
        {
            front_ortho = 0.0;
            front_ortho += cand_mn.normal.x*(-cand_mn.mean.x);
            front_ortho += cand_mn.normal.y*(-cand_mn.mean.y);
            front_ortho += cand_mn.normal.z*(-1.723-cand_mn.mean.z);
        }       
    }

    if (!isnan(left_ortho)) { if ((abs(left_ortho)-mean_ortho)/stdev_ortho < (double) 2.0) revert_elevate = true; }
    if (!isnan(right_ortho)) { if ((abs(right_ortho)-mean_ortho)/stdev_ortho < (double) 2.0) revert_elevate = true; }
    //if (!isnan(front_ortho)) { if (front_ortho > -0.3) revert_elevate = true; }
    // if (isnan(left_ortho) && isnan(right_ortho) && isnan(front_ortho)) revert_elevate = false;
    
    if (verbose_) 
    { 
        cout << "cand sector_idx: " << candidate.sector_idx
             << ", revert_elevate: " << revert_elevate
             << ", left_ortho: " << left_ortho 
             << ", right_ortho: " << right_ortho 
             << ", front_ortho: " << front_ortho << endl; 
    }
}

template<typename PointT> inline
void PatchWork<PointT>::revert_candidates(pcl::PointCloud<PointT> &cloud_out, pcl::PointCloud<PointT> &cloud_nonground,
                                          std::vector<std::tuple<int, double, double, pcl::PointCloud<PointT>>> candidates,
                                          const double mean_flatness, const double stdev_flatness,
                                          std::vector<std::tuple<int, double, double>> orthogonality_of_sectors,
                                          bool check_adjflat, bool check_avgflat, bool check_ortho)
{
    std::vector<int> reverted_sector_idx;
    for( size_t i=0; i<candidates.size(); i++ )
    {
        //std::tuple<int, double, double, pcl::PointCloud<PointT>> tuple_i = flat_sector_candidates.at(i);
        std::tuple<int, double, double, pcl::PointCloud<PointT>> tuple_i = candidates.at(i);
        
        int num_veg = 0;
        for (auto const& pt: std::get<3>(tuple_i).points) { if (pt.label == 70) num_veg++; } // 70 for vegetation label

        int TP = count_num_ground_without_vegetation(std::get<3>(tuple_i));
        int FP = std::get<3>(tuple_i).size() - num_veg - TP;
        
        auto it = find(reverted_sector_idx.begin(), reverted_sector_idx.end(), std::get<0>(tuple_i));
        if (it != reverted_sector_idx.end()) continue;
        
        cout << std::get<0>(tuple_i) 
            << "th flat_sector_candidate's flatness: " << std::get<1>(tuple_i)
            // << " / elevation height : " << std::get<2>(tuple_i)
            << endl;

        if ( std::get<1>(tuple_i) < mean_flatness || (fabs(std::get<1>(tuple_i)-mean_flatness)/stdev_flatness) < (double) 1.5)
            //&& std::get<2>(tuple_i) < 10.0 ) // (fabs(std::get<2>(tuple_i)-mean_ground_h)/stdev_ground_h) < (double) 1.5 )
        {
            if (std::get<2>(tuple_i) < 8.0)
            {
                cout << "REVERT TRUE" 
                    << "\n/ flatness : " << std::get<1>(tuple_i) << ", mean_flatness : " << mean_flatness 
                    << "\n/ straightness: " << std::get<2>(tuple_i) << endl;
                    //<< " / ground_h : " << std::get<2>(tuple_i) << ", mean_ground_h : " << mean_ground_h << endl;
                revert_pc += std::get<3>(tuple_i);
                cloud_out += std::get<3>(tuple_i);
                reverted_sector_idx.push_back(std::get<0>(tuple_i));

                cout << "After / Catch True Positive: " << TP;
                cout << " , Increase False Positive: " << FP << endl;

                reverted_TP_.push_back(TP);
                reverted_FP_.push_back(FP);
            }
            else
            {
                cout << "Revert FALSE, straigthness: " << std::get<2>(tuple_i) << endl;
                reject_pc += std::get<3>(tuple_i);
                cloud_nonground += std::get<3>(tuple_i);
            }
        }
        else
        {
            cout << "FINAL REJECT" << endl;
            reject_pc += std::get<3>(tuple_i);
            cloud_nonground += std::get<3>(tuple_i);
        }
    }
}

template<typename PointT> inline
void PatchWork<PointT>::revert_candidates(pcl::PointCloud<PointT> &cloud_out, pcl::PointCloud<PointT> &cloud_nonground,
                                          std::vector<std::tuple<int, double, pcl::PointCloud<PointT>>> candidates,
                                          const double mean_flatness, const double stdev_flatness,
                                          std::vector<std::tuple<int, double, double>> orthogonality_of_sectors,
                                          bool check_adjflat, bool check_avgflat, bool check_ortho)
{
    double time_taken = 0;
    double start = ros::Time::now().toSec();
    
    cout << "Average flatness for ground sectors = " << mean_flatness << endl;
    cout << "Std Dev flatness for ground sectors = " << stdev_flatness << endl;

    // -------------------------------------------------------------------------------
    // Adjacent Sector Flatness Analysis
    // For clustered sectors(adjacent sectors), check their flatness variation(stdev)
    // -------------------------------------------------------------------------------
    std::vector<std::tuple<int, double, pcl::PointCloud<PointT>>> adjacent_flat_sector;
    std::vector<int> reverted_sector_idx;

    if (check_adjflat)
    {    
        for ( size_t i=0; i<candidates.size(); i++ )
        {
            std::tuple<int, double, pcl::PointCloud<PointT>> tuple_i = candidates.at(i);

            // Save adjacent sectors into adjacent_flat_sector
            bool end_of_adjacent_sector = false;
            if (adjacent_flat_sector.empty())
            {
                adjacent_flat_sector.push_back(tuple_i);
            }
            else if ( std::get<0>(adjacent_flat_sector.back()) == std::get<0>(tuple_i)-1 )
            {
                adjacent_flat_sector.push_back(tuple_i);
            }
            else end_of_adjacent_sector = true;

            if (i == candidates.size()-1) end_of_adjacent_sector = true;

            // If the sector is not continuous anymore, analyze the previous adjacent sectors
            if (end_of_adjacent_sector)
            {
                if (adjacent_flat_sector.size() > 1)
                {
                    cout << "Adjacent Sectors Check " << endl;
                            
                    double adj_mean_flatness = 0.0, adj_stdev_flatness = 0.0;
                    calc_mean_stdev(adjacent_flat_sector, adj_mean_flatness, adj_stdev_flatness);

                    // Filter outlier in adjacent sectors
                    std::vector<std::tuple<int, double, pcl::PointCloud<PointT>>> clustered_flat_sector;
                    for (int i=0; i<adjacent_flat_sector.size(); i++)
                    {
                        if ( fabs(std::get<1>(adjacent_flat_sector.at(i))-adj_mean_flatness)/adj_stdev_flatness < 1.0 )
                        {
                            cout << "inlier i: " << std::get<0>(adjacent_flat_sector.at(i))
                                    << " / flatness: " << std::get<1>(adjacent_flat_sector.at(i)) << endl;
                            clustered_flat_sector.push_back(adjacent_flat_sector.at(i));
                        }
                        else
                        {
                            cout << "outlier i: " << std::get<0>(adjacent_flat_sector.at(i))
                                    << " / flatness: " << std::get<1>(adjacent_flat_sector.at(i)) << endl;
                        }
                    }

                    // Find new mean, stdev
                    double new_adj_mean_flatness = 0.0, new_adj_stdev_flatness = 0.0;
                    calc_mean_stdev(clustered_flat_sector, new_adj_mean_flatness, new_adj_stdev_flatness);

                    cout << "adj_mean: " << new_adj_mean_flatness << endl;
                    cout << "adj_stdev: " << new_adj_stdev_flatness << endl;

                    if (new_adj_stdev_flatness/stdev_flatness < 2.5)
                    {
                        for (int i=0; i<adjacent_flat_sector.size(); i++)
                        {
                            cout << "REVERT TRUE - ADJACENT i: " << std::get<0>(adjacent_flat_sector.at(i)) << endl;
                            revert_pc += std::get<2>(adjacent_flat_sector.at(i));
                            cloud_out += std::get<2>(adjacent_flat_sector.at(i));
                            reverted_sector_idx.push_back(std::get<0>(adjacent_flat_sector.at(i)));
                        }
                    }
                }

                adjacent_flat_sector.clear();
                adjacent_flat_sector.push_back(tuple_i);
            }
        }
    }
        
    // -----------------------------------------------------------------------------------
    // Average Flatness Analysis
    // For candidates, check flatness compared to upright_enough grounds' average flatness
    // -----------------------------------------------------------------------------------
    if (check_avgflat)
    {   
        for( size_t i=0; i<candidates.size(); i++ )
        {
            //std::tuple<int, double, double, pcl::PointCloud<PointT>> tuple_i = flat_sector_candidates.at(i);
            std::tuple<int, double, pcl::PointCloud<PointT>> tuple_i = candidates.at(i);
                        
            auto it = find(reverted_sector_idx.begin(), reverted_sector_idx.end(), std::get<0>(tuple_i));
            if (it != reverted_sector_idx.end()) continue;
            
            cout << std::get<0>(tuple_i) 
                << "th flat_sector_candidate's flatness: " << std::get<1>(tuple_i)
                // << " / elevation height : " << std::get<2>(tuple_i)
                << endl;

            if ( std::get<1>(tuple_i) < mean_flatness || (fabs(std::get<1>(tuple_i)-mean_flatness)/stdev_flatness) < (double) 1.5)
                //&& std::get<2>(tuple_i) < elevation_thr_[concentric_idx] ) // (fabs(std::get<2>(tuple_i)-mean_ground_h)/stdev_ground_h) < (double) 1.5 )
            {
                cout << "REVERT TRUE" 
                        << " / flatness : " << std::get<1>(tuple_i) << ", mean_flatness : " << mean_flatness << endl;
                        //<< " / ground_h : " << std::get<2>(tuple_i) << ", mean_ground_h : " << mean_ground_h << endl;
                revert_pc += std::get<2>(tuple_i);
                cloud_out += std::get<2>(tuple_i);
                reverted_sector_idx.push_back(std::get<0>(tuple_i));
            }
            else
            {
                cout << "FINAL REJECT" << endl;
                reject_pc += std::get<2>(tuple_i);
                cloud_nonground += std::get<2>(tuple_i);
            }
        }
    }
    

    // -------------------------------------------------------------------
    // Orthogonality Analysis
    // Check orthogonality between corresponding sectors in curr-prev ring
    // -------------------------------------------------------------------
    
    if (check_ortho)
    {
        for( size_t i=0; i<candidates.size(); i++ )
        {
            std::tuple<int, double, pcl::PointCloud<PointT>> tuple_i = candidates.at(i);

            auto iterator = find(reverted_sector_idx.begin(), reverted_sector_idx.end(), std::get<0>(tuple_i));
            if (iterator != reverted_sector_idx.end()) continue;

            double orth_a = 0.0, orth_b = 0.0;

            std::vector<std::tuple<int, double, double>>::iterator it;
            for(it = orthogonality_of_sectors.begin(); it != orthogonality_of_sectors.end(); ++it)
            {
                if ( std::get<0>(*it) == std::get<0>(tuple_i) )
                {
                    orth_a = std::get<1>(*it);
                    orth_b = std::get<2>(*it);
                }
            }
            if (fabs(orth_a)>0.3 && fabs(orth_b)>0.3)
            {
                cout << "Normals and relative vector btw pc_means are not orthogonal" << endl;
                reject_pc += std::get<2>(tuple_i);
                cloud_nonground += std::get<2>(tuple_i);
            }
            else 
            {
                cout << "REVERT TRUE / orth_a : " << orth_a << ", orth_b : " << orth_b << endl;
                revert_pc += std::get<2>(tuple_i);
                cloud_out += std::get<2>(tuple_i);
            }
        }
    }
    
    if(!check_adjflat && !check_avgflat && !check_ortho)
    {
        for( size_t i=0; i<candidates.size(); i++ )
        {
            std::tuple<int, double, pcl::PointCloud<PointT>> tuple_i = candidates.at(i);

            reject_pc += std::get<2>(tuple_i);
            cloud_nonground += std::get<2>(tuple_i);
        }
    }


    double end = ros::Time::now().toSec();
    
    cout << "Taken time to revert candidates: " << end-start << endl;
}

template<typename PointT> inline
void PatchWork<PointT>::gen_orthogonality_of_sectors(std::vector<std::tuple<int, double, double>> &orthogonality_of_sectors,
                                                     std::vector<std::tuple<int, Eigen::Vector4f, Eigen::MatrixXf>> prev_ring_mean_normals,
                                                     int concentric_idx, int sector_idx)
{
    std::vector<std::tuple<int, Eigen::Vector4f, Eigen::MatrixXf>>::iterator it;
    for (it = prev_ring_mean_normals.begin(); it != prev_ring_mean_normals.end(); ++it)
    {
        if ( std::get<0>(*it) == (sector_idx/(2-fmod(concentric_idx,2))) )
        {   
            Eigen::Vector4f prev_ring_pc_mean = std::get<1>(*it);
            Eigen::MatrixXf prev_ring_normal = std::get<2>(*it);

            double orthogonality_a = 0, orthogonality_b = 0;

            Eigen::Vector4f vec = pc_mean_ - prev_ring_pc_mean; // relative pc mean vector
            
            // Normalization
            double vec_len = 0.0; 
            for(int i=0; i<3; i++) vec_len += vec(i,0)*vec(i,0);
            if (vec_len != 0.0) vec /= sqrt(vec_len);
            
            for (int i=0; i<3; i++) orthogonality_a += vec(i,0)*prev_ring_normal(i,0);
            for (int i=0; i<3; i++) orthogonality_b += vec(i,0)*normal_(i,0);
/*
            std::cout << "\033[1;34m[Orthogonality] Check [" 
                      << concentric_idx << ", " << sector_idx
                      << "]. orthogonality_a :"  << orthogonality_a 
                      << ", orthogonality_b :"  << orthogonality_b 
                      << "\033[0m" << std::endl;
*/
            std::tuple<int, double, double> t = std::make_tuple(sector_idx, orthogonality_a, orthogonality_b);
            orthogonality_of_sectors.push_back(t);
        }
    }
}

template<typename PointT> inline
void PatchWork<PointT>::calc_mean_stdev(std::vector<double> vec, double &mean, double &stdev)
{
    if (vec.size() <= 1) return;

    mean = std::accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

    for (int i=0; i<vec.size(); i++) { stdev += (vec.at(i)-mean)*(vec.at(i)-mean); }  
    stdev /= vec.size()-1;
    stdev = sqrt(stdev);
}

template<typename PointT> inline
void PatchWork<PointT>::calc_mean_stdev(std::vector<std::tuple<int, double, pcl::PointCloud<PointT>>> vec, double &mean, double &stdev)
{
    if (vec.size() <= 1) return;
    
    for (int i=0; i<vec.size(); i++) { mean += std::get<1>(vec.at(i)); }
    mean /= vec.size();

    for (int i=0; i<vec.size(); i++) { stdev += (std::get<1>(vec.at(i))-mean)*(std::get<1>(vec.at(i))-mean); }
    stdev /= vec.size()-1;
    stdev = sqrt(stdev);
}

template<typename PointT> inline
double PatchWork<PointT>::calc_principal_variance(const Eigen::Matrix3f &cov, const Eigen::Vector4f &centroid) {
    double angle = atan2(centroid(1, 0), centroid(0, 0)); // y, x
    double c = cos(angle);
    double s = sin(angle);
    double var_x_prime = c * c * cov(0, 0) + s * s * cov(1, 1) + 2 * c * s * cov(0, 1);
    double var_y_prime = s * s * cov(0, 0) + c * c * cov(1, 1) - 2 * c * s * cov(0, 1);
    return max(var_x_prime, var_y_prime);
}


template<typename PointT> inline
double PatchWork<PointT>::xy2theta(const double &x, const double &y) { // 0 ~ 2 * PI
    if (y >= 0) {
        return atan2(y, x); // 1, 2 quadrant
    } else {
        return 2 * M_PI + atan2(y, x);// 3, 4 quadrant
    }
}

template<typename PointT> inline
double PatchWork<PointT>::xy2radius(const double &x, const double &y) {
    return sqrt(pow(x, 2) + pow(y, 2));
}

template<typename PointT> inline
void PatchWork<PointT>::pc2czm(const pcl::PointCloud<PointT> &src, std::vector<Zone> &czm) {

    for (auto const &pt : src.points) {
        int ring_idx, sector_idx;
        double r = xy2radius(pt.x, pt.y);
        if ((r <= max_range_) && (r > min_range_)) {
            double theta = xy2theta(pt.x, pt.y);

            if (r < min_range_z2_) { // In First rings
                ring_idx = min(static_cast<int>(((r - min_range_) / ring_sizes[0])), num_rings_each_zone_[0] - 1);
                sector_idx = min(static_cast<int>((theta / sector_sizes[0])), num_sectors_each_zone_[0] - 1);
                czm[0][ring_idx][sector_idx].points.emplace_back(pt);
            } else if (r < min_range_z3_) {
                ring_idx = min(static_cast<int>(((r - min_range_z2_) / ring_sizes[1])), num_rings_each_zone_[1] - 1);
                sector_idx = min(static_cast<int>((theta / sector_sizes[1])), num_sectors_each_zone_[1] - 1);
                czm[1][ring_idx][sector_idx].points.emplace_back(pt);
            } else if (r < min_range_z4_) {
                ring_idx = min(static_cast<int>(((r - min_range_z3_) / ring_sizes[2])), num_rings_each_zone_[2] - 1);
                sector_idx = min(static_cast<int>((theta / sector_sizes[2])), num_sectors_each_zone_[2] - 1);
                czm[2][ring_idx][sector_idx].points.emplace_back(pt);
            } else { // Far!
                ring_idx = min(static_cast<int>(((r - min_range_z4_) / ring_sizes[3])), num_rings_each_zone_[3] - 1);
                sector_idx = min(static_cast<int>((theta / sector_sizes[3])), num_sectors_each_zone_[3] - 1);
                czm[3][ring_idx][sector_idx].points.emplace_back(pt);
            }
        }

    }
}

// For adaptive
template<typename PointT> inline
void PatchWork<PointT>::extract_piecewiseground(
        const int zone_idx, const pcl::PointCloud<PointT> &src,
        pcl::PointCloud<PointT> &dst,
        pcl::PointCloud<PointT> &non_ground_dst) {
    // 0. Initialization
    if (!ground_pc_.empty()) ground_pc_.clear();
    if (!dst.empty()) dst.clear();
    if (!non_ground_dst.empty()) non_ground_dst.clear();
    // 1. set seeds!
 
    // extract_initial_seeds_(zone_idx, src, ground_pc_);
    // estimate_plane_(ground_pc_);
    
    // 1-1. Remove vertical plane (R-VPF)
    pcl::PointCloud<PointT> src_wo_walls;
    src_wo_walls = src;

    for (int i = 0; i < num_iter_; i++) {

        if (!original_)
        {
            extract_initial_seeds_(zone_idx, src_wo_walls, ground_pc_, th_seeds_v_);
            estimate_plane_(ground_pc_, th_dist_v_);
        }
        else break;

        if (zone_idx == 0 && normal_(2) < uprightness_thr_-0.1)
        {
            // if (verbose_) cout << "\033[1;31m" << "Erase Walls!!! " << i << "\033[0m" << endl;
            pcl::PointCloud<PointT> src_tmp;
            src_tmp = src_wo_walls;
            src_wo_walls.clear();
            
            Eigen::MatrixXf points(src_tmp.points.size(), 3);
            int j = 0;
            for (auto &p:src_tmp.points) {
                points.row(j++) << p.x, p.y, p.z;
            }
            // ground plane model
            Eigen::VectorXf result = points * normal_;

            for (int r = 0; r < result.rows(); r++) {
                if (result[r] < th_dist_ - d_ && result[r] > -th_dist_ - d_) {
                    non_ground_dst.points.push_back(src_tmp[r]);
                    wall_pc_.points.push_back(src_tmp[r]);
                } else {
                    src_wo_walls.points.push_back(src_tmp[r]);
                }
            }
        }
        else break;
    }

    // Deprecated -- Reverse initial seeds selection for R-VPF
    // for (int i = 0; i < num_iter_; i++) {

    //     if (!original_)
    //     {
    //         extract_initial_seeds_reverse_(zone_idx, src_wo_walls, ground_pc_, th_seeds_v_);
    //         estimate_plane_(ground_pc_, th_dist_v_);
    //     }
    //     else break;

    //     if (normal_(2) < uprightness_thr_ && singular_values_(0) > 0.05)
    //     {
    //         // if (verbose_) cout << "\033[1;31m" << "Erase Walls!!! " << i << "\033[0m" << endl;
    //         pcl::PointCloud<PointT> src_tmp;
    //         src_tmp = src_wo_walls;
    //         src_wo_walls.clear();
            
    //         Eigen::MatrixXf points(src_tmp.points.size(), 3);
    //         int j = 0;
    //         for (auto &p:src_tmp.points) {
    //             points.row(j++) << p.x, p.y, p.z;
    //         }
    //         // ground plane model
    //         Eigen::VectorXf result = points * normal_;

    //         for (int r = 0; r < result.rows(); r++) {
    //             if (result[r] < th_dist_ - d_ && result[r] > -th_dist_ - d_) {
    //                 non_ground_dst.points.push_back(src_tmp[r]);
    //                 wall_pc_.points.push_back(src_tmp[r]);
    //             } else {
    //                 src_wo_walls.points.push_back(src_tmp[r]);
    //             }
    //         }
    //     }
    //     else break;
    // }

    extract_initial_seeds_(zone_idx, src_wo_walls, ground_pc_);
    estimate_plane_(ground_pc_);

    // 2. Extract ground (R-GPF)
    for (int i = 0; i < num_iter_; i++) {
        // estimate_plane_(ground_pc_);
        ground_pc_.clear();

        //pointcloud to matrix
        Eigen::MatrixXf points(src_wo_walls.points.size(), 3);
        int j = 0;
        for (auto &p:src_wo_walls.points) {
            points.row(j++) << p.x, p.y, p.z;
        }
        // ground plane model
        Eigen::VectorXf result = points * normal_;
        // threshold filter
        for (int r = 0; r < result.rows(); r++) {
            if (i < num_iter_ - 1) {
                if (result[r] < th_dist_d_ ) {// && result[r] > - th_dist_ - d_) {
                    ground_pc_.points.push_back(src_wo_walls[r]);
                }
            } else { // Final stage
                if (result[r] < th_dist_d_ ) {
                    dst.points.push_back(src_wo_walls[r]);
                } else {
                    if (i == num_iter_ - 1) {
                        non_ground_dst.points.push_back(src_wo_walls[r]);
                    }
                }
            }
        }

        if (i < num_iter_ -1) estimate_plane_(ground_pc_);
        else estimate_plane_(dst);
    }



    if ( !original_ && non_ground_dst.size() > 0 && non_ground_dst.points.size() < 1000) {
    
        double dx = non_ground_dst[non_ground_dst.size()-1].x - pc_mean_(0,0); 
        double dy = non_ground_dst[non_ground_dst.size()-1].y - pc_mean_(1,0); 
        double dz = non_ground_dst[non_ground_dst.size()-1].z - pc_mean_(2,0); 
        
        double max_res = dx*normal_(0,0) + dy*normal_(1,0) + dz*normal_(2,0);
        double th_mean_max_res_ = 0.4;

        // if (verbose_)
        // {
        //     cout << "max_res: " << max_res 
        //          << " max p : " << non_ground_dst[non_ground_dst.size()-1].x 
        //          << ", " << non_ground_dst[non_ground_dst.size()-1].y 
        //          << ", " << non_ground_dst[non_ground_dst.size()-1].z 
        //          << ", non_ground_dst.size(): " << non_ground_dst.size() << endl;
        // } 

        if (max_res < th_mean_max_res_) {
            dst += non_ground_dst;
            non_ground_dst.points.clear();
            estimate_plane_(dst);
        }
    }
}


template<typename PointT> inline
geometry_msgs::PolygonStamped PatchWork<PointT>::set_polygons(int r_idx, int theta_idx, int num_split) {
    geometry_msgs::PolygonStamped polygons;
    // Set point of polygon. Start from RL and ccw
    geometry_msgs::Point32 point;

    // RL
    double r_len = r_idx * ring_size + min_range_;
    double angle = theta_idx * sector_size;

    point.x = r_len * cos(angle);
    point.y = r_len * sin(angle);
    point.z = MARKER_Z_VALUE;
    polygons.polygon.points.push_back(point);
    // RU
    r_len = r_len + ring_size;
    point.x = r_len * cos(angle);
    point.y = r_len * sin(angle);
    point.z = MARKER_Z_VALUE;
    polygons.polygon.points.push_back(point);

    // RU -> LU
    for (int idx = 1; idx <= num_split; ++idx) {
        angle = angle + sector_size / num_split;
        point.x = r_len * cos(angle);
        point.y = r_len * sin(angle);
        point.z = MARKER_Z_VALUE;
        polygons.polygon.points.push_back(point);
    }

    r_len = r_len - ring_size;
    point.x = r_len * cos(angle);
    point.y = r_len * sin(angle);
    point.z = MARKER_Z_VALUE;
    polygons.polygon.points.push_back(point);

    for (int idx = 1; idx < num_split; ++idx) {
        angle = angle - sector_size / num_split;
        point.x = r_len * cos(angle);
        point.y = r_len * sin(angle);
        point.z = MARKER_Z_VALUE;
        polygons.polygon.points.push_back(point);
    }

    return polygons;
}

template<typename PointT> inline
geometry_msgs::PolygonStamped PatchWork<PointT>::set_polygons(int zone_idx, int r_idx, int theta_idx, int num_split) {
    geometry_msgs::PolygonStamped polygons;
    // Set point of polygon. Start from RL and ccw
    geometry_msgs::Point32 point;

    // RL
    double zone_min_range = min_ranges[zone_idx];
    double r_len = r_idx * ring_sizes[zone_idx] + zone_min_range;
    double angle = theta_idx * sector_sizes[zone_idx];

    point.x = r_len * cos(angle);
    point.y = r_len * sin(angle);
    point.z = MARKER_Z_VALUE;
    polygons.polygon.points.push_back(point);
    // RU
    r_len = r_len + ring_sizes[zone_idx];
    point.x = r_len * cos(angle);
    point.y = r_len * sin(angle);
    point.z = MARKER_Z_VALUE;
    polygons.polygon.points.push_back(point);

    // RU -> LU
    for (int idx = 1; idx <= num_split; ++idx) {
        angle = angle + sector_sizes[zone_idx] / num_split;
        point.x = r_len * cos(angle);
        point.y = r_len * sin(angle);
        point.z = MARKER_Z_VALUE;
        polygons.polygon.points.push_back(point);
    }

    r_len = r_len - ring_sizes[zone_idx];
    point.x = r_len * cos(angle);
    point.y = r_len * sin(angle);
    point.z = MARKER_Z_VALUE;
    polygons.polygon.points.push_back(point);

    for (int idx = 1; idx < num_split; ++idx) {
        angle = angle - sector_sizes[zone_idx] / num_split;
        point.x = r_len * cos(angle);
        point.y = r_len * sin(angle);
        point.z = MARKER_Z_VALUE;
        polygons.polygon.points.push_back(point);
    }

    return polygons;
}

template<typename PointT> inline
void PatchWork<PointT>::set_ground_likelihood_estimation_status(
        const int k, const int ring_idx,
        const int concentric_idx,
        const double z_vec,
        const double z_elevation,
        const double surface_variable) {
    if (z_vec > uprightness_thr_) { //orthogonal
        if (concentric_idx < num_rings_of_interest_) {
            if (z_elevation > elevation_thr_[concentric_idx]) {
                if (flatness_thr_[concentric_idx] > surface_variable) {
                    poly_list_.likelihood.push_back(FLAT_ENOUGH);
                } else {
                    poly_list_.likelihood.push_back(TOO_HIGH_ELEVATION);
                }
            } else {
                poly_list_.likelihood.push_back(UPRIGHT_ENOUGH);
            }
        } else {
            poly_list_.likelihood.push_back(UPRIGHT_ENOUGH);
        }
    } else { // tilted
        poly_list_.likelihood.push_back(TOO_TILTED);
    }
}

#endif
