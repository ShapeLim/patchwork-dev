//
// Created by shapelim on 6/23/21.
//

#ifndef PATCHWORK_PCD_LOADER_HPP
#define PATCHWORK_PCD_LOADER_HPP

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

class PcdLoader {
public:
    PcdLoader(const std::string &pcd_path) : pcd_path_(pcd_path){
        for (num_frames_ = 0;; num_frames_++) {
            std::string filename = (boost::format("%s/%06d.pcd") % pcd_path % num_frames_).str();
            if (!boost::filesystem::exists(filename)) {
                break;
            }
        }

        if (num_frames_ == 0) {
            std::cerr << "error: no files in " << pcd_path << std::endl;
        }
    }

    ~PcdLoader() {}

    size_t size() const { return num_frames_; }

    template<typename T>
    int get_cloud(size_t idx, pcl::PointCloud<T> &cloud) const {
        cloud.clear();
        std::string filename = (boost::format("%s/%06d.pcd") % pcd_path_ % idx).str();
        if (pcl::io::loadPCDFile<T> (filename, cloud) == -1) {
            return -1;
        }
        return 1;
    }

private:
    int num_frames_;
    std::string pcd_path_;
};
#endif //PATCHWORK_PCD_LOADER_HPP
