PATCHWORK_PATH=$(rospack find patchwork)
rosparam load $PATCHWORK_PATH/config/params.yaml

rosparam set /algorithm "GSAP"
rosparam set /mode "intercoupled"
rosparam set /data_path "/data/SemanticKITTI/sequences"

rosparam set /output_csvpath "/data/gsap_original/"
rosparam set /save_csv_file true
rosparam set /stop_for_each_frame false
rosparam set /init_idx 0
rosparam set /patchwork/verbose false
rosparam set /debug false
rosparam set /patchwork/matlab_analyze false

for seq in "'00'" "'01'" "'02'" "'03'" "'04'" "'05'" "'06'" "'07'" "'08'" "'09'" "'10'"
do
	rosparam set /sequence ${seq}
	sleep 2
	rosrun patchwork offline_kitti
done
