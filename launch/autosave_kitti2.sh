PATCHWORK_PATH=$(rospack find patchwork)
rosparam load $PATCHWORK_PATH/config/params.yaml

rosparam set /algorithm "GSAP"
rosparam set /mode "intercoupled"
rosparam set /gnc_on "no_gnc" # original, GNC
rosparam set /data_path "/data/SemanticKITTI/sequences"

rosparam set /save_csv_file false
rosparam set /stop_for_each_frame false
rosparam set /init_idx 0
for th_dist in 0.125
do
    rosparam set /gsap/th_dist ${th_dist}
    # rosparam set /output_csvpath "/data/patchwork_intercoupled_th_"${th_dist}"/"
    for seq in "'00'" "'01'" "'02'" "'03'" "'04'" "'05'" "'06'" "'07'" "'08'" "'09'" "'10'"
    do
	    rosparam set /sequence ${seq}
	    sleep 1
	    rosrun patchwork offline_kitti
    done
done



