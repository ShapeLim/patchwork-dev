PATCHWORK_PATH=$(rospack find patchwork)
rosparam load $PATCHWORK_PATH/config/params.yaml

rosparam set /algorithm "GSAP"
rosparam set /mode "intercoupled"
rosparam set /gnc_on "no_gnc" # original, GNC
rosparam set /data_path "/data/SemanticKITTI/sequences"


rosparam set /save_csv_file true
rosparam set /stop_for_each_frame false
rosparam set /init_idx 0

# for th_dist in 0.10 #0.2 0.3 0.4 0.5
for idx in 7 
do
    #for uprightness in 0.9397 # 0.7070 0.8660 0.9060 0.9397 0.9659
    #do
    rosparam set /gsap/zone1_idx ${idx}
    # rosparam set /output_csvpath "/data/to_paper_v2/zone1_idx_"${idx}"/"
    rosparam set /output_csvpath "/data/to_paper_v2/final_result/"
    for seq in "'00'" "'01'" "'02'" "'03'" "'04'" "'05'" "'06'" "'07'" "'08'" "'09'" "'10'"
    # for seq in "'05'" "'10'"
    do
        rosparam set /sequence ${seq}
        sleep 1
        rosrun patchwork offline_kitti
    done
    # done
done
