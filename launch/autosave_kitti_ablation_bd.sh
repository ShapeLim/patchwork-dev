PATCHWORK_PATH=$(rospack find patchwork)
rosparam load $PATCHWORK_PATH/config/params.yaml

rosparam set /algorithm "GSAP"
rosparam set /mode "intercoupled"
rosparam set /gnc_on "no_gnc" # original, GNC
rosparam set /data_path "/data/SemanticKITTI/sequences"


rosparam set /save_csv_file true
rosparam set /stop_for_each_frame false
rosparam set /init_idx 0

for closest_ring_upper_margin in 0.3 0.4 0.5 0.6 #0.2 0.3 0.4 0.5
do
    for init_seed_soft_upper_margin in 0.20 0.25 0.30 0.35 0.40 # 0.7070 0.8660 0.9060 0.9397 0.9659
    do
        rosparam set /gsap/init_seed_soft_upper_margin ${init_seed_soft_upper_margin}
        rosparam set /gsap/closest_ring_upper_margin ${closest_ring_upper_margin}
        rosparam set /output_csvpath "/data/ablation_boundary/gsap_ring1_um_"${closest_ring_upper_margin}"_um_"${init_seed_soft_upper_margin}"/"
        for seq in "'00'" "'01'" "'02'" "'03'" "'04'" "'05'" "'06'" "'07'" "'08'" "'09'" "'10'"
        # for seq in "'10'"
        do
	        rosparam set /sequence ${seq}
	        sleep 1
	        rosrun patchwork offline_kitti
        done
    done
done
