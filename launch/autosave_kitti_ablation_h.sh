PATCHWORK_PATH=$(rospack find patchwork)
rosparam load $PATCHWORK_PATH/config/params.yaml

rosparam set /algorithm "GSAP"
rosparam set /mode "intercoupled"
rosparam set /gnc_on "no_gnc" # original, GNC
rosparam set /data_path "/data/SemanticKITTI/sequences"


rosparam set /save_csv_file true
rosparam set /stop_for_each_frame false
rosparam set /init_idx 0

for rel_z_thr_independent in 0.6 0.7 0.8 0.9 1.0 #0.2 0.3 0.4 0.5
do
    for rel_z_thr_intercoupled in 0.6 0.7 0.8 0.9 1.0 1.1 1.2 # 0.7070 0.8660 0.9060 0.9397 0.9659
    do
        rosparam set /gsap/rel_z_thr_independent ${rel_z_thr_independent}
        rosparam set /gsap/rel_z_thr_intercoupled ${rel_z_thr_intercoupled}
        rosparam set /output_csvpath "/data/ablation_height/gsap_indep_"${rel_z_thr_independent}"_interc_"${rel_z_thr_intercoupled}"/"
        for seq in "'00'" "'01'" "'02'" "'03'" "'04'" "'05'" "'06'" "'07'" "'08'" "'09'" "'10'"
        do
	        rosparam set /sequence ${seq}
	        sleep 1
	        rosrun patchwork offline_kitti
        done
    done
done
