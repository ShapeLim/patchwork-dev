//
// Created by Hyungtae Lim on 6/23/21.
//

#include <iostream>
// For disable PCL complile lib, to use PointXYZIR
#define PCL_NO_PRECOMPILE

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <patchwork/node.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/centroid.h>
#include "patchwork/patchwork.hpp"
#include "patchwork/generalized_patchwork.hpp"
#include <visualization_msgs/Marker.h>
#include "tools/kitti_loader.hpp"
#include <signal.h>
#include <opencv2/opencv.hpp>

#include <iostream>
#include <fstream>
#include <yaml-cpp/yaml.h>

using PointType = PointXYZILID;
using namespace std;

// YAML::Node doc_;
ros::Publisher CloudPublisher;
ros::Publisher PreprocessedGroundPublisher;
ros::Publisher PreprocessedNonGroundPublisher;
ros::Publisher TraversableGroundPublisher;
ros::Publisher TraversableNonGroundPublisher;

std::string output_csvpath;

std::string acc_filename;
std::string pcd_savepath;
std::string data_path;
string      algorithm;
string      seq;
bool        save_flag;

int         init_idx;
bool        save_csv_file;

void signal_callback_handler(int signum) {
    cout << "Caught Ctrl + c " << endl;
    // Terminate program
    exit(signum);
}

template<typename T>
pcl::PointCloud<T> cloudmsg2cloud(sensor_msgs::PointCloud2 cloudmsg) {
    pcl::PointCloud<T> cloudresult;
    pcl::fromROSMsg(cloudmsg, cloudresult);
    return cloudresult;
}

template<typename T>
sensor_msgs::PointCloud2 cloud2msg(pcl::PointCloud<T> cloud, std::string frame_id = "map") {
    sensor_msgs::PointCloud2 cloud_ROS;
    pcl::toROSMsg(cloud, cloud_ROS);
    cloud_ROS.header.frame_id = frame_id;
    return cloud_ROS;
}

void preprocessed_ground(const pcl::PointCloud<PointXYZILID>& src, pcl::PointCloud<PointXYZILID>& ground, pcl::PointCloud<PointXYZILID>& non_ground){
  ground.clear();
  non_ground.clear();
  std::vector<int>::iterator iter;
  for (auto const& pt: src.points){
    if (pt.label == UNLABELED || pt.label == OUTLIER) continue;
    iter = std::find(ground_classes.begin(), ground_classes.end(), pt.label);
    if (iter != ground_classes.end()){ // corresponding class is in ground classes
      if (pt.label != VEGETATION) ground.push_back(pt);
      else non_ground.push_back(pt);
    }else{
      non_ground.push_back(pt);
    }
  }
}

void traversable_ground(const pcl::PointCloud<PointXYZILID>& src, pcl::PointCloud<PointXYZILID>& ground, pcl::PointCloud<PointXYZILID>& non_ground){
  ground.clear();
  non_ground.clear();
  std::vector<int>::iterator iter;
  for (auto const& pt: src.points){
    if (pt.label == UNLABELED || pt.label == OUTLIER) continue;
    iter = std::find(traversable_ground_classes.begin(), traversable_ground_classes.end(), pt.label);
    if (iter != traversable_ground_classes.end()){ // corresponding class is in ground classes
      if (pt.label != VEGETATION) ground.push_back(pt);
    }else{
      non_ground.push_back(pt);
    }
  }
}

void color_by_label(const pcl::PointCloud<PointXYZILID>& src, pcl::PointCloud<pcl::PointXYZRGB> &colored_pc)
{
  ros::NodeHandle p_nh;

  for (auto const& pt: src.points){

    pcl::PointXYZRGB point;
    point.x = pt.x;
    point.y = pt.y;
    point.z = pt.z;    

    if (pt.label == UNLABELED || pt.label == OUTLIER) continue;

    try {

      char label[4];
      sprintf(label, "%u", pt.label);
      std::string param_name = "/color_map/l" + static_cast<string>(label);
      
      std::vector<int> bgr;
      if (p_nh.getParam(param_name, bgr))
      {
        point.b = bgr.at(0);
        point.g = bgr.at(1);
        point.r = bgr.at(2);
        // cout << "param_name:" << param_name << ", size: " << bgr.size() << endl;
      }
      else
      {
        cout << "param_name:" << param_name << endl;
      }

      colored_pc.push_back(point);

    } catch (YAML::Exception &e) {
      std::cerr << "YAML Exception: " << e.what() << std::endl;
    }
  }
}

int main(int argc, char**argv) {
    ros::init(argc, argv, "KITTI_visualization");

    ros::NodeHandle nh;

    nh.param<string>("/algorithm", algorithm, "patchwork");
    nh.param<string>("/sequence", seq, "00");
    nh.param<string>("/data_path", data_path, "/");
    nh.param<string>("/output_csvpath", output_csvpath, "/data/");

    nh.param<int>("/init_idx", init_idx, 0);
    nh.param<bool>("/save_csv_file", save_csv_file, false);
    
    CloudPublisher     = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/cloud", 100, true);
    PreprocessedGroundPublisher    = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/preprocessedGround", 100, true);
    PreprocessedNonGroundPublisher = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/preprocessedNonGround", 100, true);
    TraversableGroundPublisher    = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/traversableGround", 100, true);
    TraversableNonGroundPublisher = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/traversableNonGround", 100, true);

    signal(SIGINT, signal_callback_handler);


    boost::shared_ptr<PatchWork<PointType> > PatchworkGroundSeg;
    PatchworkGroundSeg.reset(new PatchWork<PointXYZILID>(&nh));

    data_path = data_path + "/" + seq;
    KittiLoader loader(data_path);

    // std::ifstream file("~/catkin_ws/src/patchwork/config/KITTI_visualization.yaml");
    // doc_ = YAML::Load(file);
    
    int      N = loader.size();
    for (int n = init_idx; n < N; ++n) {     

        cout << n << "th node come" << endl;
        pcl::PointCloud<PointType> pc_curr;
        loader.get_cloud(n, pc_curr);

        pcl::PointCloud<PointType> pc_ground;
        pcl::PointCloud<PointType> pc_non_ground;

        static double time_taken;
        PatchworkGroundSeg->estimate_ground(pc_curr, pc_ground, pc_non_ground, time_taken);

        // Publish msg
        pcl::PointCloud<pcl::PointXYZRGB> colored_pc;
        pcl::PointCloud<PointType> traversableGround;
        pcl::PointCloud<PointType> traversableNonGround;
        pcl::PointCloud<PointType> preprocessedGround;
        pcl::PointCloud<PointType> preprocessedNonGround;
        color_by_label(pc_curr, colored_pc);
        preprocessed_ground(pc_curr, preprocessedGround, preprocessedNonGround);
        traversable_ground(pc_curr, traversableGround, traversableNonGround);

        CloudPublisher.publish(cloud2msg(colored_pc));
        PreprocessedGroundPublisher.publish(cloud2msg(preprocessedGround));
        PreprocessedNonGroundPublisher.publish(cloud2msg(preprocessedNonGround));
        TraversableGroundPublisher.publish(cloud2msg(traversableGround));
        TraversableNonGroundPublisher.publish(cloud2msg(traversableNonGround));
        ros::spinOnce();

        if(init_idx != 0) cin.ignore();
    }

    return 0;
}
