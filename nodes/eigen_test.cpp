//
// Created by shapelim on 21. 10. 27..
//

#include <Eigen/Core>
#include <iostream>
#include <omp.h>
#include <vector>
#include <chrono>
#include <random>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <limits>
using namespace std;


int main(){
    int n = 10;
    Eigen::Matrix<float, 3, Eigen::Dynamic> test, tt;

    std::random_device                     rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937                           gen(rd());
    std::uniform_real_distribution<float> unif(-100.0, 100.0);
    std::default_random_engine             re;

    // Set dummy pcl pointcloud
    pcl::PointCloud<pcl::PointXYZ> cloud;
    cloud.reserve(n);
    for (int i = 0; i < n; ++i) {
        pcl::PointXYZ p(unif(re), unif(re), unif(re));
        cloud.points.emplace_back(p);
    }

    test.resize(n, 3);
    for (int j = 0; j < n; ++j) {
        test.row(j) << cloud.points[j].x, cloud.points[j].y, cloud.points[j].z;
    }
//    Eigen::Matrix<float, 1, 3> vec_mean;
//    vec_mean << 1.0, 2.0, 3.0;
//    cout <<test <<endl;
//    cout << " ------- " << endl;
//    Eigen::Vector3f vv(1.0, 2.0, 3.0);
//    tt = test.rowwise() - vec_mean;
//    cout<< tt << endl;

    cout << std::numeric_limits<float>::max() << endl;
    cout << std::numeric_limits<float>::min() << endl;
    cout << -std::numeric_limits<float>::max() << endl;
    cout << std::numeric_limits<float>::lowest() << endl;

}