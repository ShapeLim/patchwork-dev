//
// Created by shapelim on 21. 10. 23..
//

#include <patchwork/GSAP.hpp>
#include <pcl/common/vector_average.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <string>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/transforms.h>
#include <chrono>
#include <math.h>
template <typename T>
int load_pcd(std::string pcd_name, boost::shared_ptr<pcl::PointCloud<T> > dst) {
    if (pcl::io::loadPCDFile<T>(pcd_name, *dst) == -1) {
        PCL_ERROR ("Couldn't read file!!! \n");
        return (-1);
    }
//        std::cout << "Loaded " << dst->size() << " data points from " << pcd_name << std::endl;
    return 0;
}

void voxelize(pcl::PointCloud<pcl::PointXYZ>::Ptr pc_src, pcl::PointCloud<pcl::PointXYZ>& pc_dst, double var_voxel_size){

    static pcl::VoxelGrid<pcl::PointXYZ> voxel_filter;
    voxel_filter.setInputCloud(pc_src);
    voxel_filter.setLeafSize(var_voxel_size, var_voxel_size, var_voxel_size);
    voxel_filter.filter(pc_dst);
}

int main(int argc, char **argv){
    ros::init(argc, argv, "map_stack");
    ros::NodeHandle nh;


    pcl::PointCloud<pcl::PointXYZ>::Ptr map;
    map.reset(new pcl::PointCloud<pcl::PointXYZ>());
    map->points.reserve(10000000);

//    pcl::PointCloud<pcl::PointXYZ>::Ptr src;
//    src.reset(new pcl::PointCloud<pcl::PointXYZ>());
//    pcl::PointXYZ pt_update;
//    for (int i = 20000; i < 25310; i=i+2) {
//        cout<< i << "th stacking..."<<endl;
//        string fname = "/home/shapelim/Loam_livox_loop/aft_mapp_" + to_string(i) + ".pcd";
//        int failure_flag = load_pcd(fname, src);
//        for (const auto& pt: (*src).points) {
//            pt_update.x = pt.x - 15.0;
//            pt_update.y = pt.y - 290.0;
//            pt_update.z = pt.z - 17.0;
//            map->points.emplace_back(pt_update);
//        }
//        src->points.clear();
//    }
//
//
//    pcl::PointCloud<pcl::PointXYZ> pc_voxelized;
//    cout<< "On voxelizing..." <<endl;
//    voxelize(map, pc_voxelized, 0.2);
//    cout<< "Done !" <<endl;


    pcl::PointCloud<pcl::PointXYZ>::Ptr pc_voxelized(new pcl::PointCloud<pcl::PointXYZ>);
    load_pcd("/home/shapelim/sundial.pcd", pc_voxelized);

    pcl::PointCloud<pcl::PointXYZ>::Ptr ptr_transformed(new pcl::PointCloud<pcl::PointXYZ>);
    Eigen::Matrix4f trans;
//    trans<< 1.414/2,   0,  -1.414/2, 0,
//            0,   1,  0, 0,
//            1.414/2,   0,  1.414/2, 0,
//            0,   0,  0, 1;
//    float angle = -15;
    float angle = -7.0;
    float cos_ = cos(angle * M_PI / 180.0);
    float sin_ = sin(angle * M_PI / 180.0);
    // For y-axie
//    trans<< cos_,   0,  sin_, 0,
//            0,   1,  0, 0,
//            -sin_,   0,  cos_, 0,
//            0,   0,  0, 1;
    trans<< 1, 0, 0, 0,
            0, cos_,  -sin_, 0,
            0,   sin_,  cos_, 0,
            0,   0,  0, 1;
    pcl::transformPointCloud(*pc_voxelized, *ptr_transformed, trans);

    pcl::io::savePCDFileASCII("/home/shapelim/sundial_x.pcd", *ptr_transformed);

    sensor_msgs::PointCloud2 cloud_ROS;
    pcl::toROSMsg(*ptr_transformed, cloud_ROS);
    cloud_ROS.header.frame_id = "/map";

    ros::Rate  loop_rate(1);
    ros::Publisher pub_map = nh.advertise<sensor_msgs::PointCloud2>("/MapUpdater/map_init", 100);
    pub_map.publish(cloud_ROS);
    int count = 0;
    while (ros::ok()) {
        if (++count % 5 == 0) {
            std::cout << "On " << count << "th publish!" << std::endl;
            pub_map.publish(cloud_ROS);
        }
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}