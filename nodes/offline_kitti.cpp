//
// Created by Hyungtae Lim on 6/23/21.
//

#include <iostream>
// For disable PCL complile lib, to use PointXYZIR
#define PCL_NO_PRECOMPILE

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <patchwork/node.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/centroid.h>
#include "patchwork/patchwork.hpp"
#include "patchwork/GSAP.hpp"
#include <visualization_msgs/Marker.h>
#include "tools/kitti_loader.hpp"
#include <signal.h>
#include <opencv2/opencv.hpp>

using PointType = PointXYZILID;
using namespace std;

ros::Publisher NonGroundPublisher;
ros::Publisher CloudPublisher;
ros::Publisher TPPublisher;
ros::Publisher FPPublisher;
ros::Publisher FNPublisher;
ros::Publisher PrecisionPublisher;
ros::Publisher RecallPublisher;

boost::shared_ptr<PatchWork<PointType> > PatchworkGroundSeg;
boost::shared_ptr<GSAP<PointType> > GSAP_PP;

std::string output_csvpath;

std::string acc_filename;
std::string pcd_savepath;
std::string data_path;
string      algorithm;
string      seq;
bool        save_flag;

int         init_idx;
bool        save_csv_file;
bool        stop_for_each_frame;


void signal_callback_handler(int signum) {
    cout << "Caught Ctrl + c " << endl;
    // Terminate program
    exit(signum);
}

void pub_score(std::string mode, double measure) {
    static int                 SCALE = 5;
    visualization_msgs::Marker marker;
    marker.header.frame_id                  = "map";
    marker.header.stamp                     = ros::Time();
    marker.ns                               = "my_namespace";
    marker.id                               = 0;
    marker.type                             = visualization_msgs::Marker::TEXT_VIEW_FACING;
    marker.action                           = visualization_msgs::Marker::ADD;
    if (mode == "p") marker.pose.position.x = 28.5;
    if (mode == "r") marker.pose.position.x = 25;
    marker.pose.position.y                  = 30;
    marker.pose.position.z    = 1;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x            = SCALE;
    marker.scale.y            = SCALE;
    marker.scale.z            = SCALE;
    marker.color.a            = 1.0; // Don't forget to set the alpha!
    marker.color.r            = 0.0;
    marker.color.g            = 1.0;
    marker.color.b            = 0.0;
    //only if using a MESH_RESOURCE marker type:
    marker.text               = mode + ": " + std::to_string(measure);
    if (mode == "p") PrecisionPublisher.publish(marker);
    if (mode == "r") RecallPublisher.publish(marker);

}

template<typename T>
pcl::PointCloud<T> cloudmsg2cloud(sensor_msgs::PointCloud2 cloudmsg) {
    pcl::PointCloud<T> cloudresult;
    pcl::fromROSMsg(cloudmsg, cloudresult);
    return cloudresult;
}

template<typename T>
sensor_msgs::PointCloud2 cloud2msg(pcl::PointCloud<T> cloud, std::string frame_id = "map") {
    sensor_msgs::PointCloud2 cloud_ROS;
    pcl::toROSMsg(cloud, cloud_ROS);
    cloud_ROS.header.frame_id = frame_id;
    return cloud_ROS;
}

int main(int argc, char**argv) {
    ros::init(argc, argv, "Offline_KITTI");

    ros::NodeHandle nh;
    nh.param<string>("/algorithm", algorithm, "patchwork");
    nh.param<string>("/sequence", seq, "00");
    nh.param<string>("/data_path", data_path, "/");
    nh.param<string>("/output_csvpath", output_csvpath, "/data/");

    nh.param<int>("/init_idx", init_idx, 0);
    nh.param<bool>("/save_csv_file", save_csv_file, false);
    nh.param<bool>("/stop_for_each_frame", stop_for_each_frame, false);

    // if (!save_csv_file){
    //     throw invalid_argument("Something's wrong!");
    // }

    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    string HOME = std::getenv("HOME");
    data_path = HOME + data_path;
    output_csvpath = HOME + output_csvpath;

    std::ostringstream oss;
    oss << std::put_time(&tm, "%m%d_%H:%M:%S/");
    auto str = oss.str();

    //output_filename += str;

    NonGroundPublisher = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/nonground_for_viz", 100, true);
    CloudPublisher     = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/cloud", 100, true);
    TPPublisher        = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/TP", 100, true);
    FPPublisher        = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/FP", 100, true);
    FNPublisher        = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/FN", 100, true);
    PrecisionPublisher = nh.advertise<visualization_msgs::Marker>("/precision", 1, true);
    RecallPublisher    = nh.advertise<visualization_msgs::Marker>("/recall", 1, true);

    signal(SIGINT, signal_callback_handler);

    PatchworkGroundSeg.reset(new PatchWork<PointXYZILID>(&nh));
    GSAP_PP.reset(new GSAP<PointXYZILID>(&nh));
    data_path = data_path + "/" + seq;
    KittiLoader loader(data_path);

    int      N = loader.size();
    for (int n = init_idx; n < N; ++n) {     

        cout << n << "th node come" << endl;
        pcl::PointCloud<PointType> pc_curr;
        loader.get_cloud(n, pc_curr);
        pcl::PointCloud<PointType> pc_ground;
        pcl::PointCloud<PointType> pc_non_ground;
        pcl::PointCloud<PointType> pc_viz_non_ground;
        pc_ground.points.reserve(NUM_HEURISTIC_MAX_PTS_IN_CLOUD);
        pc_non_ground.points.reserve(NUM_HEURISTIC_MAX_PTS_IN_CLOUD);
        pc_viz_non_ground.points.reserve(NUM_HEURISTIC_MAX_PTS_IN_CLOUD);

        static double time_taken;
        cout << "Operating patchwork..." << endl;
        if (algorithm == "patchwork"){
            PatchworkGroundSeg->estimate_ground(pc_curr, pc_ground, pc_non_ground, time_taken);
        }else if (algorithm == "GSAP"){
            GSAP_PP->estimate_ground(pc_curr, pc_ground, pc_non_ground, pc_viz_non_ground, time_taken);
        }else{throw invalid_argument("Not implemented"); }


        // Estimation
        static double precision, recall, precision_wo_veg, recall_wo_veg;
        static vector<int> TPFNs; // TP, FP, FN, TN order
        static vector<int> TPFNs_wo_veg; // TP, FP, FN, TN order
        calculate_precision_recall(pc_curr, pc_ground, precision, recall, TPFNs);
        calculate_precision_recall_without_vegetation(pc_curr, pc_ground, precision_wo_veg, recall_wo_veg, TPFNs_wo_veg);
        //calculate_precision_recall(pc_curr, pc_ground, precision_naive, recall_naive, false);
        //calculate_precision_recall_without_vegetation(pc_curr, pc_ground, precision_naive, recall_naive, false);

        cout << "\033[1;32m" << n << "th. Takes : " << time_taken << " | " << pc_curr.size() << " -> "
             << pc_ground.size() << " (Reverted percentage: " << (pc_ground.size() + pc_non_ground.size()) / static_cast<float>(pc_curr.size()) * 100<<"%)"
             << "\033[0m" << endl;

        cout << "\033[1;32m [W/ Vegi.] P: " << precision << " | R: " << recall << "\033[0m" << endl;
        cout << "\033[1;32m [WO Vegi.] P: " << precision_wo_veg << " | R: " << recall_wo_veg << "\033[0m" << endl;


// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
//        If you want to save precision/recall in a text file, revise this part
// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        static string w_veg_path = output_csvpath + "w_veg_"+ seq + ".csv";
        static string wo_veg_path = output_csvpath + "wo_veg_"+ seq + ".csv";
        if (save_csv_file)
        {
            // Save w/ veg
            ofstream output(w_veg_path, ios::app);
            output << n << "," << time_taken << "," << precision << "," << recall << "," << TPFNs[0] << "," << TPFNs[1] << "," << TPFNs[2] << "," << TPFNs[3];
            output << std::endl;
            output.close();
            // Save w/o veg
            output.open(wo_veg_path, ios::app);
            output << n << "," << time_taken << "," << precision_wo_veg << "," << recall_wo_veg << "," << TPFNs_wo_veg[0] << "," << TPFNs_wo_veg[1] << "," << TPFNs_wo_veg[2] << "," << TPFNs_wo_veg[3];
            output << std::endl;
            output.close();

        }
        
// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -

        // Publish msg
        pcl::PointCloud<PointType> TP;
        pcl::PointCloud<PointType> FP;
        pcl::PointCloud<PointType> FN;
        pcl::PointCloud<PointType> TN;
        // discern_ground(pc_ground, TP, FP);
        discern_ground_without_vegetation(pc_ground, TP, FP);
        // discern_ground(pc_non_ground, FN, TN);
        discern_ground_without_vegetation(pc_non_ground, FN, TN);
// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
//        If you want to save the output of pcd, revise this part
// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
//        std::map<int, int> pc_curr_gt_counts, g_est_gt_counts;
//        double             accuracy;
//        save_all_accuracy(pc_curr, pc_ground, acc_filename, accuracy, pc_curr_gt_counts, g_est_gt_counts);
//        pcd_savepath                 = "/media/shapelim/UX960NVMe1/patchwork2_pcd/" + seq;
//        std::string count_str        = std::to_string(n);
//        std::string count_str_padded = std::string(NUM_ZEROS - count_str.length(), '0') + count_str;
//        std::string pcd_filename     = pcd_savepath + "/" + count_str_padded + ".pcd";
//        pc2pcdfile(TP, FP, FN, TN, pcd_filename);
// -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
        NonGroundPublisher.publish(cloud2msg(pc_viz_non_ground));
        CloudPublisher.publish(cloud2msg(pc_curr));
        TPPublisher.publish(cloud2msg(TP));
        FPPublisher.publish(cloud2msg(FP));
        FNPublisher.publish(cloud2msg(FN));
        pub_score("p", precision);
        pub_score("r", recall);
        ros::spinOnce();
        if (stop_for_each_frame) {
            cout<< "[Debug]: STOP!" <<endl;
            cin.ignore();
        }
    }

    // For analyze
    // int right[4], wrong[4];
    // for (int i=0; i<4; i++) 
    // { 
    //     PatchworkGroundSeg->get_right_wrong_bin_num(right[i], wrong[i], i);
    //     cout << i << ", right: " << right[i] << ", wrong: " << wrong[i] << ", ratio: " << (double)right[i]/(double)(right[i]+wrong[i]) << endl;
    //     ofstream output(output_csvpath + "right_wrong.csv", ios::app);
    //     output << right[i] << ", " << wrong[i] << ", " << (double)right[i]/(double)(right[i]+wrong[i]) << ", " << ground_num << endl;
    //     output.close();
    // }

    // std::vector<double> flatness[4];
    // std::vector<double> elevation[4];
    // std::vector<int> def_num[4];
    
    // std::vector<double> ground_flatness[4];
    // std::vector<double> ground_elevation[4];
    // std::vector<int> ground_num[4];

    // std::vector<double> nonground_flatness[4];
    // std::vector<double> nonground_elevation[4];
    // std::vector<int> nonground_num[4];

    // for (int i=0; i<4; i++)
    // {
    //     PatchworkGroundSeg->get_flatness(flatness[i], i);
    //     PatchworkGroundSeg->get_elevation(elevation[i], i);
    //     PatchworkGroundSeg->get_def_num(def_num[i], i);

    //     PatchworkGroundSeg->get_ground_flatness(ground_flatness[i], i);
    //     PatchworkGroundSeg->get_ground_elevation(ground_elevation[i], i);
    //     PatchworkGroundSeg->get_ground_num(ground_num[i], i);

    //     PatchworkGroundSeg->get_nonground_flatness(nonground_flatness[i], i);
    //     PatchworkGroundSeg->get_nonground_elevation(nonground_elevation[i], i);
    //     PatchworkGroundSeg->get_nonground_num(nonground_num[i], i);

    //     // ofstream output(output_csvpath + seq + "_definite_ring_" + std::to_string(i) + ".csv", ios::app);
    //     // for (int j=0; j<flatness[i].size(); j++) 
    //     // { 
    //     //     output << flatness[i].at(j) << ", " << elevation[i].at(j) << ", " << def_num[i].at(j) << endl;
    //     // }
    //     // output.close();
        
    //     ofstream output_g(output_csvpath + seq + "_ground_ring_" + std::to_string(i) + ".csv", ios::app);
    //     for (int j=0; j<ground_flatness[i].size(); j++) 
    //     {
    //         output_g << ground_flatness[i].at(j) << ", " << ground_elevation[i].at(j) << ", " << ground_num[i].at(j) << endl; 
    //     }
    //     output_g.close();

    //     ofstream output_n(output_csvpath + seq + "_nonground_ring_" + std::to_string(i) + ".csv", ios::app);
    //     for (int j=0; j<nonground_flatness[i].size(); j++) 
    //     { 
    //         output_n << nonground_flatness[i].at(j) << ", " << nonground_elevation[i].at(j) << ", " << nonground_num[i].at(j) << endl; 
    //     }
    //     output_n.close();
    // }
    
//    ros::spin();

    return 0;
}
