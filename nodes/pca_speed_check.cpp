//
// Created by shapelim on 21. 10. 23..
//

#include <patchwork/GSAP.hpp>
#include <pcl/common/vector_average.h>
#include <chrono>

float rand_float(float x_min, float x_max){
    float rand_ratio = rand()/(float)RAND_MAX;
    return (x_min + rand_ratio * (x_max - x_min));
}

boost::shared_ptr<GSAP<pcl::PointXYZ>> patchwork;
int main(int argc, char **argv){
    ros::init(argc, argv, "test_bench");
    ros::NodeHandle nh;

    pcl::VectorAverage<float, 3> test = pcl::VectorAverage<float, 3>();
    Eigen::Vector3f v1(1, 2, 3);
    Eigen::Vector3f v2(3, 2, 1);
    test.add(v1, 0.5);
    test.add(v2, 1.0);
    cout<<test.getNoOfSamples()<<endl;
    Eigen::Vector3f mean_ = test.getMean();

    Eigen::Matrix3f cov_ = test.getCovariance();
    cout<<test.getMean()<<endl;
    cout<<mean_<<endl;
    cout<<cov_<<endl;
    float aa = 10.0;
    Eigen::Matrix3f b;
    b<< 1, 2, 3,
        4, 5, 6,
        7, 8, 9;
    cout<< b * v1<<endl;
    auto c = (b * v1).array() + aa;
    cout<< c <<endl;
    cout<< c(0, 0)<<endl;
    cout<< c(1, 0)<<endl;
    cout<< c(2, 0)<<endl;
    cout<< c.rows()<<endl;
    cout<<c[0]<<endl;
    cout<<c[1]<<endl;
    cout<<c[2]<<endl;


    pcl::PointCloud<pcl::PointXYZ> cloud;

    pcl::PointCloud<pcl::PointXYZ> in;
    pcl::PointCloud<pcl::PointXYZ> out;
    double total_svd = 0;
    int M = 1000;
    for (int trial = 0 ; trial < M; trial++) {
        int N = 2500;
        cloud.reserve(N);
        pcl::PointXYZ pt;
        for (int i          = 0; i < N; ++i) {
            pt.x = rand_float(-1.0, 1.0);
            pt.y = rand_float(-1.0, 1.0);
            pt.z = rand_float(-5.0, 5.0);
            cloud.points.emplace_back(pt);
        }

        auto t1 = chrono::high_resolution_clock::now();
        Eigen::Vector4f pc_mean;
        pcl::computeMeanAndCovarianceMatrix(cloud, cov_, pc_mean);
        Eigen::JacobiSVD<Eigen::MatrixXf> svd(cov_, Eigen::DecompositionOptions::ComputeFullU);
        auto t2 = chrono::high_resolution_clock::now();

        auto t_ms   = chrono::duration_cast<chrono::microseconds>(t2 - t1).count() / 1e3;
        total_svd += t_ms;
        cloud.clear();
    }
    cout<< "Final: " << total_svd / M  << " ms" <<endl;

    return 0;
}