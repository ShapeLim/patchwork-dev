//
// Created by shapelim on 21. 10. 23..
//

#include <patchwork/GSAP.hpp>
#include <pcl/common/vector_average.h>
#include <chrono>

float rand_float(float x_min, float x_max){
    float rand_ratio = rand()/(float)RAND_MAX;
    return (x_min + rand_ratio * (x_max - x_min));
}

float ABS_SCALE = 100.0;
int main(int argc, char **argv){

    pcl::PointCloud<pcl::PointXYZ> pc_resize;
    pcl::PointCloud<pcl::PointXYZ> pc_push_back;
    pcl::PointXYZ pt;
    int M = 1000;


    vector<int> ns = {100, 1000, 3000, 5000, 10000};
    for (auto& N: ns) {
        double total_t1 = 0;
        double total_t2 = 0;
        for (int trial = 0 ; trial < M; trial++) {

            auto t1 = chrono::high_resolution_clock::now();
            pc_resize.resize(N);
            auto t2 = chrono::high_resolution_clock::now();
            pc_push_back.reserve(N);
            pc_push_back.clear();
            auto t3 = chrono::high_resolution_clock::now();

            auto t_resize = chrono::duration_cast<chrono::microseconds>(t2 - t1).count() / 1e3;
            auto t_reserve = chrono::duration_cast<chrono::microseconds>(t3 - t2).count() / 1e3;
            total_t1 += t_resize;
            total_t2 += t_reserve;

            for (int i = 0; i < N; ++i) {
                pt.x = rand_float(-ABS_SCALE, ABS_SCALE);
                pt.y = rand_float(-ABS_SCALE, ABS_SCALE);
                pt.z = rand_float(-ABS_SCALE, ABS_SCALE);

                auto t1 = chrono::high_resolution_clock::now();
                pc_resize[i].x = pt.x;
                pc_resize[i].y = pt.y;
                pc_resize[i].z = pt.z;
                auto t2 = chrono::high_resolution_clock::now();

                pc_push_back.points.emplace_back(pt);
                auto t3 = chrono::high_resolution_clock::now();

                auto t_resize = chrono::duration_cast<chrono::microseconds>(t2 - t1).count() / 1e3;
                auto t_reserve = chrono::duration_cast<chrono::microseconds>(t3 - t2).count() / 1e3;
                total_t1 += t_resize;
                total_t2 += t_reserve;
            }
        }
        cout << N << endl;
        cout<< "Total: " << total_t1 << " vs " << total_t2 << endl;
        cout<< "Mean : " << total_t1 / M<< " vs " << total_t2 / M << endl;
    }

    return 0;
}