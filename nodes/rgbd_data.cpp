//
// Created by Hyungtae Lim on 6/23/21.
//

#include <iostream>
// For disable PCL complile lib, to use PointXYZIR
#define PCL_NO_PRECOMPILE
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <patchwork/node.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/centroid.h>
#include "patchwork/GSAP.hpp"
#include "tools/pcd_loader.hpp"
#include <signal.h>

using namespace std;

using PointType = pcl::PointXYZRGB;
using namespace std;

boost::shared_ptr<GSAP<PointType> > GSAP_PP;

std::string output_filename;
std::string acc_filename, pcd_savepath;
string      algorithm;
string      data_path;
string      mode;
string      seq;
bool        save_flag;

template<typename T>
pcl::PointCloud<T> cloudmsg2cloud(sensor_msgs::PointCloud2 cloudmsg)
{
    pcl::PointCloud<T> cloudresult;
    pcl::fromROSMsg(cloudmsg,cloudresult);
    return cloudresult;
}

template<typename T>
sensor_msgs::PointCloud2 cloud2msg(pcl::PointCloud<T> cloud, std::string frame_id = "map")
{
    sensor_msgs::PointCloud2 cloud_ROS;
    pcl::toROSMsg(cloud, cloud_ROS);
    cloud_ROS.header.frame_id = frame_id;
    return cloud_ROS;
}

void signal_callback_handler(int signum) {
    cout << "Caught Ctrl + c " << endl;
    // Terminate program
    exit(signum);
}

void parse_obj(const pcl::PointCloud<pcl::PointXYZRGB>& src,
               pcl::PointCloud<pcl::PointXYZRGB>& dst, vector<double> boundary) {
    dst.clear();
    for (const auto &pt: src.points) {
        bool is_in_x = ((pt.x > boundary[0]) && (pt.x < boundary[3]));
        bool is_in_y = ((pt.y > boundary[1]) && (pt.y < boundary[4]));
        bool is_in_z = ((pt.z > boundary[2]) && (pt.z < boundary[5]));
        if (is_in_x && is_in_y && is_in_z) {
            dst.points.emplace_back(pt);
        }
    }
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "Benchmark");
    ros::NodeHandle nh;
    nh.param<string>("/algorithm", algorithm, "GSAP");
    nh.param<string>("/data_path", data_path, "/");

    bool stop_for_each_frame;
    int init_idx = 0;
    nh.param<int>("/init_idx", init_idx, 0);
    nh.param<bool>("/stop_for_each_frame", stop_for_each_frame, false);

    ros::Publisher CloudPublisher  = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/cloud", 100);
    ros::Publisher PositivePublisher = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/positive", 100);
    ros::Publisher NegativePublisher = nh.advertise<sensor_msgs::PointCloud2>("/benchmark/negative", 100);
    ros::Publisher ObjPublisher = nh.advertise<sensor_msgs::PointCloud2>("/cluster_for_viz", 100);

    ros::Publisher HumanPublisher = nh.advertise<sensor_msgs::PointCloud2>("/cluster_for_viz/human", 100);
    ros::Publisher ExtinguisherPublisher = nh.advertise<sensor_msgs::PointCloud2>("/cluster_for_viz/extinguisher", 100);
    // For clustering
    std::vector<double> obj_f15, obj_f109, obj_f142, obj_f143_1, obj_f143_2;
    nh.getParam("/viz_for_paper/obj_frame15", obj_f15);
    nh.getParam("/viz_for_paper/obj_frame109", obj_f109);
    nh.getParam("/viz_for_paper/obj_frame142", obj_f142);
    nh.getParam("/viz_for_paper/obj_frame143_1", obj_f143_1);
    nh.getParam("/viz_for_paper/obj_frame143_2", obj_f143_2);

    GSAP_PP.reset(new GSAP<pcl::PointXYZRGB>(&nh));

    // Load dataset
    PcdLoader loader(data_path);

    int N = loader.size();
    for (int n = init_idx; n < N; ++n){
        // ToDo. Rviz error 잡아야 함!
        if ((56 < n && n < 69) || (87 < n && n < 94) || (125 < n && n < 136)) {
            cout << "Skip via prior knowledge" << endl;
            continue;
        }
        if (n > 193) {
            break;
        }
        signal(SIGINT, signal_callback_handler);
        cout << n << "th node come" << endl;
        pcl::PointCloud<PointType> pc_curr;
        pcl::PointCloud<PointType> pc_curr_target;
        pcl::PointCloud<PointType> pc_curr_roof;
        int status = loader.get_cloud(n, pc_curr);
        if ((status == -1) || (pc_curr.points.size() < 10000)) {
            continue;
        }
        cout<<"Data load complete!"<<endl;
        pcl::PointCloud<PointType> pc_ground;
        pcl::PointCloud<PointType> pc_non_ground;

        for (const auto &pt: pc_curr.points) {
            if (pt.z > 0.5) {
                pc_curr_roof.points.emplace_back(pt);
            }else {
                pc_curr_target.points.emplace_back(pt);
            }
        }

        if (pc_curr.points.size() == pc_curr_target.points.size()) {
            cout << " Too close to wall! 1" <<endl;
            continue;
        }

        static double time_taken;
        cout << "Operating patchwork..." << endl;
        cout << "\033[1;32mInput: \033[0m" << pc_curr_target.points.size() << " (" << pc_curr.points.size() << ")" << endl;
        GSAP_PP->estimate_ground(pc_curr_target, pc_ground, pc_non_ground, time_taken);
//
        cout << "\033[1;32mOutput: \033[0m"<< pc_ground.points.size() << ", " << pc_non_ground.points.size() << endl;
        cout << "Takes " <<time_taken << "s" << endl;

        if (pc_ground.points.empty()) {
            continue;
        }

        pc_non_ground += pc_curr_roof;

        // Publish data
        if ((!pc_curr.points.empty()) && (CloudPublisher.getNumSubscribers() != 0) ) {
            CloudPublisher.publish(cloud2msg(pc_curr));
        }
        if ((!pc_ground.points.empty()) && (PositivePublisher.getNumSubscribers() != 0) ) {
            PositivePublisher.publish(cloud2msg(pc_ground));
        }
        if ((!pc_non_ground.points.empty()) && (NegativePublisher.getNumSubscribers() != 0) ) {
            NegativePublisher.publish(cloud2msg(pc_non_ground));
        }

        pcl::PointCloud<pcl::PointXYZRGB> object;
        object.reserve(pc_non_ground.size());

        if (n == 15) {
            parse_obj(pc_non_ground, object, obj_f15);
            ObjPublisher.publish(cloud2msg(object));
        }

        if (n == 109) {
            parse_obj(pc_non_ground, object, obj_f109);
            ObjPublisher.publish(cloud2msg(object));
        }

        if (n == 143) {
            parse_obj(pc_non_ground, object, obj_f143_1);
            HumanPublisher.publish(cloud2msg(object));
            object.clear();
            parse_obj(pc_non_ground, object, obj_f143_2);
            ExtinguisherPublisher.publish(cloud2msg(object));
        }

        ros::spinOnce();
        if (stop_for_each_frame) {
            cout<< "[Debug]: STOP!" <<endl;
            cin.ignore();
        }
    }


    return 0;
}
