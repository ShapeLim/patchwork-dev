//
// Created by shapelim on 21. 10. 23..
//

#include <patchwork/GSAP.hpp>
#include <pcl/common/vector_average.h>
#include <chrono>
void const_test2(const int b){
    cout<<b<<endl;

}
void const_test(const int& a){
    const_test2(a);
}

float rand_float(float x_min, float x_max){
    float rand_ratio = rand()/(float)RAND_MAX;
    return (x_min + rand_ratio * (x_max - x_min));
}

boost::shared_ptr<GSAP<pcl::PointXYZ>> patchwork;
int main(int argc, char **argv){
    ros::init(argc, argv, "test_bench");
    ros::NodeHandle nh;

    pcl::VectorAverage<float, 3> test = pcl::VectorAverage<float, 3>();
    Eigen::Vector3f v1(1, 2, 3);
    Eigen::Vector3f v2(3, 2, 1);
    test.add(v1, 0.5);
    test.add(v2, 1.0);
    cout<<test.getNoOfSamples()<<endl;
    Eigen::Vector3f mean_ = test.getMean();

    Eigen::Matrix3f cov_ = test.getCovariance();
    cout<<test.getMean()<<endl;
    cout<<mean_<<endl;
    cout<<cov_<<endl;
    float aa = 10.0;
    Eigen::Matrix3f b;
    b<< 1, 2, 3,
        4, 5, 6,
        7, 8, 9;
    cout<< b * v1<<endl;
    auto c = (b * v1).array() + aa;
    cout<< c <<endl;
    cout<< c(0, 0)<<endl;
    cout<< c(1, 0)<<endl;
    cout<< c(2, 0)<<endl;
    cout<< c.rows()<<endl;
    cout<<c[0]<<endl;
    cout<<c[1]<<endl;
    cout<<c[2]<<endl;

    Eigen::Matrix<float, 1, Eigen::Dynamic> a = Eigen::Matrix<float, 1, Eigen::Dynamic>::Zero(1, 10);

    cout<< a <<endl;
    pcl::PointXYZ pt;
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::PointCloud<pcl::PointXYZ> cloud_sort;
    double total_sort = 0;
    double total_greedy = 0;
    int M = 50;

    pcl::PointCloud<pcl::PointXYZ> in;
    pcl::PointCloud<pcl::PointXYZ> out;
    for (int trial = 0 ; trial < M; trial++) {
        int N = 2500;
        cloud.reserve(N);
        cloud_sort.reserve(N);
        for (int i          = 0; i < N; ++i) {
            pt.x = rand_float(-1.0, 1.0);
            pt.y = rand_float(-1.0, 1.0);
            pt.z = rand_float(-5.0, 5.0);
            cloud.points.emplace_back(pt);
            cloud_sort.points.emplace_back(pt);
        }
        float    z_criteria = 0.0;

        auto t33 = chrono::high_resolution_clock::now();

        in.clear();
        out.clear();
        in.reserve(N);
        out.reserve(N);

        for (const auto &pt: cloud.points) {
            if (pt.z < z_criteria) {
                out.points.emplace_back(pt);
            } else {
                in.points.emplace_back(pt);
            }
        }
        auto t4 = chrono::high_resolution_clock::now();

        auto t1 = chrono::high_resolution_clock::now();
        sort(cloud_sort.points.begin(), cloud_sort.points.end(), z_cmp<pcl::PointXYZ>);
        auto t2 = chrono::high_resolution_clock::now();

        auto     it = cloud_sort.points.begin();
        for (int i  = 0; i < cloud_sort.points.size(); i++) {
            if (cloud_sort.points[i].z < z_criteria) {
                it++;
            } else {
                break;
            }
        }
        cloud_sort.points.erase(cloud_sort.points.begin(), it);
        auto t3 = chrono::high_resolution_clock::now();

        auto sort_pure_t_ms   = chrono::duration_cast<chrono::microseconds>(t2 - t1).count() / 1e3;
        auto sort_t_ms   = chrono::duration_cast<chrono::microseconds>(t3 - t1).count() / 1e3;
        auto greedy_t_ms = chrono::duration_cast<chrono::microseconds>(t4 - t33).count() / 1e3;
        cout<< sort_t_ms << "( " << sort_pure_t_ms << " )  vs " << greedy_t_ms <<endl;
        total_sort += sort_t_ms;
        total_greedy += greedy_t_ms;
        cloud_sort.clear();
        cloud.clear();
    }
    cout<< "Final: "<< endl;
    cout<<total_sort / M <<endl;
    cout<<total_greedy / M <<endl;

    return 0;
}