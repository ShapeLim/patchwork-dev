from tabulate import TableFormat, tabulate
import argparse

import numpy as np

if __name__ == "__main__":
<
	parser = argparse.ArgumentParser(description='Insert path')
	parser.add_argument('--path', required=True, help='path')
	parser.add_argument('--idx', required=False, default=11, help='idx')
	args = parser.parse_args()

	#abs_dir = "/home/seungjae/data/patchwork/analysis/adjflat_lastmrg/"
	abs_dir = "/home/seungjae/data/patchwork" + args.path
	# abs_dir = "/home/seungjae/patchwork_tro_matlab/data/" + args.path
	#abs_dir = "/home/seungjae/data/ransac" + args.path
	
	table = None

	total_f1_mean = 0
	total_p_mean = 0
	total_r_mean = 0
	sum_scene = 0
	total_t = 0
	for i in range(int(args.idx)):
		abs_path = abs_dir + "%02d"%i + ".csv"
		#print(abs_path)
		data = np.loadtxt(abs_path, delimiter=",", dtype=np.float32)[:, 2:]
		mean = np.mean(data, axis = 0)
		std = np.std(data, axis = 0)
		min_ = np.min(data, axis = 0)
<<<<<<< HEAD


=======
		sum = np.sum(data, axis = 0)
>>>>>>> dev
		p_idx = 2
		r_idx = 3
		# line = np.array([[i, mean[p_idx], std[p_idx], mean[r_idx], std[r_idx], min_[p_idx], min_[r_idx], 2 * mean[p_idx] * mean[r_idx] / (mean[p_idx] + mean[r_idx]) ]])
		line = np.array([[i, mean[p_idx], std[p_idx], mean[r_idx], std[r_idx], min_[p_idx], min_[r_idx], 2 * mean[p_idx] * mean[r_idx] / (mean[p_idx] + mean[r_idx]), sum[p_idx+2], sum[r_idx+2], mean[p_idx+2], mean[r_idx+2] ]])
		# line = np.array([[i, mean[p_idx], std[p_idx], mean[r_idx], std[r_idx], min_[p_idx], min_[r_idx],
		# 				  2 * mean[p_idx] * mean[r_idx] / (mean[p_idx] + mean[r_idx]), sum[p_idx+2], sum[r_idx+2], mean[r_idx+3], mean[r_idx+4], mean[r_idx+5], mean[r_idx+6], std[r_idx+3], std[r_idx+4], std[r_idx+5], std[r_idx+6]  ]  ])
		# line = np.array([[i, mean[0], mean[1], mean[2], mean[3],
		# 					std[0], std[1], std[2], std[3] ]])
		# line = np.array([[i, mean[p_idx], std[p_idx], mean[r_idx], std[r_idx], min_[p_idx], min_[r_idx], 2 * mean[p_idx] * mean[r_idx] / (mean[p_idx] + mean[r_idx]), sum[p_idx+2], sum[r_idx+2], sum[p_idx+8], sum[r_idx+8] ]])

		np.set_printoptions(suppress=True)

		if i == 0:
			table_w_veg = line_w_veg
			table_wo_veg = line_wo_veg
		else:
<<<<<<< HEAD
			table_w_veg = np.concatenate((table_w_veg, line_w_veg), axis=0)
			table_wo_veg = np.concatenate((table_wo_veg, line_wo_veg), axis=0)
=======
			table = np.concatenate((table, line), axis=0)
		total_scene = np.size(data, axis = 0)
		total_f1_mean += ( 2 * mean[p_idx] * mean[r_idx] / (mean[p_idx] + mean[r_idx]) ) * total_scene
		total_p_mean += mean[p_idx] * total_scene
		total_r_mean += mean[r_idx] * total_scene
		sum_scene += total_scene
>>>>>>> dev

		data_time = np.loadtxt(abs_path, delimiter=",", dtype=np.float32)[:, 1]
		total_t = total_t + np.mean(data_time, axis = 0) * total_scene

	print("Time: " + "{:.5f} sec".format(total_t))
	total_t /= sum_scene
	total_f1_mean /= sum_scene
	total_p_mean /= sum_scene
	total_r_mean /= sum_scene

<<<<<<< HEAD
=======
	text = "Overall F1: " + "{:.3f}".format(total_f1_mean) + " // P: " + "{:.3f}".format(total_p_mean) + " // R: " + "{:.3f}".format(total_r_mean) + " // F(Hz): " + "{:.6f}".format(1/total_t)
	print(text)

	#print(table)
	np.set_printoptions(suppress=True)
	# print(tabulate(table, headers=['Seq','P', 'P-std', 'R', 'R-std', 'P-min', 'R-min', 'F1', 'TP', 'FP', 'avg0', 'avg1', 'avg2', 'avg3', 'std0', 'std1', 'std2', 'std3'], tablefmt='github', floatfmt=".2f"))
	print(tabulate(table, headers=['Seq','P', 'std', 'R', 'std', 'P-min', 'R-min', 'F1', 'TP', 'FP', 'well', 'bad'], tablefmt='github'))
	# print(tabulate(table, headers=['avg0_GT', 'avg1_GT', 'avg2_GT', 'avg3_GT', 'std0_GT', 'std1_GT', 'std2_GT', 'std3_GT'], tablefmt='github'))
	# print(tabulate(table, headers=['avg0', 'avg1', 'avg2', 'avg3', 'std0', 'std1', 'std2', 'std3'], tablefmt='github'))
	
>>>>>>> dev
